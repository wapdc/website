(function (win) {
	"use strict";

	jQuery('.pdc-heading-state-search').on('change', function (e) {
		if (this.checked) {
			jQuery('#search-api-page-block-form-search #edit-keys').focus();
		}
	});

})(typeof window !== "undefined" ? window : this);
