(function ($, Drupal, drupalSettings) {
  'use strict';

  // Define method to subscribe to class change events.
  // @see https://stackoverflow.com/a/19401707
  jQuery.fn.onClassChange = function(cb) {
    return $(this).each((_, el) => {
      new MutationObserver(mutations => {
        mutations.forEach(mutation => cb && cb(mutation.target, $(mutation.target).prop(mutation.attributeName)));
      }).observe(el, {
        attributes: true,
        attributeFilter: ['class']
      });
    });
  };

  Drupal.behaviors.views_bootstrap_accordion_custom = {
    attach: function (context, settings) {
      // Hide links within dd elements from the tab index when closed.
      $("dd").onClassChange(function(el, newClass) {
        if (newClass === "active") {
          $(el).find('a').removeAttr('tabindex');
        }
        else {
          $(el).find('a').attr('tabindex', -1);
        }
      });

      $('a.accordion-toggle').unbind().keydown(function(e) {
        if ($('div a.accordion-toggle').is(":focus")) {
          var selected = $(this);
          if (e.which == 35) {
            /*Last item select*/
            e.preventDefault();
            $('a.accordion-toggle:focus').parent().parent().parent().siblings('div').last().find('a.accordion-toggle').focus();
          }
          if (e.which == 36) {
            /*First item select*/
            e.preventDefault();
            $('a.accordion-toggle:focus').parent().parent().parent().siblings('div').first().find('a.accordion-toggle').focus();
          }
          if (e.which == 40) {
            /*Next item select*/
            e.preventDefault();
            $('a.accordion-toggle:focus').parent().parent().parent().next('div').eq(0).find('a.accordion-toggle').focus();
            //$('a.accordion-toggle:focus').parent().parent().parent().nextAll('div').eq(0).addClass('PREVIOUS');
          }
          if (e.which == 38) {
            /*Previous item select*/
            e.preventDefault();
            $('a.accordion-toggle:focus').parent().parent().parent().prev('div').eq(0).find('a.accordion-toggle').focus();
            //$('a.accordion-toggle:focus').parent().parent().prevAll('div').eq(0).addClass('NEXT');
          }
        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
