/**
 * @file
 * CKEditor Accordion functionality.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.ckeditorAccordion = {
    // Track accordion body (<dd>) generated ID position.
    // @see Drupal.behaviors.ckeditorAccordion.generateId().
    lastGeneratedId: 1,

    /**
     * Attach the Drupal behavior to the current DOM context.
     *
     * @param {Document|Node} context
     *   The DOM content to attach the behaviors to ().
     * @param {object} settings
     *   The drupalSettings to use for this behavior.
     *
     * @see Drupal.attachBehaviors
     */
    attach: function (context, settings) {
      var behavior = this;

      // Create accordion functionality if the required elements exist is available.
      once('ckeditor-accordion', '.ckeditor-accordion', context).forEach(function (accordion) {
        // Create simple accordion mechanism for each tab.
        var $accordion = $(accordion);

        // The first one is the correct one.
        if (!settings.ckeditorAccordion.accordionStyle.collapseAll) {
          $accordion.children('dt:first').addClass('active');
          $accordion.children('dd:first').addClass('active');
          $accordion.children('dd:first').css('display', 'block');
        }
        // Turn the accordion tabs to links so that the content is accessible & can be traversed using keyboard.
        $accordion.children('dt').each(function() {
          var $tab = $(this);
          var isActive = $tab.hasClass('active');
          var $content = $tab.next('dd');

          if (!($content.attr('id'))) {
            $content.attr('id', behavior.generateId());
          }

          // Ensure aria expanded and controls attributes are applied to the
          // accordions for accessibility. This notifies assistive devices of
          // the expand/collapse state of the accordion body content.
          var togglerAttrs = `aria-controls="${$content.attr('id')}"`;
          if (isActive) {
            $content.addClass('active');
            togglerAttrs += ' aria-expanded="true"';
          }
          else {
            $content.addClass('closed');
            togglerAttrs += ' aria-expanded="false"';
          }

          // Create the toggler controls and attach the click event to it.
          const toggler = $(`<a class="ckeditor-accordion-toggler" href="#" ${togglerAttrs}><span class="ckeditor-accordion-toggle"></span>${$tab.text().trim()}</a>`);
          toggler.on('click', function (e) {
            // Don't add hash to url.
            e.preventDefault();

            var $t = $(this);
            var $dt = $t.parent();
            // Clicking the toggle toggles the display state of the accordion item.
            $dt.hasClass('active') ? behavior.collapseAccordion($t) : behavior.expandAccordion($t);
          });

          // Replace the tab content with the accordion toggle controls.
          $tab.empty().append(toggler);
        });

        // Wrap the accordion in a div element so that quick edit function shows the source correctly.
        $accordion.removeClass('ckeditor-accordion').wrap('<div class="ckeditor-accordion-container"></div>');

        // Trigger an ckeditorAccordionAttached event to let other frameworks know that the accordion has been attached.
        $accordion.trigger('ckeditorAccordionAttached');

        // Create and attach the expand or collapse event handlers.
        var $accordionCtrls = $('<ul class="accordion-controls"></ul>').prependTo($accordion);

        $(`<button class="accordion-control accordion-control--expand-all">${Drupal.t('Expand All')}</button>`)
          .on('click', behavior.applyToAll($accordion, behavior.expandAccordion))
          .appendTo($accordionCtrls)
          .wrap('<li>');

        $(`<button class="accordion-control accordion-control--collapse-all">${Drupal.t('Collapse All')}</button>`)
          .on('click', behavior.applyToAll($accordion, behavior.collapseAccordion))
          .appendTo($accordionCtrls)
          .wrap('<li>');
      });
    },

    /**
     * Generates DOM IDs that can be used with the CKEditor accordions.
     *
     * The IDs are scoped to the CKEditor accordion body context and should not
     * cause any collisions with other DOM elements. The ID helps identify the
     * body content for assistive devices and works in conjunction with
     * "aria-expanded" to link the display status of the accordion for
     * screenreaders.
     *
     * @return {string}
     *   A valid ID that can be used for an accordion body content element.
     */
    generateId() {
      return `ckeditor-accordion-body-${this.lastGeneratedId++}`;
    },

    /**
     * Collapses an accordion item.
     *
     * @param {jQuery} $toggler
     *   The accordion toggler element (wrapped with jQuery).
     */
    collapseAccordion($toggler) {
      var $dt = $toggler.closest('dt');
      if ($dt.hasClass('active')) {
        $dt.removeClass('active');
        $toggler.attr('aria-expanded', 'false');

        // Should be the <dd> with the accordion body.
        $dt.next().slideUp(300, function(){
          $dt.next().removeClass('active')
            .addClass('closed')
            .slideDown(0);
        });
      }
    },

    /**
     * Collapses an accordion item.
     *
     * @param {jQuery} $toggler
     *   The accordion toggler element (wrapped with jQuery).
     */
    expandAccordion($toggler) {
      var $dt = $toggler.closest('dt');
      if (!$dt.hasClass('active')) {
        $dt.addClass('active');
        $toggler.attr('aria-expanded', 'true');

        $dt.next().slideUp(0,function(){
          $dt.next().removeClass('closed')
            .addClass('active')
            .slideDown(300);
        });
      }
    },

    /**
     * Higher order function which creates a callback method to execute an
     * action to ALL accordion items belonging to an accordion container.
     *
     * @param {jQuery} $accordion
     *   The accordion container instance to apply the method to all items for.
     * @param {function} method
     *   Method to be called on all accordion items. Method should take a single
     *   argument, which is the toggler element wrapped by jQuery.
     *
     * @return {function}
     */
    applyToAll($accordion, method) {
      return function() {
        $accordion.find('dt > .ckeditor-accordion-toggler').each(function(i, el) {
          method($(el));
        });
      }
    }
  };

  //Add extra AX functionality - only called once on document load.
  $(document).ready(function() {
    if (!$('.ckeditor-accordion').size) return;

    // Add the key binding for accordion key events. Attached once and only
    // on document load. This prevents multiple keydown listeners from being
    // registered repeatedly.
    $(document).keydown(function(e) {
      if ($('dt a.ckeditor-accordion-toggler').is(":focus")) {
        var parent = $(this).closest('div.ckeditor-accordion-container');
        if (e.which == 35) {
          /*Last item select*/
          e.preventDefault();
          $('dt a.ckeditor-accordion-toggler:focus').parent().siblings('dt').last().find('a.ckeditor-accordion-toggler').focus();
        }
        if (e.which == 36) {
          /*First item select*/
          e.preventDefault();
          $('dt a.ckeditor-accordion-toggler:focus').parent().siblings('dt').first().find('a.ckeditor-accordion-toggler').focus();
        }
        if (e.which == 40) {
          /*Next item select*/
          e.preventDefault();
          $('dt a.ckeditor-accordion-toggler:focus').parent().nextAll('dt').eq(0).find('a.ckeditor-accordion-toggler').focus();
        }
        if (e.which == 38) {
          /*Previous item select*/
          e.preventDefault();
          $('dt a.ckeditor-accordion-toggler:focus').parent().prevAll('dt').eq(0).find('a.ckeditor-accordion-toggler').focus();
        }
        if (e.which == 32) {
          e.preventDefault();
          var $t = $('dt a.ckeditor-accordion-toggler:focus');
          var $dt = $t.parent();

          // Toggle the active state of the target element.
          const behavior = Drupal.behaviors.ckeditorAccordion;
          $dt.hasClass('active') ? behavior.collapseAccordion($t) : behavior.expandAccordion($t);
        }
      }
    });
  });
})(jQuery, Drupal, drupalSettings);
