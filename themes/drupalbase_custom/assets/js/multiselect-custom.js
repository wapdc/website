(function ($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.filter_toggle = {
		attach: function (context, settings) {
			//$('#edit-field-road-target-id').multiselect({
      //  note: instead of using the jQuery shorthand selector syntax (above) to select the target ID, we use a more verbose version (below)
      //    so that we can include the carrot ' ^ ' wildcard. This will select any IDs starting with the string 'edit-field-road-target-id'
      //    which means this will match:  'edit-field-road-target-id-randomstring' 
			$("[id^=edit-field-target-audiences-target-id]").multiselect({
				nonSelectedText: 'Select Audience',
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				buttonWidth:'100%',
			});
			$("[id^=edit-field-topic-target-id]").multiselect({
				nonSelectedText: 'Select Topics',
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				buttonWidth:'100%'
			});
			$("[id^=edit-field-news-type-target-id]").multiselect({
				nonSelectedText: 'Select Type',
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				buttonWidth:'100%'
			});
			$(".multiselect-search").ajaxSend(false);
		}

	}
})(jQuery, Drupal, drupalSettings);


