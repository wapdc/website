#!/usr/bin/env bash
set -e
# Include pantheon build commands from dev ops
if [[ -f "/app/bin/ci_build_environment.sh" ]] ; then
. /app/bin/ci_build_environment.sh
fi
# Needed later
start_dir=`pwd`
echo "Pulling site from Pantheon ..."
# Pull a copy of the pdc site
pull_site pdc-pdc
echo "Installing custom themes ..."
# delete theme
rm -rf web/themes/drupalbase_custom
# copy theme
cp -a $start_dir/themes/drupalbase_custom/. web/themes/drupalbase_custom
echo "Installing custom modules ..."
# delete modules
rm -rf web/modules/custom/pdc_api_tools web/modules/custom/pdc_custom_search_results
# copy modules
cp -a $start_dir/modules/. web/modules/custom/

# Copy config to config/common
rm -rf config/common/*
cp -a $start_dir/config/. config/common/

echo "Commit pushing to Pantheon ..."
# Commit and push to main
commit_push