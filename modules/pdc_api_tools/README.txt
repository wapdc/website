How to Add a New Datatable Page (not a page like candidates or committees or a subpage)
	1) Open pdc_api_tools_routing.yml
	2) Copy the example page config section
	3) paste it just below the last page config
	4) Update the following:
		a) page ID (must match table_name)
		b) path (full path)
		c) _title (human-readable title)
		d) table_name (must match table_name used in page ID above)
	5) Save the file
	6) Open /conf/page_table_example.yml
	7) Save as /conf/datatables/table_name.yml (table_name should match the name used in the pdc_api_tools.routing.yml file)
	8) Edit and save the new table_name.yml file (comments are in the file for specifics)
		a) If you are rewriting any fields in the table like taking data from two cells to make a link continue with step 9
		b) Otherwise skip to step 10
	9) Rewriting cells
		a) Open /assets/js/column_defs/custom_column_defs_example.js
		b) Save as your_table_name.js (must match table_name used in the routing YML)
		c) Make your edits and save 
		d) Open pdc_api_tools.liraries.yml
		e) Copy/paste the example for a new column_defs library
		f) Change the table_name to match the one in routing.yml
		g) Change table_name.js to match the one in routing.yml
	10) Save it all
	11) Commit
	12) Flush caches

How to Add a new block that presents a DataTable
The blocks are defined in code, you do not define the block through the Drupal UI

Find another block that is similar such as enforcement_case_documents_block

Duplicate the files
- enforcement_case_documents_block_table.yml
- EnforcementCaseDocumentsBlock.php
- enforcement_case_documents_block_table.js

Find the related section in pdc_api_tools.module and implement the same.

How to Add a New Page (like Committees or Candidates)
**To be honest, this is a very complex task but here is a basic, very high level rundown.

	1) First, add the conf YML file to conf/pages
		a) Follow the other pages as a guide
	2) Determine if this is a static or dynamic page
		a) if static, add a new entry in pdc_api_tools.routing.yml, similar to a datatable
		b) if dynamic, add a route_callback which gets sent to a route_callback declared in src/Routing
	3) Once the routing is in place, add the controller to src/Controller (the name is declared in the routing.yml and must match)
	4) In the controller, you can build as much or as little content as you need. Generally, we just controller the title and content in this module
	5) From the controller, the data is sent to the hook_theme function in the .module file
		a) Nothing needs to be done there unless you have added new variables
		b) If new variables are added, they must be declared here in the page section
	6) From the .module file, the data is sent to the template
		a) The naming structure will follow the ID set in the conf YML file
		b) The file will live in templates/pages
	7) Once all is in place and saved, clear cache and test it out
