(function($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.filter_control = {
		attach: function(context, settings) {
			$(once('filter_control', '#camp-exp-button')).on('click touch', function(){
				$('#table-top-nav .nav-tabs-dropdown li').each(function(){
					if ($(this).hasClass('mobile-hide')) {
						if ($(this).hasClass('opened')) {
							$(this).hide('slow').removeClass('opened');
						}
						else {
							$(this).show('slow').addClass('opened');
						}
					}
				});
			});
			
			$(once('filter-tag', '.current-filters', context)).off().on('click touch', '.filter-tag-close', function(event) { 
				var target = $(this).attr('aria-controls');
				$(this).parent('div.filter-tag').remove();
				$('#' + target + ' button').trigger('click');
			});
			
			once('filter_control', '.yadcf-filter-wrapper .yadcf-filter', context).forEach(function(filter){
				var filterId = $(filter).attr('id');
				$(this).attr('name',filterId);
				$(this).parent().parent().siblings('label').attr('for',filterId);
				var labelText = $(filter).parent().parent().siblings('label').text();
				$(this).siblings('button').prepend('<span class="sr-only">Clear the ' + labelText + ' filter</span>');
			});
			
			$(document).ajaxComplete(function( event, xhr, settings ) {
				$('.yadcf-filter-wrapper').each(function(){
					var selectVal = 'Select';
					var searchVal = 'Search';
					if($(this).find('select').length > 0) {
					}
					if($(this).find('input').length > 0) {
						$(this).find('input').removeAttr('value');
					}
					if($(this).find('div.select2-container').length > 0) {
					}
				});
			});
			
			$(once('responsive-datatable', 'table.dataTable .responsive-toggle', context)).on('click touch', function(e){
				$(this).children('.responsive-toggle-text').first().toggleClass('opened');
				if ($(this).children('.responsive-toggle-text').first().hasClass('opened')) {
					$(this).children('.responsive-toggle-text').first().html('Close');
				}
				else {
					$(this).children('.responsive-toggle-text').first().html('Open');
				}
			});
			
			$(once('filter-collapse', '#filter-colapse-control, .filter-collapse-control', context)).on('click touch', function(){
				const text = $(this).attr('aria-expanded') == 'true' ? 'Edit Filters' : 'Hide Filters';
				$(this).html(text);
			});
			
			$(once('nav-tabs-dropdown', '.nav-tabs-dropdown', context))
				.on('click touch', 'li:not(".active") a', function(event) {
					$(this).closest('ul').removeClass("open");
				})
				.on('click touch', 'li.active a', function(event) {
					$(this).closest('ul').toggleClass("open");
				});
		}
  }
})(jQuery, Drupal, drupalSettings);
