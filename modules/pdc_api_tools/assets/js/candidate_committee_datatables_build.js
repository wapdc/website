//used only for the in-page tables on the Committee and Candidate pages
(function($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.candidate_datatable = {
		attach: function(context, settings) {
			// Uses once for "datatable" so jQuery.DataTable is already initialized
			// for this table. The jQuery.DataTable library adds "dataTable" class
			// to this table, so the regular Drupal.behaviors.candidate_datatable
			// behavior might try to initialize this again with jQuery.DataTable,
			// which causes errors.
			once('datatable', 'table.dataTableAlt').forEach(function (table) {
				$(table).DataTable({
					"autoWidth": false,
					"paging": true,
					"pageLength": 10,
					"responsive": true,
					"order": [[ 1, "desc" ]]
				});
			});

		}
	}
})(jQuery, Drupal, drupalSettings);
