((Drupal, once, $, Chart, PDC) => {

  Drupal.behaviors.pdc_chart_bar = {
    attach(context, settings) {
      const allChartsSettings = settings.pdc_chart_bar;
      $(once('pdc_chart_bar', '[data-pdc-chart="bar"]'))
        .find('.pdc-chart-table')
        .each((i, element) => {
          const table = $(element).addClass('visually-hidden');
          const wrapper = table.parent();
          const chartId = wrapper.data('pdc-chart-id');
          let {title, headings, rows, chart_options} = allChartsSettings[chartId];
          const labels = rows.map(r => r[0]).filter(r => Boolean(r));
          const labelless = labels.length === 0;

          // The data is structured and rendered in HTML in a row-wise fashion.
          // But for charts, it's column-wise by dataset. Also, the data is
          // expected to be a full table (i.e. with row and column headings)
          // This means the data actually starts at (1,1) instead of (1, 0).
          const datasets = headings.slice(1).map((heading, i) => ({
            label: heading,
            backgroundColor: PDC.chartColors[i],
            data: rows.map(r => r[i + 1])
          }));

          const legendContainer = $('<div />')
            .appendTo(wrapper)
            .addClass('pdc-chart-legend-container')
            .get(0);

          const canvas = $('<canvas />').appendTo(wrapper);
          const context = canvas.get(0).getContext('2d');

          // Chart.js does not show data if the labels are not supplied. For
          // unlabelled data (e.g. independent expenditures), just supply a
          // blank label.
          const finalLabels = labelless ? rows.map(() => '') : labels;

          new Chart(context, {
            type: "bar",
            data: {
              labels: finalLabels,
              datasets: datasets
            },
            plugins: [
              PDC.pdcHtmlTitlePlugin,
              PDC.pdcHtmlLegendPlugin
            ],
            options: $.extend(true, {}, chart_options, {
              responsive: true,
              scales: {
                y: {
                  beginAtZero: true
                }
              },
              indexAxis: 'y',
              plugins: {
                legend: {
                  display: false
                },
                pdcHtmlTitle: {
                  container: wrapper,
                  title: title || table.find('.pdc-chart-title').text(),
                },
                pdcHtmlLegend: {
                  container: legendContainer,
                  onItemToggle(chart, item) {
                    chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
                  },
                  getLegendItemInfo(chart, item, options) {
                    const {text, fillStyle: color, datasetIndex} = item;

                    // One-off implementation for independent expenditures.
                    // Show the first value of the dataset as labels.
                    const value = options.showValueOnLabel
                      ? `$${datasets[datasetIndex].data[0].toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2})}`
                      : '';

                    return {text, color, value}
                  }
                }
              }
            })
          });
        });
    }
  };
})(Drupal, once, jQuery, Chart, PDC);
