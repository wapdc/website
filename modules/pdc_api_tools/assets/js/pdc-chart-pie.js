((Drupal, once, $, Chart, PDC) => {

  Drupal.behaviors.pdc_chart_pie = {
    attach (context, settings) {
      $(once('pdc_chart_pie', '[data-pdc-chart="pie"]'))
        .find('.pdc-chart-table')
        .each((i, e) => {
          const table = $(e).addClass('visually-hidden');
          const wrapper = table.parent();
          const chartId = wrapper.data('pdc-chart-id');
          const {title, rows, chart_options} = settings.pdc_chart_pie[chartId];
          const labels = rows.map(r => r[0]).filter(r => Boolean(r));
          const values = rows.map(r => r[1]).filter(r => Boolean(r));

          const widget = $(`
            <div class="pdc-chart-widget">
              <div class="pdc-chart-legend-container"></div>
              <div class="pdc-chart-chart-container"></div>
            </div>
          `).appendTo(wrapper);

          const legendContainer = $('.pdc-chart-legend-container', widget).get(0);
          const canvas = document.createElement('canvas');
          const context = canvas.getContext('2d');

          $('.pdc-chart-chart-container', widget).append(canvas);

          new Chart(context, {
            type: 'pie',
            responsive: true,
            data: {
              labels: labels,
              datasets: [{
                backgroundColor: PDC.chartColors,
                data: values
              }]
            },
            plugins: [
              PDC.pdcHtmlTitlePlugin,
              PDC.pdcHtmlLegendPlugin
            ],
            options: $.extend(true, {}, chart_options, {
              responsive: true,
              plugins: {
                legend: {
                  display: false
                },
                pdcHtmlTitle: {
                  container: wrapper,
                  title: title || table.find('.pdc-chart-title').text(),
                },
                pdcHtmlLegend: {
                  container: legendContainer,
                  onItemToggle(chart, item) {
                    chart.toggleDataVisibility(item.index);
                  },
                  getLegendItemInfo(chart, item) {
                    const {text, fillStyle: color, index} = item;
                    const value = `$${(values[index]||0).toLocaleString('en-US', {minimumFractionDigits: 2})}`;
                    return {text, color, value}
                  }
                }
              }
            })
          });
        });
    }
  };
})(Drupal, once, jQuery, Chart, PDC);
