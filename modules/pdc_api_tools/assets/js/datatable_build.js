(function($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.pdcDatatable = {
		instances: new Map(),

		attach: function(context) {
			// Get all the tables with the class dataTable
			once('datatable', 'table.dataTable', context).forEach((table) => {
				// Create a datatable instance and store it by the table element ID.
				const pdcTable = new PdcDatatable(table.id, table);
				this.instances.set(table.id, pdcTable);

			  let oTable = $(table).DataTable({
					// Defer the render to the query params loader to avoid duplicate calls.
					deferRender: true,
					autoWidth: false,
					searching: true,
					processing: true,
					serverSide: true,
					columns: pdcTable.columns,
					columnDefs: column_defs(pdcTable.tableId),
					searchCols: pdcTable.getDefaultSearch(),
					pageLength: pdcTable.pageLength,
					initComplete: function(settings, data){
						$("#independent-expenditures-table_filter").detach().appendTo('#global-filter');
						if (pdcTable.responsive) {
							//pdcTable.table.querySelectorAll('tbody tr').forEach(function (row) {
							//  $(row).children('td').first().prepend('<a href="#" class="sr-only responsive-toggle">Expand this row</a>');
							//});
						}
					},
					fnDrawCallback: function() {
						$('.data-table-container .cover').addClass('remove');

						if (pdcTable.responsive) {
							pdcTable.table.querySelectorAll('tbody tr').forEach(function (row) {
								const cell = $(row).children('td').first();
								if (cell.length) {
									cell.prepend(`<button class="sr-only responsive-toggle" role="button" aria-hidden="false"><span class="responsive-toggle-text">Open</span> extra data for ${cell.text()}</button>`);
								}
							});
						}
					},
					dom: 'Blrtip',
					buttons: [],
					ajax: function(data, callback, settings) {
						if (!(pdcTable.initComplete && pdcTable.active)) {
							// Datatables try to hydrate the table immediately with data,
							// this happens before we even get a chance to initialize YADCF
							// filters. Wait for YADCF filters to be ready to fetch data.
							return;
						}

						// Storing this on the settings object might not be the best approach. It's cautioned against unless you are
						// writing a plugin but we need a way to keep some state about the last action so we don't need to query counts
						// for each ajax call. For example, a sort operation won't change the filtered count. There is a possible timing
						// issue where ajax requests run out of order and a different request changes these values.
						if (!settings.dadcSettings) {
							settings.dadcSettings = {};
						}

						// Clear the previous filters
						const filter = {list: []};
						const currentFilters = $('.current-filters', pdcTable.container);
						currentFilters.find('.filter-tag').not('.jurisdiction_code_address').remove();

						const filterTagTpl = function (machineName, label, text, filterName) {
							return `<div id="filter-close--${machineName}" class="bg-primary filter-tag">
									<span class="filter-label">${label}</span>: <span class="filter-value">${text}</span>
									<button id="filter-tag-close--${machineName}" class="filter-tag-close" aria-controls="${filterName}">
										<i class="fas fa-times"></i><span class="sr-only">Remove ${text}</span>
									</button>
								</div>`;
						};

						if ('keyword' === pdcTable.filterType) {
							filter.type = '$q';
							filter.list.push(pdcTable.searchQuery);

							// If search query, create the current filter tag.
							if (pdcTable.searchQuery) {
								const tag = $(filterTagTpl('global-filter', Drupal.t('Keywords'), pdcTable.searchQuery, 'table-filter-wrapper--search'));
								currentFilters.append(tag);
							}
						} else {
							// The where clause is always case sensitive so if we are searching for text, we need to use lower() on both
							// sides of the comparison.
							filter.type = '$where';
							filter.list.push(pdcTable.baseFilter);

							// Loop through the data columns and get the search object data.columns[n]search.value. There is also a
							// data.columns[n]search.regex that could probably be used to determine if we should use = or contains.
							data.columns.forEach(function (item, index) {
								if (item.search.value) {
									// Chosen is escaping characters in the selection with \ thinking that they need to be when
									// it thinks it should consider the text a regular expression. I couldn't figure out if there
									// was a way to turn ot off so I'm just checking for regex and un-escaping.
									if (item.search.regex) {
										// Replaces all \ that is not followed by \ with just the empty string and then replace all instances
										// of \\ with just \, the un-escaped version of a literal \. This is a pretty weak mechanism but should
										item.search.value = item.search.value.replaceAll(/\\(?!\\)/g, "").replaceAll(/\\\\/g, "\\")
									}

									var searchValLabel = $('#' + item.name + ' > label').text();
									if (item.search.value !== '-yadcf_delim-') {
										var searchValText = item.search.value.replace('-yadcf_delim', '');

										// Allow search values with pipes to wrap.
										searchValText = searchValText.replaceAll('|', ' | ');
										const tag = $(filterTagTpl(item.name, searchValLabel, searchValText, `yadcf-filter-wrapper--${item.data}`));

										if ((pdcTable.id === 'candidates-table') && (item.name === 'jurisdiction_code')) {
											if(currentFilters.find('.filter-tag.jurisdiction_code_address').length === 0) {
												tag.addClass('jurisdiction_code_address');
												currentFilters.append(tag);
											}
										} else {
											currentFilters.append(tag);
										}
									}

									// Is this text to be quoted or a number and what is the operation type
									const whereStatementItem = buildWhereStatementItem(item.search.value, pdcTable.columns[index]);
									filter.list.push(whereStatementItem);
								}
								else if ('jurisdiction_code' === item.name) {
									$('#address-search-input').val('');
									$('#filter-tag-close--jurisdiction_code').parent().remove();
								}
							});

							filter.list = filter.list.filter(item => item.trim().length > 0);
							if (filter.list.length > 0) {
								filter.list[0] = filter.list[0].replace(/^\s*(where|and)\s*/i, '');
							}
						}

						// After filter tags are updated, determine if there current are any.
						const hasFilterTags = Boolean(currentFilters.find('.filter-tag').length);
						currentFilters.siblings('.no-filter-results')[ hasFilterTags ? 'hide' : 'show']();

						pdcTable.search(filter, data, settings.dadcSettings, callback);
					}
				});

				// Flag that indicates that we shouldn't push state if the resulting XHR
				// is from a popstate draw call.
				let isSearchPopping = false;

				// Flag that indicates that we shouldn't push state if the resulting XHR
				// is from the initial draw call (we just use the page load state for
				// the initial landing).
				let isSearchInitializing = true;

				// Push the for search into history as query parameters.
				oTable.on('preXhr.dt', (event, settings, data) => {
					if (!isSearchPopping && !isSearchInitializing) {
						const tableInst = this.instances.get(event.currentTarget.id);
						let queryString = '';

						if (tableInst && 'keyword' === tableInst.filterType) {
							queryString = tableInst.searchQuery ? `?search=${encodeURIComponent(tableInst.searchQuery)}` : '';
						} else {
							const searchParamPairs = data.columns
								.filter((item) => item.name != 'jurisdiction_code' && Boolean(item.search.value))
								.map((item) => [
									item.name,
									item.search.regex
										? item.search.value.replaceAll(/\\(?!\\)/g, "").replaceAll(/\\\\/g, "\\")
										: item.search.value
								]);

							// Remove any empty pairs.
							Object.keys(searchParamPairs).forEach(function (key) {
								if ((searchParamPairs[key]||[])[1] === '-yadcf_delim-') {
									searchParamPairs.splice(key, 1);
								}
							});
							queryString = searchParamPairs.length ? `?${new URLSearchParams(searchParamPairs)}` : ''
						}
						window.history.pushState({queryString}, '', `${window.location.pathname}${queryString}${window.location.hash}`);
					}
					isSearchPopping = false;
					isSearchInitializing = false;
				});

				// Listen for when back and forward are used.
				$(window).on('popstate', ({originalEvent}) => {
					isSearchPopping = true;
					pdcTable.initFromSearch(new URLSearchParams(originalEvent.state?.queryString ?? ''));
				})

				// Initialize the table with searches based on query parameters.
				pdcTable.initFromSearch(new URLSearchParams(window.location.search));
			});
		},

		/**
		 * Set the active state of the PDC datatable instance for this table element.
		 *
		 * This is used mainly to prevent inactive tables (on non-visible tabs) from
		 * querying and reacting to URL parameter changes from active tables.
		 *
		 * @param {Element} table
		 *   The datatable table element to set the active state for.
		 * @param {bool} enabled
		 *   The active state value to set for the datatable.
		 */
		setDatatableState(table, enabled = true) {
			const pdcTable = Drupal.behaviors.pdcDatatable.instances.get(table.id);

			if (pdcTable) {
				pdcTable.active = enabled;
			}
		}
	};

	/**
	 * For all the columns that require a distinct value list, fetch the
	 * distinct values for that column.
	 * @param pdcTable
	 * @param columnFilterQuery
	 * @param filterList
	 * @returns {Promise<unknown[]>} An array of the distinct values.
	 */
	async function getColumnFilterValues(pdcTable, columnFilterQuery, filterList) {
		let columnFilterValues = [];
		pdcTable.columns.forEach(function (column, index) {
			if (column.dadcColGetDistinctValues) {
				let query = columnFilterQuery
					.replaceAll('%column%', column.dadcCol)
					.replaceAll('%alias%', column.name);

				// Revise options for fields based the values of fields they depend on.
				Object.entries(pdcTable.filters||{}).forEach(function ([name, filter]) {
					Object.keys(filter.dependencies).forEach(function (dependencyName) {
						if (name === column.name) {
							let dependentFilters = getFilters(filterList, dependencyName);
							if (dependentFilters.length) {
								let qParts = query.split(' group by ');
								qParts[0] += ` and lower(${dependencyName}) in ("${dependentFilters.join('","')}")`;
								query = qParts.join(' group by ');
							}
						}
					});
				});

				const url = `${pdcTable.baseUrl}?$query=${encodeURIComponent(query)}`;
				columnFilterValues[index] = fetch(url, {headers: {'X-App-Token': pdcTable.socrataToken}})
					.then(response => response.json())
					.then(result => result.map(item => item[column.name]));
			}
			else {
				columnFilterValues[index] = null;
			}
		});
		return Promise.all(columnFilterValues);
	}

	function getFilters(filterList, filterName) {
		let filters = [];
		filterList.forEach(filter => {
			if (filter.includes(filterName)) {
				const allMatches = [...filter.matchAll(/%([a-z]+)%/g)];
				allMatches.forEach(matches => {
					filters.push(matches[1]);
				})
			}
		});
		return filters;
	}

	/**
	 * Represents a single table filter item.
	 */
	class TableFilter {
		constructor(table, id, element, dependencies = {}) {
			this.table = table;
			this.id = id;
			this.el = element;
			this.dependencies = dependencies;

			this.inputs = this.el.querySelectorAll('input,select');
			this.clearBtn = this.el.getElementsByClassName('yadcf-filter-reset-button')[0];

			this.col = {};
			for (let i = 0; i < table.columns.length; ++i) {
				if (id === table.columns[i].name) {
					this.col.def = table.columns[i];
					this.col.index = i;
					break;
				}
			}

			this.label = this.el.querySelector(':scope > label');
			this.inputs.forEach((item, i) => {
				const input = $(item);

				// Need to initialize the datapickers after the form elements are created.
				// Otherwise the textfield values disappear after you interact with them
				// (datepicker resets with its internal value - empty).
				if (input.val() && (input.hasClass('yadcf-filter-range-date') || input.hasClass('yadcf-filter-date'))) {
					input.datepicker('setDate', input.val());
				}

				// Attach the label to the form element or create an additional label for
				// range form elements (date range and number range).
				if (this.label) {
					if (0 === i && !$(this.label).attr('for')) {
						$(this.label).attr('for', input.attr('id'));
					}
					else if (1 === i && !input.data('hasLabel')) {
						// Add a input label for the end of number or date range inputs - only visible to screenreaders (sr-only).
						const endText = input.hasClass('yadcf-filter-range-date') ? 'to date' : 'to value';
						input.before(`<label for="${input.attr('id')}" class="sr-only">${$(this.label).text()} ${endText}</label>`).data('hasLabel', 1);
					}
				}
			});
		}

		attachDependencies() {
			const dependencyKeys = Object.keys(this.dependencies);

			if (dependencyKeys.length) {
				const title = this.getDependencyTitle(dependencyKeys[0]);
				const el = $(this.el);
				const showTooltip = function () {
					if (el.hasClass('disabled')) {
						el.tooltip('show');
					}
				};
				const hideTooltip = function () {
					el.tooltip('hide');
				};

				// Support filter keyboard focus/blur tooltip display.
				$(this.clearBtn)
					.on('focus', showTooltip)
					.on('blur', hideTooltip);

				// Support field mouse over/out tooltip display.
				el
					.on('mouseover', showTooltip)
					.on('mouseout', hideTooltip)
					.attr('data-toggle', 'tooltip')
					.attr('data-original-title', title)
					.addClass('disabled');
			}
		}

		getDependencyTitle(name) {
			if (this.dependencies[name]) {
				return this.dependencies[name];
			}
			else if (this.table.filters[name]) {
				return `Please select "${this.table.filters[name].label.textContent||name}" before selecting "${this.label.textContent||this.id}".`
			}

			return ``;
		}

		enable() {
			$(this.el)
				.removeClass('disabled')
				.removeAttr('data-toggle');

			$(this.inputs).find('.yadcf-filter').removeAttr('tabindex').show();
		}

		disable() {
			$(this.el)
				.addClass('disabled')
				.attr('data-toggle', 'tooltip');

			$(this.inputs).find('.yadcf-filter').attr('tabindex', '-1').hide();
		}

		dependencyChanged(dependency, value) {
			if (value.length) {
				// Dependency has value. Allow the filter to be used.
				this.enable();
				$(this.el).removeAttr('data-original-title');
			}
			else {
				// Dependency does not have value. Do not allow the filter to be used.
				this.disable();
				$(this.el).attr('data-original-title', this.getDependencyTitle(dependency.id));

				if ($(this.inputs).val().length) {
					// Clear the filter value for the filter with the dependency.
					yadcf.doFilter('clear', '-candidates-table', this.col.index);
				}
			}
		}
	}

	class KeywordFilter extends TableFilter {
		constructor(table, id, element, dependencies = {}) {
			// Create the search input, since these are a special case not managed
			// by the YADCF library. Needs to be generated a hooked separately.
			$(`
				<div id="table-filter-wrapper--search" class="yadcf-filter-wrapper">
					<input name="keyword-search" type="search" id="keyword-search" class="form-control" placeholder="Enter search keywords"/>
					<button class="yadcf-filter-reset-button"><span class="sr-only">Clear the search filter</span><span aria-hidden="true">x</span></button>
				</div>
			`).appendTo(element);

			super(table, id, element, dependencies);

			// Event callback when the input value changes.
			this.onChanged = Drupal.debounce((input) => {
				this.table.searchQuery = input.value;
				$(this.table.table).DataTable().draw();
			}, 500);

			// Attach the inputs and behaviors.
			$(this.inputs)
				.val(table.searchQuery||'')
				.on('keyup', (e) => {
					const ignore = [9, 16, 17, 18, 20, 27, 33, 34, 35, 36, 37, 38, 39, 40];
					if (ignore.includes(e.keyCode)) {
						return;
					}

					this.onChanged(e.target);
				});

			$(this.clearBtn).on('click', () => {
				$(this.inputs).val('');
				this.onChanged(this.inputs[0]);
			});
		}
	}

	class PdcDatatable {
		constructor(id, table) {
			this.active = true;
			this.id = id;
			this.table = table;
			this.container = $(table).closest('.data-table-container').get(0);

			const { dataset } = this.table;
			this.tableId = dataset.tableId;
			this.datasetId = dataset.dadcDsid;
			this.socrataToken = dataset.dadcSocrataAppToken;
			this.baseUrl = `https://${dataset.dadcSocrataDomain}/resource/${this.datasetId}.json`;
			this.baseFilter = dataset.dadcFixedFilter || '1 = 1';
			this.pageLength = dataset.pageLength || 25;

			// Data attributes are strings, and needs to be converted to a boolean value.
			this.responsive = /^(true|1)$/i.test(dataset.responsive);

			// The dataset :id psudo-column is used if none is provided. :id is whatever the is the id column, regardless of the
			// column name. Having this always reliable final sort is important so paging always happens in the same order. For
			// example, sorting by name will actually sort by name, id to force the same order even when a name is not unique.
			this.finalSort = dataset.dadcFinalSort || ':id';

			// Initialize table data columns and meta-data from the table head columns definitions.
			this.initComplete = false;
			this.filters = null;
			this.columns = [];
			this.selectList = [];
			this.rowProtoType = {};

			const headRow = table.querySelector("tHead tr");
			if (headRow) {
				headRow.querySelectorAll("th").forEach(this.addTableColumn, this);

				// Append additional columns to the select list. These are not part of the table but available to DataTables
				const extraCols = headRow.dataset.dadcCol ? headRow.dataset.dadcCol.split(',') : [];
				this.selectList.push(...extraCols);
			}
		}

		initFromSearch(params) {
			this.initComplete = true;
			const dt = $(this.table).DataTable();

			// Determine if keyword search is available and initialize the search type.
			if (!this.filterType) {
				const filter = $(this.container).find('.all-the-filters .global-filter');
				this.filterType = filter.length && params.get('search') ? 'keyword' : 'filter';
			}

			if ('filter' === this.filterType) {
				// @todo Document why these keys are omitted.
				const keysToOmit = ['juris', 'address', 'exp'];
				const rangeValues = [];
				Array.from(params.entries())
					.filter(([k]) => !keysToOmit.includes(k))
					.forEach(([k, v]) => {
						const colId = this.getColumnId(k);
						if (null !== colId) {
							dt.columns(colId).search(v);

							// Initialize number and date range filters. The default YADCF does not recognize
							// and populate these values correctly on its own.
							const searchType = this.columns[colId].dadcSearchType;
							if ('number-range' === searchType || 'date-range' === searchType) {
								if (v.includes('-yadcf_delim-')) {
									let [start = '', end = ''] = v.split('-yadcf_delim-');
									rangeValues.push([colId, {from: start, to: end}]);
								}
							}
						} else {
							dt.columns(`th[data-dadc-col="${k}"]`).search(v);
						}
					});

				// Apply the default range values for all the filters.
				if (rangeValues) {
					yadcf.exFilterColumn(dt, rangeValues);
				}
			} else {
				this.searchQuery = params.get('search');
			}

			yadcf.init(dt, this.getYadcfSettings());

			if (null === this.filters) {
				this.filters = {}

				// Build the table filters attach them.
				const filterDependencies = getFilterDependencies();
				$(this.container).find('.all-the-filters .table__filter').each((i, el) => {
					const id = el.id;
					const deps = filterDependencies[id];

					this.filters[id] = ('global-filter' === id) ? new KeywordFilter(this, id, el, deps) : new TableFilter(this, id, el, deps);
				});

				// Special case for jurisdiction_code which is attached to the
				// address tool, which needs to be cleared when the filter is cleared.
				$('#yadcf-filter-wrapper--jurisdiction_code button.yadcf-filter-reset-button').on('click touch',function(){
					$('#address-search-input').val('');
					$('#filter-tag-close--jurisdiction_code').parent().remove();
				});

				if (this.filters['global-filter']) {
					this.createFilterGroups(this.filters['global-filter'], $(this.container));
				}

				Object.values(this.filters).forEach(function(filter) {
					filter.attachDependencies();
				});

				// Listen for changes to the filter dependency.
				// For consistency, this needs to occur on draw.dt events.
				let filterHistory = {};
				$(document).on('draw.dt', (e) => {
					const table = this;

					// A different datatable instance, these table filters can be ignored.
					if (e.target.id !== this.id) return;

					// Check for filter dependency changes, and update the filter state.
					Object.values(this.filters).forEach(function(filter) {
						Object.keys(filter.dependencies||{}).forEach((name) => {
							const val = $(table.filters[name].inputs).val();
							if (undefined === filterHistory[name] || !isValuesMatch(val, filterHistory[name])) {
								filterHistory[name] = val;
								filter.dependencyChanged(table.filters[name], val);
							}
						});
					});
				});
			}
		}

		search(filter, data, dadcSettings, callback) {
			(async function (pdcTable, filter, data, dadcSettings, callback) {
				const {baseUrl, selectList} = pdcTable;
				const baseFilter = pdcTable.baseFilter.replace(/^\s*(where|and)\s*/i, '');

				const selectFilter = filter.list.join(' ');
				const requestOpts = {headers: {'X-App-Token': pdcTable.socrataToken}};
				const baseQuery = new URLSearchParams();
				baseQuery.set(filter.type, selectFilter);

				// Fetch the all rows count.
				const resultAllRowCount = dadcSettings.allRowCount
					? dadcSettings.allRowCount
					: fetch(`${baseUrl}?$select=count(*)&$where=${encodeURIComponent(baseFilter)}`, requestOpts).then(response => response.json()).then(rows => rows[0].count);

				// Fetch the filtered rows count.
				let filteredRowCount = null;
				if (dadcSettings.selectFilter === selectFilter && dadcSettings.filteredRowCount) {
					filteredRowCount = dadcSettings.filteredRowCount;
				} else {
					dadcSettings.selectFilter = selectFilter;
				}

				if (!filteredRowCount) {
					filteredRowCount = fetch(`${baseUrl}?$select=count(*)&${baseQuery.toString()}`, requestOpts)
						.then(response => response.json())
						.then(rows => rows[0].count);
				}

				// A template of the url to fetch the distinct values of a column which can be used to populate select lists.
				// This needs to apply baseFilter because if the base is all candidates, the filters should not include values
				// that are only for non-candidates
				let resultColumnFilterValues = [];
				if ('$where' === filter.type) {
					const columnFilterTemplate = `select %column% as %alias% where ${baseFilter} and %column% is not null group by %column% limit 1000000`;
					resultColumnFilterValues = getColumnFilterValues(pdcTable, columnFilterTemplate, filter.list);
				}

				// Convert the filter into search query values.
				var sortColumns = [];
				const tableOrder = data.order || [];
				tableOrder.forEach(function (item) {
					sortColumns.push(`${pdcTable.columns[item.column].data} ${item.dir}`);
				});
				sortColumns.push(pdcTable.finalSort);

				// Apply the data query constraints for the paginated rows.
				baseQuery.set('$select', selectList.join(','))
				baseQuery.set('$order', sortColumns.join(','));

				// Create a download link that applies the filters and query without the page or size limit.
				let filename = pdcTable.id || 'data';
				let dlQuery = new URLSearchParams(baseQuery);
				dlQuery.set('$limit', 10000000);
				let downloadUrl = `/political-disclosure-reporting-data/browse-search-data/download?dsid=${pdcTable.datasetId}&fname=${filename}&${dlQuery}`;

				// Apply paged query range constraints.
				baseQuery.set('$offset', data.start);
				baseQuery.set('$limit', data.length);
			  const pagedQuery = fetch(`${baseUrl}?${baseQuery}`, requestOpts).then(response => response.json());

				const [resultRows, rowCount, allCount, filterValues] = await Promise.all([pagedQuery, filteredRowCount, resultAllRowCount, resultColumnFilterValues]);

				// Value is a error object when invalid input is provided.
				if (Array.isArray(resultRows)) {
					resultRows.forEach(function (item, index, array) {
						array[index] = {...pdcTable.rowProtoType, ...item};
					});
				}

				let json = {
					"draw": data.draw,
					"recordsTotal": allCount,
					"recordsFiltered": rowCount,
					"data": resultRows
				};
				dadcSettings.filteredRowCount = rowCount;
				dadcSettings.allRowCount = allCount;
				pdcTable.createDownloadLinkInCaption(downloadUrl, rowCount);

				if (filterValues && filterValues.length) {
					dadcSettings.columnFilterValues = filterValues

					// Always update filter values for filters with dependencies on other filters.
					Object.values(pdcTable.filters).forEach(function (filter) {
						const colId = filter.col.index || -1;
						if (filter.dependencies && colId >= 0) {
							dadcSettings.columnFilterValues[colId] = filterValues[colId];
						}
					});

					if(dadcSettings.columnFilterValues) {
						dadcSettings.columnFilterValues.forEach(function (item, index) {
							if (item) {
								json["yadcf_data_" + index] = item
							}
						})
					}
				}

				if (callback) {
					callback(json);
				}
			}) (this, filter, data, dadcSettings, callback);
		}

		addTableColumn(cell) {
			// Column type defaults to text and alias defaults to column
			const {dataset: cellData} = cell;
			const dadcCol = cellData.dadcCol;
			const dadcColAlias = cellData.dadcColAlias;

			let selectItem = dadcCol;
			if (dadcColAlias) {
				selectItem += ` as ${dadcColAlias}`;
			}
			this.selectList.push(selectItem);

			const column = {
				cell: cell,
				name: dadcColAlias || dadcCol,
				data: dadcColAlias || dadcCol,
				dadcCol: dadcCol,
				dadcColType: cellData.dadcColType ?? "text",
			};

			// Store values on the columns to be used by the fetcher ajax. Not using the built in DataTables types because of
			// the server-side handling and wanting to have particular handling based on Socrata types.
			if (cellData.dadcRender) column.dadcRender = cellData.dadcRender;
			if (cellData.dadcSearchType) column.dadcSearchType = cellData.dadcSearchType;
			if (cellData.dadcColGetDistinctValues) column.dadcColGetDistinctValues = cellData.dadcColGetDistinctValues;

			this.columns.push(column);

			// The row prototype contains all columns because Socrata only returns columns with values. It also specifies the
			// the default value when the column is not present. Datatables needs all the columns.
			this.rowProtoType[dadcColAlias ?? dadcCol] = cellData.dadcColMissingValue ?? "";
		}

		getColumnId(column_name) {
			for (let i = 0; i < this.columns.length; ++i) {
				if (column_name === this.columns[i].name) {
					return i;
				}
			}
			return null;
		}

		getColumnIds(column_names) {
			if (!column_names instanceof Array) {
				column_names = [column_names];
			}

			const ids = [];
			this.columns.forEach((col, i) => {
				if (column_names.includes(col.name)) {
					ids.push(i);
				}
			});
			return ids;
		}

		getDefaultSearch() {
			var searchCols = [];
			let colCount = 0;

			this.columns.forEach(function (col) {
				colCount++;
				const cell = col.cell;

				if (cell && cell.innerText.toLowerCase() == 'election year') {
					if (cell.dataset.disableDefaultYear != 1) {
						colCount = colCount - 1;
						for (let i = 0; i < colCount; i++) {
							searchCols.push(null);
						}
						//The push() below adds the current year to the default filters
						//Added in the beginning but removed later
						//Just here in case it needs to come back
							// const d = new Date();
							// searchCols.push({"search": d.getFullYear()});
					}
				}
			});
			return searchCols;
		}

		createDownloadLinkInCaption(downloadUrl, rowCount) {
			let caption = this.table.querySelector('caption');
			if (!caption) {
				caption = document.createElement("caption");
				this.table.appendChild(caption);
			}

			let dlElement = caption.querySelector('.datatable__download-data');
			if (!dlElement) {
				dlElement = document.createElement("span");
				dlElement.classList.add('datatable__download-data');
				caption.prepend(dlElement);
			}

			let warning = "";
			if (rowCount > 30000) {
				warning = "Downloading this many rows might take a very long time. Consider searching for a more specific result."
			}
			dlElement.innerHTML = `<a href="${downloadUrl}" download>Download ${rowCount} rows as CSV.</a> ${warning}`
		}

		getYadcfSettings() {
			const settings = [];
			this.columns.forEach(function ({cell}, i) {
				const attrsHavingPrefix = getDataAttributesHavingPrefix(cell.dataset, 'yadcf');
				if (Object.keys(attrsHavingPrefix).length > 0) {
					attrsHavingPrefix.style_class = 'form-control';
					if(attrsHavingPrefix.filter_type == 'select' || attrsHavingPrefix.filter_type == 'multi_select') {
						attrsHavingPrefix.filter_default_label = "Select";
						//attrsHavingPrefix.filter_default_label = "Select " + cell.textContent;
					}
					if (attrsHavingPrefix.filter_type == 'text' || attrsHavingPrefix.filter_container_id == 'jurisdiction') {
						attrsHavingPrefix.filter_default_label = "Enter text";
						//attrsHavingPrefix.filter_default_label = "Enter " + cell.textContent;
					}
					settings.push({column_number: i, ...attrsHavingPrefix});
				}
			});
			return settings;
		}

		createFilterGroups(searchFilter, container) {
			const filters = $(container).find('.filters').wrap('<div>').removeClass('filters');
			const wrapper = filters.parent().addClass('filters').attr('role', 'search');
			const switchers = $('<div class="search-switcher-container"></div>').insertBefore(filters);
			const keyword = $('<div class="global-filter-wrapper"></div>').insertBefore(filters);

			// Put the keyword search input into the wrapper.
			keyword.append(searchFilter.el);

			const switcherTpl = function(key, label) {
				return `<div class="search-type-switcher">
						<input id="table-search-type-${key}" type="radio" name="search-type-radio" value="${key}"/>
						<label for="table-search-type-${key}">${label}</label>
					</div>`;
			};

			const groups = [
				{
					type: 'keyword',
					wrapper: keyword,
					el: $(switcherTpl('keyword', Drupal.t('Search by keyword'))),
				},
				{
					type: 'filter',
					wrapper: filters,
					el: $(switcherTpl('filter', Drupal.t('Search by filter criteria'))),
				}
			];

			// Initialize each of the filter groups.
			groups.forEach((item) => {
				item.el.appendTo(switchers);

				// Set the radio button for the active filter type.
				if (item.type === this.filterType) {
					item.el.find('input').attr('checked', 'checked');
				} else {
					item.wrapper.hide();
				}

				item.el.find('input').on('change', (e) => {
					this.filterType = e.target.value;

					groups.forEach((item) => {
						const disp = (e.target.value === item.type) ? 'show' : 'hide';
						$(item.wrapper)[disp]();
					});

					$(this.table).DataTable().draw();
				});
			});
		}
	};

	/**
	 * Callback for each of the datatable column data types and search methods. The statement builders are organized by
	 * the column data type, with the callbacks keyed by the column search type.
	 *
	 * So a "text" column type using "text-tokens" as the filter or search method can be found with:
	 *   whereStatementBuilders["text"]["text-tokens"]
	 *
	 * The default search method for the column type has the key "__default" and is used when no matching search method
	 * is found for the data type. All callbacks have the function signature "function(column, value)" where the "column"
	 * parameter is the column definition from a PdcTable object.
	 *
	 * @see buildWhereStatementItem()
	 */
	const whereStatementBuilders = {
		'text': {
			'equals': function(column, value) {
				return `lower(${column.dadcCol}) = "${value.toLowerCase()}"`;
			},
			'text-tokens': function(column, value) {
				// Split on whitespace, remove all extra empty terms and terms that are less than three characters and match
				// all terms in any order

				//Originally added to omit any search term less than three characters
				//Removed but preserved
				//const terms = value.split(/[ ,]+/).filter(Boolean).filter(word => word.length >= 3);

				//Simply omitted the length filter
				const terms = value.split(/[ ,]+/).filter(Boolean);

				//This was added to make sure JS errors were not thrown if the terms var is empty
				if(terms.length > 0) {
					const tokenStatements = terms.map(term => `lower(${column.dadcCol}) like "%${term.toLowerCase()}%"`);
					return `(${tokenStatements.join(" and ")})`;
				}
				return "";
			},
			'text-tokens-ordered': function (column, value) {
				// Require all tokens in the same order. search "Jay Inslee" = "%jay%inslee%" matches "Jayleen R. Inslee Jr."
				const terms = value.split(/[ ,]+/).filter(Boolean).filter(word => word.length >= 3).map(term => term.toLowerCase());
				return `lower(${column.dadcCol}) like "%${terms.join("%")}%"`;
			},
			'like': function (column, value) {
				return `lower(${column.dadcCol}) like "%${value.toLowerCase()}%"`
			},
			'__default': function (column, value) {
				return `lower(${column.dadcCol}) like "%${value.toLowerCase()}%"`
			},
		},
		'number': {
			'number-range': function (column, value) {
				// The yadcf control provides a single value of the form n-yadcf_delim-n where n is a number, the empty string
				// or NaN. It is important that the empty string is not treated like 0. Unfortunately yadcf passes the string
				// of whitespace as 0 so nothing we can do about it.
				const components = value.split('-yadcf_delim-').map(item => {
					return (item === '' || !Number.isFinite(+item)) ? null : +item;
				})

				if (components[0] != null && components[1] != null) {
					return `${column.dadcCol} >= ${components[0]} and ${column.dadcCol} <= ${components[1]}`
				} else if (components[0] != null) {
					return `${column.dadcCol} >= ${components[0]}`
				} else if (components[1] != null) {
					return `${column.dadcCol} <= ${components[1]}`
				}
			},
			'__default': function (column, value) {
				return `${column.dadcCol} = ${value}`;
			},
		},
		'date': {
			'date-range': function (column, value) {
				const components = value.split('-yadcf_delim-').map(item => {
					return (item === '') ? null : '"' + moment(item).format('YYYY-MM-DDTHH:mm') + '"';
				})
				if (components[0] != null && components[1] != null) {
					return `${column.dadcCol} >= ${components[0]} and ${column.dadcCol} <= ${components[1]}`
				} else if (components[0] != null) {
					return `${column.dadcCol} >= ${components[0]}`
				} else if (components[1] != null) {
					return `${column.dadcCol} <= ${components[1]}`
				}
				return '';
			},
			'__default': function (column, value) {
				var formattedDate = moment(value).format('YYYY-MM-DDTHH:mm');
				return `${column.dadcCol} = "${formattedDate}"`;
			},
		}
	};

	function buildWhereStatementItem(searchValue, column) {
		let statements = [];
		const searchType = Array.isArray(column.dadcSearchType) ? column.dadcSearchType[0] : column.dadcSearchType;

		// Multi-select returns the terms pipe separated by default. When multi-select is used, the where statement item needs
		// to be wrapped in parens so for year: 2007|2008 it should produce (year=2007 or year=2008)
		searchValue.split("|").forEach((value) => {
			const builders = whereStatementBuilders;
			const dataType = column.dadcColType;

			let statement = "";
			if (builders[dataType]) {
				const builder = builders[dataType][searchType] ?? builders[dataType]['__default'];
				statement = builder(column, value);
			} else {
				statement = `lower(${column.dadcCol}) = "${value.toLowerCase()}"`
			}

			if (statement) {
				statements.push(statement);
			}
		});

		// If statements has one or more statement, wrap in parens and join with or
		if (statements.length) {
			return (statements.length > 1) ? ` and (${statements.join(" or ")})` : ` and ${statements[0]}`;
		}

		return '';
	}

	function getDataAttributesHavingPrefix(dataObject, prefix) {
    const attributes = {};
    for (const prop in dataObject) {
    	if (dataObject.hasOwnProperty(prop) && prop.indexOf(prefix) === 0) {
    		let propName = prop.replace(prefix, '')
    		propName = propName && propName[0].toLowerCase() + propName.slice(1);
    		let value;

				// attribute.value might be a string or possibly a json object, serialized and uri encoded.
				// The encoding is only needed because we are using forena and it overides the meaning of {}
				// which would normally appear in the json object serialized string. Using the exception for
				// flow control is stupid but there's now way to determine if the string is a serialized
				// object except to just try it. There's no C# like tryParse function in jquery.
				try {
					value = JSON.parse(dataObject[prop]);
				} catch (e) {
					value = dataObject[prop];
				}
				attributes[propName] = value;
			}
		}
		return attributes;
	}

	function getFilterDependencies() {
		const filterDependencies = {};
		Object.values(drupalSettings.pdc_api_tools?.filters ?? {}).forEach(function (value) {
			if (value.id && value.dependencies) {
				filterDependencies[value.id] = value.dependencies;
			}
		});
		return filterDependencies;
	}

	function isValuesMatch(a, b) {
		if (typeof a === 'string' && typeof b === 'string') {
			return a.toLowerCase() === b.toLowerCase();
		}
		if (a.length !== b.length) {
			return false;
		}
		for (let i = 0; i < a.length; i++) {
			if (!isInArray(a[i], b)) {
				return false;
			}
		}
		return true;
	}

	function isInArray(needle, haystack) {
		return haystack.indexOf(needle) !== -1;
	}
})(jQuery, Drupal, drupalSettings);
