(function($, Drupal, drupalSettings) {
    'use strict';
    Drupal.behaviors.address_tool = {
        attach: function(context, settings) {

            var code_select_observer = new MutationObserver(function (mutations, me) {
                // `mutations` is an array of mutations that occurred
                // `me` is the MutationObserver instance
                var jurisdiction_code_canvas = $('#yadcf-filter-wrapper--jurisdiction_code select');
                if (jurisdiction_code_canvas) {
                    $('#yadcf-filter-wrapper--jurisdiction_code div.chosen-container').addClass('hidden').attr('aria-hidden','true');
                    $('#yadcf-filter-wrapper--jurisdiction_code select').addClass('hidden').attr('aria-hidden','true');
                    me.disconnect(); // stop observing
                    return;
                }
            });
            // start observing
            code_select_observer.observe(document, {
                childList: true,
                subtree: true
            });
            var myDistrictsUi = {
                $textInput: $('input[name="address-search-input"]'),
                $errorMsg: $("#address-error"),
                $noResultsMsg: $("#address-no-results"),
                $spinner: $("#address-progress"),
            };

            addressAutoComplete();

            function addressAutoComplete() {
             myDistrictsUi.$textInput.autocomplete({
                    delay: 400,
                    source: function (request, response) {

                        //Hide the error message once they
                        //start typing again.
                        myDistrictsUi.$errorMsg.hide();
                        myDistrictsUi.$noResultsMsg.hide();
                        //myDistrictsUi.$spinner.progressbar( "destroy" );
                        myDistrictsUi.$spinner.hide();

                        var term = request.term;
                        
                        // Remove chars that can mess up searching
                        term = term.replace(/(,|\.|-|@|#|&|\(|\)|\+|\/)/g, ''); //,.-@#&()+/
                        term = term.replace(/( )(wa)$/i, '$1'); // removes wa from end of input

                        // Replace spaces with a url encoded % so searching
                        // "%513%ash%" would return "513 W Ash"
                        term = ($.trim(term)).replace(/ /g, "%25");

                        // Url to search the My Candidate Address dataset
                        var soql = "https://data.wa.gov/resource/maaf-qkua.json?$select=streetaddress,street_city," +
                        "regcity,precinctcode,levycode,countycode&$where=lower(streetaddress) " +
                        "like lower('%25" + term + "%25')&$limit=20" +
                        "&$order=regcity,street,regstnum";
                        var view_data;
                        $.ajax({
                            url: soql,
                            cache: false,
                            success:function(response_data_json) {
                                view_data = response_data_json;
                                findAddress(view_data); // Pass data to a function
                            }
                        });

                        function findAddress(data) {
                            // Array of Objects that are returned to the dropdown list.
                            // One entire object is passed to the selected callback.
                            var newData = [];
                            $(data).each(function (index, element) {
                                newData.push({
                                    'label': element.streetaddress,
                                    'value': element.streetaddress,
                                    'displayAddress': element.street_city,
                                    'precinctCode': element.precinctcode,
                                    'levyCode': element.levycode,
                                    'countyCode': element.countycode
                                });
                            });
                            if(newData.length === 0 && myDistrictsUi.$textInput.val() !== ''){
                                myDistrictsUi.$noResultsMsg.show();
                                //myDistrictsUi.$spinner.progressbar( "destroy" );
                            }
                            response(newData);
                        };
                    },
                    select: function (event, ui) {

                        //Build url to find jurisOfficeCodes office codes
                        var precinctCode = ui.item.precinctCode;
                        var levyCode = ui.item.levyCode;
                        var countyCode = ui.item.countyCode;
                        var myJurisUrl = "https://data.wa.gov/resource/gwv8-rk5b.json?$select=jurisdiction_code,office_code&" +
                        "$where=precinctcode='" + precinctCode + "' and levycode='" + levyCode + "' and countycode ='" + countyCode + "'";
                        var view_data = '';
                        $.ajax({
                            url: myJurisUrl,
                            cache: false,
                            success:function(response_data_json) {
                                view_data = response_data_json;
                                findJurisdiction(view_data); // Pass data to a function
                            }
                        });
                        function findJurisdiction(data) {
                            // Assemble the jurisOfficeCodes office code as
                            // an array of url params that forena understands
                            var juris_office_codes = '';
                            var jurisdiction_code_array = [];
                            var jurisdiction_code_array_unique = [];
                            var jurisdiction_codes_string = [];
                            $(data).each(function (index, value) {
                                jurisdiction_code_array.push(value.jurisdiction_code);
                            });
                            var jurisdiction_code_array_unique = jurisdiction_code_array.filter(function(item, i, code) {
                                return i == code.indexOf(item);
                            });
                            jurisdiction_code_array_unique.sort();
                            
                            var address_observer = new MutationObserver(function (mutations, me) {
                                // `mutations` is an array of mutations that occurred
                                // `me` is the MutationObserver instance
                                var canvas = document.getElementById('filter-tag-close--jurisdiction_code');
                                if (canvas) {
                                    if (myDistrictsUi.$textInput.val()) {
                                        $('#filter-tag-close--jurisdiction_code').prev().prev().text('Address');
                                        $('#filter-tag-close--jurisdiction_code').prev().text(myDistrictsUi.$textInput.val());
                                    }
                                    else {
                                        // Remove the tag if input was cleared before the
                                        // autocomplete results returned.
                                        $('#filter-tag-close--jurisdiction_code').parent().remove();
                                    }
                                    me.disconnect(); // stop observing
                                    return;
                                }
                            });

                            // start observing
                            address_observer.observe(document, {
                                childList: true,
                                subtree: true
                            });

                            juris_office_codes = JSON.stringify(jurisdiction_code_array);
                            var machineAddVal = myDistrictsUi.$textInput.val()
                                .toString()
                                .toLowerCase()
                                .replace(/[^A-Za-z0-9 ]/g,'')
                                .replace(/\s{2,}/g,' ')
                                .replace(/\s/g, "-");
                            if ($('.current-filter-container').hasClass('hide')) {
                                $('.current-filter-container').addClass('show'); 
                                $('.current-filter-container').removeClass('hide'); 
                            }
                            $(jurisdiction_code_array_unique).each(function(i,val){
                                $('#jurisdiction_code select option[value="' + val + '"]').prop('selected', true)
                            });
                            $('#jurisdiction_code select').trigger('change');

                            
                        };
                    }
                });
            }
        }
    }
})(jQuery, Drupal, drupalSettings);
