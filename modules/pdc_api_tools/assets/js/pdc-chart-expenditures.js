// A one-off version of the bar chart specifically for the expenditures chart,
// as it has specific tweaks that are not fit for the generic bar chart.

((Drupal, once, $, Chart, PDC) => {

  Drupal.behaviors.pdc_chart_expenditures= {
    attach(context, settings) {
      const allChartsSettings = settings.pdc_chart_expenditures;
      $(once('pdc_chart_expenditures', '[data-pdc-chart="expenditures"]'))
        .find('.pdc-chart-table')
        .each((i, element) => {
          const table = $(element).addClass('visually-hidden');
          const wrapper = table.parent();
          const chartId = wrapper.data('pdc-chart-id');
          const {title, headings, rows, chart_options} = allChartsSettings[chartId];
          const labels = rows.map(r => r[0]).filter(r => Boolean(r));
          const labelless = labels.length === 0;

          // The data is structured and rendered in HTML in a row-wise fashion.
          // But for charts, it's column-wise by dataset. Also, the data is
          // expected to be a full table (i.e. with row and column headings)
          // This means the data actually starts at (1,1) instead of (1, 0).
          const datasets = headings.slice(1).map((heading, i) => ({
            label: heading,
            backgroundColor: PDC.chartColors[i],
            data: [...rows.map(r => r[i + 1]), null]
          }));

          const legendContainer = $('<div />')
            .appendTo(wrapper)
            .addClass('pdc-chart-legend-container')
            .get(0);

          const dedicatedCanvasWrapper = $('<div />')
            .appendTo(wrapper)
            .addClass('pdc-chart-canvas-container');

          const canvas = $('<canvas />').appendTo(dedicatedCanvasWrapper);
          const context = canvas.get(0).getContext('2d');

          // Chart.js does not show data if the labels are not supplied. For
          // unlabelled data (e.g. independent expenditures), just supply a
          // blank label.
          const finalLabels = labelless ? rows.map(() => '') : labels;

          new Chart(context, {
            type: "bar",
            data: {
              labels: [...finalLabels, ''],
              datasets: datasets
            },
            plugins: [
              PDC.pdcHtmlTitlePlugin,
              PDC.pdcHtmlLegendPlugin
            ],
            options: $.extend(true, {}, chart_options, {
              responsive: true,
              barPercentage: 0.5,
              maintainAspectRatio: false,
              scales: {
                y: {
                  beginAtZero: true,
                  ticks: {
                    mirror: true,
                    labelOffset: 18,
                    font: {
                      weight: 'bold',
                    }
                  },
                  grid: {
                    lineWidth: 0
                  }
                }
              },
              indexAxis: 'y',
              plugins: {
                tooltip: {
                  enabled: false
                },
                legend: {
                  display: false
                },
                pdcHtmlTitle: {
                  container: wrapper,
                  title: title,
                },
                pdcHtmlLegend: {
                  container: legendContainer,
                  onItemToggle(chart, item) {
                    chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
                  },
                  getLegendItemInfo(chart, item, options) {
                    const {text, fillStyle: color, datasetIndex} = item;

                    // One-off implementation for independent expenditures.
                    // Show the first value of the dataset as labels.
                    const value = options.showValueOnLabel
                      ? `$${datasets[datasetIndex].data[0].toLocaleString('en-US', {minimumFractionDigits: 2})}`
                      : '';

                    return {text, color, value}
                  }
                }
              }
            })
          });
        });
    }
  };
})(Drupal, once, jQuery, Chart, PDC);
