(function($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.pdcApiToolsViewsExposedForm = {
		attach: function(context, settings) {
			function multiselectToggleSelected(e) {
				let selectedCount = 0;
				let btnGroup = e.closest('.btn-group');
				let inputs = btnGroup.find('input');
				let button = btnGroup.find('button');
				inputs.each(function(index, input) {
					if ($(input).prop('checked')) {
						selectedCount++;
					}
				});
				if (selectedCount) {
					button.addClass('selected');
				}
				else {
					button.removeClass('selected');
				}
			}

			function selectToggleSelected(e) {
				if (e.val() === 'All') {
					e.removeClass('selected');
				}
				else {
					e.addClass('selected');
				}
			}

			let buttonGroupInputs = $('.views-exposed-form .btn-group input', context);
			let selectInputs = $('.views-exposed-form select', context);

			// Toggle selected class on select elements on load & change.
			selectInputs.each(function() {
				let e = $(this);
				selectToggleSelected(e);
				e.on('change', function() {
					selectToggleSelected(e);
				});
			});

			// Toggle selected class on multiselect button elements on load & change.
			buttonGroupInputs.each(function(index, buttonGroupInput) {
				let e = $(buttonGroupInput);
				multiselectToggleSelected(e);
				e.on('change', function() {
					multiselectToggleSelected(e);
				});
			});
		}
	};
})(jQuery, Drupal, drupalSettings);