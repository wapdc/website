(function(PDC, {pdcApiTools: apiConfig }) { 
  /**
   * Fetch data from Socrata dataset.
   * 
   * @param {string} dsid 
   *   The dataset ID to query the Socrata data.
   * @param {URLSearchParams|string} query
   *   The query to perform on the dataset formatted as URL query parameters.
   * @param {*} options
   *   Request and fetch options, including headers, signals and timeout.
   */
  PDC.sodaQuery = async function(dsid, query, options = apiConfig) {
    let response;
    let url = `${apiConfig.schema}://${apiConfig.endpointUri}/${dsid}.json?${String(query)}`;
    
    const timeout = options.timeout ?? 5000;
    // Use timeout if value is non-zero / non-falsy value.
    if (timeout) {
      const ctrl = new AbortController();
      const tid = setTimeout(() => ctrl.abort('SODA request timeout exceeded.'), timeout);
      
      // Make the fetch request with a timeout abort attached.
      options.signal = ctrl.signal;
      response = await fetch(url, options).finally(() => clearTimeout(tid));
    }
    else {
      response = await fetch(url, options);
    }
    
    if (!response.ok) {
      throw new Error(`Response: ${response.status} - ${response.statusText}`);
    }
    
    return await response.json();
  }
})(window.PDC = window.PDC || {}, drupalSettings);
