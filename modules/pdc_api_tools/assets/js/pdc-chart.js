((PDC, $) => {

  PDC.chartColors = [
    '#006800',
    '#e49600',
    '#383f59',
    '#b11e2c',
    '#767676',
    '#bfbfbf',
    '#38853c',
  ];

  const legendItem = ({ chart, item, color, text, value, onItemToggle }) => {
    const itemDom = $(`
        <div class="pdc-chart-legend-text">${text}</div>
        ${value ? `<div class="pdc-chart-legend-value">${value}</div>` : '' }
    `);
    itemDom.css('--pdc-chart-legend-color', color);
    
    $('.pdc-chart-legend-text', itemDom)
      .css('text-decoration', item.hidden ? 'line-through' : '');

    return itemDom;
  };

  PDC.pdcHtmlLegendPlugin = PDC.pdcHtmlLegendPlugin ?? {
    id: 'pdcHtmlLegend',
    defaults: {
      container: undefined,
      onItemToggle: () => {},
      getLegendItemInfo: () => {},
      showValueOnLabel: false,
    },
    afterRender(chart, args, options) {
      if (chart.pdcHtmlLegend === true) return;
      chart.pdcHtmlLegend = true;

      const {container, onItemToggle, getLegendItemInfo, showValueOnLabel} = options;
      const items = chart.options.plugins.legend.labels.generateLabels(chart);

      $('<ul />')
        .addClass('pdc-chart-legend')
        .appendTo(container)
        .append(items.flatMap(item => {
          const legendInfo = getLegendItemInfo(chart, item, options);

          return !legendInfo ? [] : [
            $('<li />')
              .addClass('pdc-chart-legend-item')
              .append(legendItem({chart, item, onItemToggle, ...legendInfo}))
          ]
        }))

    },
    afterUpdate(chart, args, options) {
      const {container} = options;
      const items = chart.options.plugins.legend.labels.generateLabels(chart);

      $('.pdc-chart-legend-text', container).each((i, e) => {
        $(e).css('text-decoration', items[i].hidden ? 'line-through' : '')
      });
    }
  };

  PDC.pdcHtmlTitlePlugin = PDC.pdcHtmlTitlePlugin ?? {
    id: 'pdcHtmlTitle',
    afterRender(chart, args, options) {
      if (chart.pdcHtmlTitle === true) return;
      chart.pdcHtmlTitle = true;

      const {container, title} = options;

      $('<h2 />')
        .text(title)
        .addClass('pdc-chart-title')
        .prependTo(container);
    }
  };

})(window.PDC = window.PDC ?? {}, jQuery);
