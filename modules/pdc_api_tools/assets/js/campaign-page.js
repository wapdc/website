(function($, PDC, {t}, drupalSettings) {
  const loadingSpinner = (show = true) => {
    Drupal.pdcModule.loadingSpinner('loading-spinner', show);
  };
  loadingSpinner(true);

  // Prefer to get the ID from the URL pattern is possible. This allows us to
  // cache the page settings and definitions fully, and perform specific data
  // fetching on the frontend for any candidate or committee from the URL.
  const {pdcCampaignPage: pageDef} = drupalSettings;
  const content = document.getElementById('campaign-data-overview');

  async function getReportingInfo(id, year) {
    const url = `https://data.wa.gov/resource/3h9x-7bvm.json?committee_id=${id}`;
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Response status: ${response.status}`);
    }

    const allCommittees = await response.json();

    return allCommittees.map(committee => ({
      url: `/political-disclosure-reporting-data/browse-search-data/committees/${committee.id}`,
      election_year: committee.election_year
    }));
  }

  if (pageDef.path) {
    const match = RegExp(`^${pageDef.path.replace('{id}', '([^/]+)')}$`, 'i').exec(window.location.pathname);
    if (match) {
      pageDef.id = match[1];
      const committeeId = match[1].substring(match[1].lastIndexOf('-') + 1);
      const committeeYear =match[1].substring(match[1].indexOf('-') + 1, match[1].lastIndexOf('-'));
      getReportingInfo(committeeId, committeeYear)
      .then(committeeInfo => {
        const options = committeeInfo
            .map(info => ({
              year: info.election_year,
              url: info.url
            }))
            .sort((a, b) => b.year - a.year);

        const dropdown = $(`<select class="campaign-year"></select>`);
        dropdown.css('display', 'inline-block');

        dropdown.on('change', function() {
          const selectedOption = $(this).find(':selected');
          const selectedUrl = selectedOption.data('url');
          if (selectedUrl) {
            window.location.href = selectedUrl;
          }
        });

        function populateDropdown(dropdown, options, committeeYear) {
          options.forEach(option => {
            $('<option></option>')
              .text(option.year)
              .prop('value', option.year)
              .data('url', option.url)
              .appendTo(dropdown);
          });
          dropdown.val(committeeYear);
        }

        populateDropdown(dropdown, options, committeeYear);
        $('.campaign-title .campaign-year').replaceWith(dropdown);
      })
      .catch(error => {
        console.error('Failed to load campaign finance data:', error);
      });
    }
  } else {
    pageDef.path = window.location.pathname;
  }

  // Initial request to verify this is a valid candidate and also fetch the
  // general overview data needed to populate the basic candidate info.
  params = new URLSearchParams();
  params.set(pageDef.field, pageDef.id)

  PDC.sodaQuery(pageDef.dsid, params)
    .then(([info]) => {
      if (!info || !campaignType[pageDef.type]) {
        throw new Error('No campaign finance filing information found.');
      }

      // Calculate the totals display value.
      info.chartData = {};
      info.url = (info.url||{}).url;
      info.total_raised_amount = parseFloat(info.carryforward_amount||0) + parseFloat(info.contributions_amount||0) + parseFloat(info.loans_amount||0);

      // Ensure value is a float for accurate empty checks.
      info.pledges_amount = parseFloat(info.pledges_amount||0);

      // Create a pre-formatted version of the address.
      let formatAddr = [info.committee_city, info.committee_state].filter(Boolean).join(', ');
      if (info.committee_zip) {
        formatAddr += (formatAddr.length ? ' ' : '') + info.committee_zip;
      }
      if (info.committee_address) {
        formatAddr = formatAddr.length ? `${info.committee_address}<br>${formatAddr}` : info.committee_address;
      }
      info.committee_formatted_address = formatAddr.length ? formatAddr : null;

      // Process the campaign data based on the type (candidate or committee).
      campaignType[pageDef.type](info, content, pageDef);

      const header = $('.campaign-title');
      header.find('.campaign-name').text(info.filer_name);
      header.find('.campaign-blurb').text(info.campaign_blurb);

      const siteName = /\s*\|\s*([^|]+)$/i.exec(document.title)[1]||'';
      document.title = `${info.filer_name} - ${info.election_year} | ${siteName}`;
      $('.breadcrumb li.active').text(`${info.filer_name} - ${info.election_year}`);

      // Initial campaign data loaded, we can start populating pages and
      // initializing follow-up data requests.
      const activeTab = (window.location.hash||'overview').replace(/^#/, '');
      drupalSettings.pdcCampaignData = info;
      displayTab(pageDef, activeTab);
    })
    .catch(error => {
      document.title = `${pageDef.label} not found`;
      $('.campaign-title .campaign-name').text(`${pageDef.label} not found`);
      $('.campaign-finance__tabs').hide();

      loadingSpinner(false);
    });

  // Check for and build campaign page tabs. Start building these while we wait
  // for the candidate info to be retrieved from the Socrata data requests.
  pageDef.subpages = {
    overview: {title: Drupal.t('Overview'), content},
    ...pageDef.subpages||{},
  }

  const tabs = Object.entries(pageDef.subpages);
  if (tabs.length > 1) {
    let activeTab = (window.location.hash||'overview').replace(/^#/, '');

    const tabListEl = $('<div class="campaign-finance__tabs tabs nav-tabs" role="tablist"></div>').appendTo('#campaign-data-navigation');
    const tabPanelEl = $(content).wrap('<div class="campaign-finance__panels"></div>').parent();

    // Create a tab switcher mobile responsive sizes.
    const switcherId = 'campaign-page-tab-switcher';
    const tabSwitcher = $(`<select id="${switcherId}" aria-controls=""></select>`);
    $(`<div class="campaign-finance__tab-switcher container"><label id="${switcherId}-label" for="${switcherId}">${Drupal.t(`View ${pageDef.label.toLowerCase()}:`)}</label></div>`)
      .appendTo('#campaign-data-navigation')
      .append(tabSwitcher);

    // Add the overview tab and render the tabs.
    tabs.forEach(([key, tabDef]) => {
      const isActive = (key === activeTab);
      const idPrefix = `campaign-finance-${key}`;

      // Capture the initial search query for the active tab.
      if (key === activeTab) {
        tabDef.query = window.location.search;
      }

      // Create the content panel.
      const panelHtml = `<div id="${idPrefix}-tabpanel" aria-labelledby="${idPrefix}-tab" tabindex="0" role="tabpanel" hidden></div>`;
      tabDef.panel = tabDef.content ? $(tabDef.content).wrap(panelHtml).parent() : $(panelHtml);

      // Add tab to the drop-down selector (for mobile).
      tabSwitcher.attr('aria-controls', `${tabSwitcher.attr('aria-controls')} ${idPrefix}-tabpanel`);
      tabSwitcher.append(`<option value="${key}">${Drupal.t(tabDef.title)}</option>`);

      // Add the tab and panel to the panel containers.
      tabDef.ctrl = $(`<button id="${idPrefix}-tab" class="tab" data-tab-id="${key}" aria-controls="${idPrefix}-tabpanel" aria-selected="${isActive}" role="tab">${Drupal.t(tabDef.title)}</button>`);
      tabDef.ctrl.on('click', (e) => window.location.hash = $(e.target).data('tabId'));
      tabDef.ctrl.appendTo(tabListEl);
      tabDef.panel.appendTo(tabPanelEl);
    });

    // When value of the tab switcher changes, swap the active tab.
    tabSwitcher.val(activeTab);
    tabSwitcher.on('change', (e) => {
      window.location.hash = $(e.target).val();
    });

    // Internal links may also trigger campaign page tab to switch.
    window.addEventListener('hashchange', (e) => {
      const [, activeTab]= (/#([^&?]+)/.exec(e.oldURL)||[,'overview']);
      const hash = (window.location.hash|| 'overview').replace(/^#/, '');

      // If tab was actually changed, switch the displayed tab.
      if (activeTab !== hash) {
        tabSwitcher.val(hash);
        displayTab(pageDef, hash, activeTab);
      }
    });
  }

  /**
   * Make the tab panel identified by "tabKey" active and visible.
   *
   * This includes fetching the subpage contents if they have not already been
   * loaded. This function updates the tab state and ensures the campaign data
   * is applied and Drupal behaviors are attached.
   *
   * @param {Object} pageDef
   *   The page configurations including the subpage settings.
   * @param {string} tabKey
   *   The identifier for target tab panel to load, enable and display.
   */
  function displayTab(pageDef, tabKey) {
    if (!drupalSettings.pdcCampaignData) {
      // Don't attempt to render pages until the base campaign data has loaded.
      return;
    }

    const {id, path, subpages} = pageDef;
    const {pathname, search, hash} = window.location;
    // Method to enable and disable initialize datatables.
    const {setDatatableState} = Drupal.behaviors.pdcDatatable;

    // Disable the current active tab and hide the panel.
    $('#campaign-data-navigation .tab.tab--active').each((i, tab) => {
      const tabId = tab.dataset.tabId;
      const panel = $(`#${$(tab).attr('aria-controls')}`).attr('hidden', 'hidden');

      // Disable any datatables on this panel so it doesn't react to changes
      // to the URL parameters while other tabs are being used.
      panel.find('table.dataTable').each((i, table) => setDatatableState(table, false));
      $(tab).removeClass('tab--active').attr('aria-selected', false);

      if (tabId && subpages[tabId]) {
        subpages[tabId].query = search;
      }
    });

    // Make the target tab visible and load the content as needed.
    if (subpages[tabKey]) {
      const tab = subpages[tabKey];
      tab.ctrl.attr('aria-selected', true).addClass('tab--active');
      tab.panel.removeAttr('hidden');

      // Restore the datatable filter states for the tab if used.
      const queryStr = subpages[tabKey].query ?? '';
      if (queryStr !== search) {
        window.history.pushState(queryStr, '', `${pathname}${queryStr}${hash}`);
      }

      if (!tab.content) {
        loadingSpinner(true);
        const pageUri = path.replace(/\/\{id\}(\/|$)/i, (m, c) => `/${id + c}`);

        // Load subpage from Drupal route
        tab.content = fetch(`${pageUri}/${tabKey.replace('_', '-')}?_format=json`)
          .then(async response => {
            if (response.ok) {
              const settings = await response.json();
              tab.panel.html(settings.content);

              await loadPageContent(drupalSettings.pdcCampaignData, tab.panel.get(0), settings.data_block_settings);
              tab.init = true;
            }
          })
          .catch(() => loadingSpinner(false));
      }
      else if (!tab.init && tab.content instanceof Element) {
        loadPageContent(drupalSettings.pdcCampaignData, tab.content);
        tab.init = true;
      }
      else {
        // Enable any datatables on the tab which is being set as active.
        tab.panel.find('table.dataTable').each((i, table) => setDatatableState(table, true))
      }
    }
  }

  /**
   * Applies campaign finance data and attaches Drupal behaviors to the content.
   *
   * After the HTML template is available, we need to apply the campaign finance
   * data to data-value elements, load data blocks and render charts & tables.
   * This function prepares the content and attaches the Drupal behaviors.
   *
   * @param {Object} data
   *   The candidate or committee campaign finance data.
   * @param {Element} context
   *   The HTML element to apply the data and behaviors to.
   * @param {Object} blockSettings
   *   Any data block settings for subpage to use when building subpages.
   */
  async function loadPageContent(data, context, blockSettings) {
    // Keep track of any pending promises, so we can wait for them at the end
    // before attaching Drupal behaviors to the content.
    const fetches = [];

    // Conditionally load and build subpage data block.
    if (blockSettings) {
      const fetch = buildSubPageBlock(data, blockSettings, context);
      if (fetch) {
        fetches.push(fetch);
      }
    }

    // Format the campaign status items if present.
    if (data.campaignStatus && data.campaignStatus.length) {
      $('.campaign-status-callout .candidate-status-items', context).each((i, list) => {
        data.campaignStatus.forEach(item => {
          $(list).prepend($(`<li>${t('Candidate @title @date', {
            '@title': item[0],
            '@date': moment(item[1]).format('(M/D/YYYY)'),
          })}</li>`));
        });
      });
    }

    // Replaced fixed filter placeholder with the candidate data field values.
    $('.campaign-reports-table', context).each((i, table) => {
      if (table.dataset.dadcFixedFilter) {
        table.dataset.dadcFixedFilter = table.dataset.dadcFixedFilter.replace(/\{(\w+)\}/g, (m, did) => data[did]);
      }
      table.classList.add('dataTable');
    });

    // Remove all blocks and rows that are marked as dependent on a campaign
    // "fund_id" value should be removed if the "fund_id" is not defined. This
    // is done at the start to avoid loading data that isn't relevant.
    if (undefined === data.fund_id || null === data.fund_id) {
      $('.campaign-fund-dependent', context).remove();
    }

    // Fetch and populate any data charts.
    once('chart-data', '.campaign-data-chart', context).forEach((el) => {
      const conf = el.dataset;

      // Check if this data has already been populated, create a fetch request
      // to get it if it is not already available.
      const dataReq = (conf.set && data.chartData[conf.set])
        ? new Promise((resolve) => resolve(data.chartData[conf.set]))
        : getChartData(data, conf).then((rows) => (data.chartData[conf.set] = rows));

      // When the chart data is available, build the display tables and prepare
      // the chart for PDC chart rendering behaviors.
      dataReq.then((rows) => buildChart(el, rows, conf));
      fetches.push(dataReq);
    });

    // Build the opponents table.
    once('opponent-table', '#opponents-datatable .table', context).forEach((el) => {
      const loadOpps = getOpponents(data);

      // Conditionally alter the title if position data is being used.
      if (data.position) {
        const card = $(el).closest('.pdc-card');
        card.find('.pdc-card-title').text(t('Candidates Filed for this Position'));
        card.find('.pdc-chart-title').text(t('Candidates filed for this position'));
      }

      loadOpps.then((rows) => {
        el.classList.add('dataTableAlt');

        const body = $('tbody', el);
        rows.forEach((row) => body.append(
          `<tr>
            <td>
              <a href="/political-disclosure-reporting-data/browse-search-data/candidates/${row.candidacy_id}">${row.filer_name}</a>
              ${(row.declared && !(row.discontinued || row.withdrew)) ? '<i class="candidacy-icon fa fa-solid fa-asterisk"></i>':''}
            </td>
            <td>${toCurrency(row.contributions_amount)}</td>
            <td>${toCurrency(row.expenditures_amount)}</td>
          </tr>`
        ));
        return rows;
      });
      fetches.push(loadOpps);
    });

    // Bind fill in campaign data to items marked have attached data values.
    once('campaign-data', '.campaign-data-value', context).forEach((el) => {
      const {valueId: dataId, format = 'text'} = el.dataset;

      if (campaignDataFormatter[format] && (!el.dataset.hideEmpty || data[dataId])) {
        campaignDataFormatter[format](el, data[dataId]);
      } else {
        // Hide data elements with unknown formatters or are marked as hidden when empty.
        if ('parent' === (el.dataset.hideEmpty || 'self')) {
          el = el.parentElement;
        }
        el.style.display = 'none';
      }
    });

    // ballot_proposal_detail is expected to be JSON or an array.
    // Parse the JSON if it is a "string", otherwise have data use the ballot
    // data if it is an non-empty array.
    let ballotProposals = data.ballot_proposal_detail;
    if (typeof ballotProposals === 'string' || ballotProposals instanceof String) {
      // Ensure the JSON is an array. If it ends up empty, also abort.
      try {
        ballotProposals = JSON.parse(ballotProposals);
      } catch (e) {
        ballotProposals = [];
      }
    }

    // Ensure the ballot data is an array and not empty.
    if (Array.isArray(ballotProposals) && ballotProposals.length) {
      // Bind ballot proposition data
      once('ballot-proposition', '.ballot-proposals-callout', context).forEach((section) => {
          // Update the rendered header with one that contains the year.
          section.querySelectorAll('.callout-title-heading').forEach(heading => {
            heading.textContent = t('Ballot Measures Supported or Opposed for @year', {
              '@year': data.election_year,
            });
          });

          section.querySelectorAll('.callout-content').forEach(content => {
            ballotProposals.forEach(p => {
              const stance = {
                "against": t("Opposed"),
                "for": t("Supported")
              };
              const heading = stance[p.stance] ?? p.stance;
              const title = p.title;
              const number = p.number ? t('(@n)', {'@n': p.number}) : '';
              const jurisdiction = p.jurisdiction ? t('@j', {'@j': p.jurisdiction}): '';
              const text = [title, number].filter(Boolean).join(' ')
              const type = p.proposal_type ? `<span class="callout-card-type">${p.proposal_type}</span>` : '';
              $(`
                <li class="callout-card-group-item col-xs-12 col-sm-6 pdc-mt-4">
                  <div class="callout-card">
                    <div class="callout-card-content">
                      <h3 class="callout-card-title">${heading}</h3>
                      <p class="callout-card-text">${text}</p>
                      <p class="callout-card-jurisdiction">${jurisdiction}</p>
                    </div>
                    <div>
                      ${type}
                    </div>
                  </div>
                </li>
              `).appendTo(content);
            });
          });

      });
    } else {
      $('.ballot-proposals-callout', context).remove();
    }

    // Wait for any pending fetches to complete and charts to be setup before
    // attaching the Drupal behaviors.
    if (fetches.length) {
      await Promise.all(fetches);
    }

    Drupal.attachBehaviors(context, drupalSettings);
    loadingSpinner(false);
  }

  /**
   * Build and load subpage data blocks.
   *
   * @param {Object} data
   *   The candidate or committee campaign finance data.
   * @param {Object} settings
   *   The subpage data block settings.
   * @param {Element} context
   *   The HTML element which contains the subpage content being rendered.
   *
   * @returns {Promise|null}
   *   If the data block has a query that fetches additional data, then an
   *   promise for the query fetch is returned, otherwise NULL.
   */
  async function buildSubPageBlock(data, settings, context) {
    const {query, depend_fields, process_result = 'addHeaders'} = settings;
    const block = $('.subpage-data-block', context);
    const setEmpty = function() {
      block.parent().append($(`<div class="no-results"><h2>${settings.empty_message||t('No data currently available.')}</h2></div>`));
      block.remove();
    };

    // Display of the data block is conditional on fields having data, check
    // that the conditions are met and hide the block accordingly.
    if (depend_fields) {
      let hasData = false;
      for (let field of depend_fields) {
        if (data[field] && parseFloat(data[field])) {
          hasData = true;
          break;
        }
      }
      // No data dependencies, set the empty message and exit.
      if (!hasData) {
        setEmpty();
        return; // Block is hidden, we're done.
      }
    }

    // The datablock includes a query to fetch itemized data.
    if (query) {
      const select = [query.field];
      if (query.sum) {
        select.push(`sum(${query.sum})`);
      }

      const filters = [
        `filer_id="${data.filer_id}"`,
        `election_year=${data.election_year}`,
        query.where,
      ].filter(Boolean);

      let string = `select ${select.join(',')} where ${filters.join(' and ')}`
        + (query.group ? ` group by ${query.group}` : '')
        + (query.sort ? ` order by ${query.sort}` : '')

      const resultsCb = subpageQueryResultHandlers[process_result];

      return PDC.sodaQuery(query.dsid, new URLSearchParams({'$query': string + ` limit ${query.limit||9999}`})).then(
        (rows) => {
          if (rows.length) {
            resultsCb(block, query, rows);
          } else {
            setEmpty();
          }
          return rows;
        },
        () => {
          // Hide datablock on error / no data response.
          setEmpty();
          return [];
        }
      );
    }
  }

  /**
   * Fetches data for display on a chart.
   *
   * @param {object} data
   *   Loaded candidate or committee information.
   * @param {object} conf
   *   The chart configurations which should include dataset ID, and filters to
   *   build the data fetch query from.
   *
   * @return {array}
   *   Rows of data formatted for use with the bar and pie chart displays.
   */
  async function getChartData(data, conf) {
    // Opponents is a special case.
    if ('opponents' === conf.set) {
      const values = await getOpponents(data);
      return values.slice(0, conf.buckets||5).map((opp) => [
        opp.filer_name,
        opp.contributions_amount,
        opp.expenditures_amount,
      ]);
    }

    const groups = [];
    if (conf.dsid) {
      const {field, sumField, buckets = 1000000} = conf;
      const chartData = await PDC.sodaQuery(conf.dsid, new URLSearchParams({
        '$query': `select ${field}, sum(${sumField}) where filer_id='${data.filer_id}' and election_year=${data.election_year} group by ${field} order by sum(${sumField}) desc`,
      }))
        .catch((err) => {
          console.error(err);
          return [];
        });

      if (chartData) {
        let index = 0;
        let aggrTotal = 0;

        Object.entries(chartData).forEach(([k, v]) => {
          if (index++ < (buckets - 1)) {
            let name = v[field] || 'Not Provided';

            if (v[field]) {
              let words = name.split(' ', 5);
              if (words.length > 4) {
                words.pop();
                name = words.join(' ');
                if (name.length > 30) {
                  words.pop();
                  name = words.join(' ');
                }
                name += '...';
              }
            }
            groups.push([name, parseFloat(v.sum_amount)]);
          } else {
            aggrTotal += parseFloat(v.sum_amount);
          }
        });

        // If we've exceeded the buckets limit, or have an aggregated value, we
        // need to make room for it by trimming the extra data groups.
        if (aggrTotal || groups.length > buckets) {
          let misc = (aggrTotal ?? 0) + groups.splice(buckets - 2).reduce((p, c) => p + c[1], 0);
          groups.push(['Miscellaneous', misc]);
        }
      }
    }
    return groups;
  }

  /**
   * Build the pie or bar chart when using the fetch data rows.
   *
   * @param {Element} el
   *   The HTML Element to build the chart display from.
   * @param {array} rows
   *   The data rows to populate the chart with.
   * @param {object} conf
   *   Chart configurations, such as type and options.
   */
  function buildChart(el, rows, conf) {
    if (rows.length) {
      const chartId = el.dataset.pdcChartId;
      const type = el.dataset.chartType;
      const tbody = $(el).find('.pdc-chart-table tbody');

      // Populate the table cells for accessibility. The data is readable
      // from a table, even if the chart is not available.
      rows.forEach(([name, ...values]) => {
        const rowLabel = name ? `<td class="pdc-chart-label">${name}</td>` : '';
        tbody.append(
          `<tr class="pdc-chart-row">
            ${rowLabel}
            ${values.reduce((p, val) => p + `<td class="pdc-chart-value">${val.toFixed(2)}</td>`, '')}
          </tr>`
        );
      });

      let chart_options = {};
      if (conf.legendValues) {
        // If the chart is set for display values in the legend.
        chart_options.plugins = {pdcHtmlLegend: {showValueOnLabel: true}};
      }

      drupalSettings[`pdc_chart_${type}`][chartId] = {rows, chart_options};
      if ('bar' === type) {
        // Check if the first column of the row data is empty, indicates an
        // empty header item as the first column.
        const headings = rows[0][0] ? [] : [''];
        $(el).find('.pdc-chart-table thead th').each((i, th) => headings.push(th.textContent));
        drupalSettings[`pdc_chart_${type}`][chartId].headings = headings;
      }

      // Apply data so the charting library is able to render the chart.
      el.dataset.pdcChart = type;
      $(el).addClass('pdc-chart').find('.pdc-chart-empty-message').remove();
    } else {
      // No chartable data, keep the empty message and remove table.
      $(el).find('.pdc-chart-table').remove();
    }
  }

  /**
   * Create and executes a query to fetch the list of opponents for this
   * candidate ordered by contributions in descending order.
   *
   * List includes the candidate.
   *
   * @param {object} data
   *   The base campaign finance data.
   *
   * @returns {Promise}
   *   An array of oppenents data (can include candidate). Each opponent object
   *   includes the "candidacy_id", "filer_name", "contributions_ammount", and
   *   "expenditures_amount".
   */
  function getOpponents(data) {
    if (!data.opponents) {
      const select = ['candidacy_id', 'filer_name', 'contributions_amount', 'expenditures_amount', 'declared', 'discontinued', 'withdrew'];
      const filters = [
        `jurisdiction_code='${data.jurisdiction_code}'`,
        `office_code='${data.office_code}'`,
        `election_year=${data.election_year}`,
      ];

      // Position filter is conditionally applied.
      if (data.position) {
        filters.push(`position='${data.position}'`);
      }

      const query = new URLSearchParams({'$query': `select ${select.join(',')} where ${filters.join(' and ')} order by contributions_amount desc`});
      data.opponents = PDC.sodaQuery(drupalSettings.pdcCampaignPage.dsid, query).then(
        (rows) => {
          rows.forEach((row) => {
            row.contributions_amount = parseFloat(row.contributions_amount||0);
            row.expenditures_amount = parseFloat(row.expenditures_amount||0);
          });
          return rows.sort((a, b) => b.contributions_amount - a.contributions_amount);
        },
        () => [], // Failed to load data, set to empty data / no results.
      );
    }
    return data.opponents;
  }

  /**
   * Consistent formatting for a float value to a currency string.
   *
   * @param {float} value
   *   The value to format into the currency string.
   *
   * @returns {string}
   *   Formatted currency string in US dollar format.
   */
  function toCurrency(value) {
    return `\$${parseFloat(value||0).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2})}`;
  }

  /**
   * Functions to transforming campaign info into a displayable format.
   */
  campaignDataFormatter = {
    'text': function(el, value) {
      el.textContent += value;
    },
    'currency': function(el, value) {
      el.textContent = toCurrency(value||0);
    },
    'email_link': function(el, value) {
      el.innerHTML = `<a href="mailto:${value}">${value}</a>`;
    },
    'html': function(el, value) {
      el.innerHTML = value;
    },
    'token': function(el, value) {
      const data = drupalSettings.pdcCampaignData || {};
      el.innerHTML = el.innerHTML.replace(/\{(\w+)\}/g, (m, did) => data[did]);
    },
    'date': function(el, value) {
      const format = el.dataset.dateFormat||'MM/DD/YYYY';
      this.text(el, moment(value).format(format));
    },
    'date_full': function(el, value) {
      this.text(el, moment(value).format('MMMM D, YYYY'));
    }
  }

  /**
   * Different campaign page processors keyed by campaign page type.
   */
  campaignType = {
    candidate: function(info, content, pageDef) {
      info.campaign_blurb = `Candidate for ${info.office} for ${info.jurisdiction} in the ${info.election_year} election.`;

      // Data needed for the indendent expenditures chart is already available,
      // store it here so it's available if a chart needs it.
      info.chartData.independent_expenditures = [];
      if (info.independent_expenditures_for_amount || info.independent_expenditures_for_amount) {
        info.chartData.independent_expenditures.push([
          '',
          parseFloat(info.independent_expenditures_for_amount||0),
          parseFloat(info.independent_expenditures_against_amount||0),
        ]);
      }

      // Find and sort the candidate campaign statuses.
      info.campaignStatus = [];
      ['registered', 'declared', 'discontinued', 'withdrew'].forEach(prop => {
        if (info[prop]) info.campaignStatus.push([prop, info[prop]]);
      });
      info.campaignStatus.sort((a, b) => b[1].localeCompare(a[1]));

      // If this candidate opted for mini-reporting, add the tooltip.
      if ('mini' == info.reporting_option) {
        $('.finance-overview-callout .callout-title', content).append(
          `<div class="callout-tooltip" data-toggle="tooltip" data-original-title="${t('Campaigns that raise and spend no more than $7,000 can register under mini reporting and do not have to report contributions and expenditures to the PDC.')}">
            <span class="tooltip-text">${t('Candidate opted for <strong>mini reporting</strong>')}</span>
          </div>`
        );
      }
    },
    committee: function(info, content, pageDef) {
      info.campaign_blurb = '';
      // Create a pre-formatted version of the address.
      let formatAddr = [info.treasurer_city, info.treasurer_state].filter(Boolean).join(', ');
      if (info.committee_zip) {
        formatAddr += (formatAddr.length ? ' ' : '') + info.treasurer_zip;
      }
      if (info.treasurer_address) {
        formatAddr = formatAddr.length ? `${info.treasurer_address}<br>${formatAddr}` : info.treasurer_address;
      }
      info.treasurer_formatted_address = formatAddr.length ? formatAddr : null;
    }
  };

  /**
   * Callbacks for processing results of the subpage block queries.
   *
   * Allows subpage blocks to process results differently. The default is to
   * add the rows result rows as new headers with the aggregated value results.
   */
  subpageQueryResultHandlers = {
    none: function() {},
    addHeaders: function(block, query, rows) {
        const target = block.find('.first');
        rows.forEach((row) => target.append(`<h2>${row[query.field]} ${query.suffix}</h2><p>${toCurrency(row.sum_amount)}</p>`));
    }
  }
})(jQuery, window.PDC, Drupal, drupalSettings);
