function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				return `<a href="/political-disclosure-reporting-data/browse-search-data/committees/${row['id']}" target="_blank">${data}</a>`;
			},
			"targets": 0
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [4,5,6]
  		},
	    {
        targets: [1,4,5,6,11],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 1 },
        { responsivePriority: 10009, targets: 2 },
        { responsivePriority: 8, targets: 3 },
        { responsivePriority: 3, targets: 4 },
        { responsivePriority: 4, targets: 5 },
        { responsivePriority: 5, targets: 6 },
        { responsivePriority: 7, targets: 7 },
        { responsivePriority: 10010, targets: 8 },
        { responsivePriority: 10001, targets: 9 },
        { responsivePriority: 10002, targets: 10 },
        { responsivePriority: 10003, targets: 11 },

	]
	return column_defs;
}


