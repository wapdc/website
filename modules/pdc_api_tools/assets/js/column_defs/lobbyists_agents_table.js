function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['agent_pic_url'] == '') {
					return data;
				}
				else {
					return `<a href="${row['agent_pic_url']}" target="_blank">${data}</a>`;
				}
			},
			"targets": 0
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['lobbyist_firm_url'] == '') {
					return data;
				}
				else {
					return `<a href="${row['lobbyist_firm_url']['url']}" target="_blank">${data}</a>`;
				}
			},
			"targets": 2
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [7]
  		},
  		{ responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 1 },
        { responsivePriority: 3, targets: 2 },
        { responsivePriority: 4, targets: 3 },
        { responsivePriority: 5, targets: 4 },
        { responsivePriority: 6, targets: 5 },
        { responsivePriority: 7, targets: 6 },
        { responsivePriority: 8, targets: 7 },
	]
	return column_defs;
}


