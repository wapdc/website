function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['receipt_date'] == '') {
					return data;
				}
				else {
					return moment(data).format("MM/DD/YYYY");
				}
			},
			"targets": [13] // receipt_date
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [11] // amount
  		},
  		{
	    	"render": function ( data, type, row ) {
	    		return `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
	    	},
			"targets": [19] // url
	    },
  		{
        targets: [2,11,13], // election_year, amount, receipt_date
        	className: 'dt-right'
    	},
    	{ responsivePriority: 1, targets: 0 },
    	{ responsivePriority: 3, targets: 1 },
        { responsivePriority: 4, targets: 2 },
        { responsivePriority: 11, targets: 3 },
        { responsivePriority: 6, targets: 4 },
        { responsivePriority: 9, targets: 5 },
        { responsivePriority: 10, targets: 6 },
        { responsivePriority: 12, targets: 7 },
        { responsivePriority: 13, targets: 8 },
        { responsivePriority: 10010, targets: 9 },
        { responsivePriority: 10011, targets: 10 },
        { responsivePriority: 2, targets: 11 },
        { responsivePriority: 7, targets: 12 },
        { responsivePriority: 8, targets: 13 },
        { responsivePriority: 5, targets: 14 },
        { responsivePriority: 14, targets: 15 },
        { responsivePriority: 15, targets: 16 },
        { responsivePriority: 16, targets: 17 },
        { responsivePriority: 17, targets: 18 },
        { responsivePriority: 18, targets: 19 },

	]
	return column_defs;
}


