function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (data == '') {
					return data;
				}
				else {
					var date_formatted = moment(data).format("MM/DD/YYYY");
					return `<a href="${row['url']}" target="_blank">${date_formatted}</a>`;
				}
			},
			"targets": 2
	    },
		{

			"render": function ( data, type, row ) {
				if (row['person_id'] == '') {
					return data;
				}
				else {
					return `<a href="/political-disclosure-reporting-data/browse-search-data/candidates/surplus-funds/reports/?person_id=${row['person_id']}" target="_blank">${data}</a>`;
				}
			},
			"targets": 1
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [3,4,5]
  		},
  		{
        targets: [3,4,5],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 10009, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 3, targets: 3 },
        { responsivePriority: 4, targets: 4 },
        { responsivePriority: 5, targets: 5 },
        { responsivePriority: 10010, targets: 6 },
	]
	return column_defs;
}