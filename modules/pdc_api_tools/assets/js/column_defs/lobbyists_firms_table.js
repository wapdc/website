function column_defs() {
	var column_defs = [
		{ 'width': '5%', "targets": 1 },
	    {
			"render": function ( data, type, row ) {
				if (row['filer_id'] == '') {
					return data;
				}
				else {
					return `<a href="https://accesshub.pdc.wa.gov/node/${row['filer_id']}" target="_blank">${data}</a>`;
				}
			},
			"targets": 1
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
				"defaultContent": "",
			"targets": [3,4,5,6,7,8,9,10]
  		},
  		{
        targets: [3,4,5,6,7,8,9,10],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 10009, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 3, targets: 10 },
        { responsivePriority: 10001, targets: 3 },
        { responsivePriority: 10002, targets: 4 },
        { responsivePriority: 10003, targets: 5 },
        { responsivePriority: 10004, targets: 6 },
        { responsivePriority: 10005, targets: 7 },
        { responsivePriority: 10006, targets: 8 },
        { responsivePriority: 10007, targets: 9 },
	]
	return column_defs;
}


