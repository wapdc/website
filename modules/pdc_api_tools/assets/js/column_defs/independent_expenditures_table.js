function column_defs() {
	moment.updateLocale(moment.locale(), { invalidDate: 'Invalid Date' });
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['url'] != '') {
					if (row['candidate_first_name'] != '') {
						return `<a href="${row['url']['url']}" target="_blank">${row['candidate_first_name']} ${data}</a>`;
					}
					else {
						return `<a href="${row['url']['url']}" target="_blank">${data}</a>`;
					}
				}
	    		else {
					if (row['candidate_first_name'] != '') {
						return `${row['candidate_first_name']} ${data}`;
					}
					else {
						return data;
					}
	    		}
			},
			"targets": 0
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);;
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [8]
  		},
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [6]
  		},
  		{
        targets: [5,8,11],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 10012, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 10003, targets: 3 },
        { responsivePriority: 3, targets: 4 },
        { responsivePriority: 10004, targets: 5 },
        { responsivePriority: 5, targets: 6 },
        { responsivePriority: 10006, targets: 7 },
        { responsivePriority: 10007, targets: 8 },
        { responsivePriority: 10008, targets: 9 },
        { responsivePriority: 100009, targets: 10 },
        { responsivePriority: 10010, targets: 10 },
        { responsivePriority: 6, targets: 10 },
	]
	return column_defs;
}


