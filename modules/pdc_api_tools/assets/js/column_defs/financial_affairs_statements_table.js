function column_defs() {
	var column_defs = [
	    {
        targets: [1,2,3,4],
        	className: 'dt-right'
    	},
    	{
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [1,2,3]
  		},
  		{
	    	"render": function ( data, type, row ) {
	    		return `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
	    	},
			"targets": [4]
	    },
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 1 },
        { responsivePriority: 4, targets: 2 },
        { responsivePriority: 5, targets: 3 },
        { responsivePriority: 3, targets: 4 },
        { responsivePriority: 10001, targets: 5 },
	]
	return column_defs;
}
