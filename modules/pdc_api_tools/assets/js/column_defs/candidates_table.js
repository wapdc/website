function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				let text = data;
				if (row.declared && !(row.discontinued || row.withdrew)) {
					text += ' <i class="candidacy-icon fa fa-solid fa-asterisk"></i>';
				}
				
				return `<a href="/political-disclosure-reporting-data/browse-search-data/candidates/${row['candidacy_id']}" target="_blank">${text}</a>`;
			},
			"targets": 0
	    },
	    {
			"visible": false,
			"targets": ['candidacy_id']
		},
		{ responsivePriority: 1, targets: 0 },
    { responsivePriority: 2, targets: 1 },
    { responsivePriority: 6, targets: 2 },
    { responsivePriority: 4, targets: 3 },
    { responsivePriority: 10008, targets: 4 },
    { responsivePriority: 10009, targets: 5 },
    { responsivePriority: 5, targets: 6 },
    { responsivePriority: 3, targets: 7 },
    { responsivePriority: 7, targets: 8 },
  	{ responsivePriority: 8, targets: 9 },
    { responsivePriority: 10007, targets: 10 },
    {
	    "render": function ( data, type, row ) {
	    	if (data) {
	    		return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    	}
	    	else {
	    		return data;
	    	}
			},
			"targets": [6,7,8,9,10]
  	},
  	{
      targets: [1,6,7,8,9,10],
        className: 'dt-right'
    }
	]
	return column_defs;
}
