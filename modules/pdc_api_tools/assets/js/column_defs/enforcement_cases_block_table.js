function column_defs() {
	var column_defs = [
	    {
			"render": function ( data, type, row ) {
				if (row['case'] == '') {
	    			return `${data}`;
	    		}
	    		else {
	    			return `<a href="enforcement-cases/${data}" target="_blank">${data}</a>`
	    		}
			},
			targets: [0]
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['opened'] == '') {
					return data;
				}
				else {
					return moment(row['opened']).format("MM/DD/YYYY");
				}
			},
			"targets": [1],
	    }
	]
	return column_defs;
}