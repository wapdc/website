function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['amended_by_report'] == '') {
					return moment(row['report_from']).format("MM/DD/YYYY") + ' - ' + moment(row['report_through']).format("MM/DD/YYYY");
				}
				else {
					return '<span class="amended-strikethrough">' + moment(row['report_from']).format("MM/DD/YYYY") + ' - ' + moment(row['report_through']).format("MM/DD/YYYY") + '</span>';
				}
			},
			"targets": 2 // report_from
	    },
		{
			"render": function ( data, type, row ) {
				if (row['report_through'] == '') {
					return data;
				}
				else {
					return moment(data).format("MM/DD/YYYY");
				}
			},
			"targets": 3 // report_through
	    },
		{
			"render": function ( data, type, row ) {
				if (row['amended_by_report'] == '') {
					return data;
				}
				else {
					return '<span class="amended-strikethrough">' + data + '</span> Amended by: ' + row['amended_by_report'];
				}
			},
			"targets": 4 // report_through
	    },
		{
			"render": function ( data, type, row ) {
				if (row['receipt_date'] == '') {
					return data;
				}
				else {
					return moment(data).format("MM/DD/YYYY");
				}
			},
			"targets": 5 // receipt_date
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['url'] == '') {
					return 'No report available';
				}
				else {
					return `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				}
			},
			"targets": 6 // url
	    },
		{
			"render": function ( data, type, row ) {
				if (row['amends_report'] == '') {
					return data;
				}
				else {
					return data + ' - Amended';
				}
			},
			"targets": 7 // origin
	    }
	]
	return column_defs;
}