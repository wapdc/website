function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['expenditure_date'] == '') {
					return data;
				}
				else {
					return moment(data).format("MM/DD/YYYY");
				}
			},
			"targets": 2
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['url'] == '') {
					return 'No report available';
				}
				else {
					return `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				}
			},
			"targets": 5
	    },
	]
	return column_defs;
}