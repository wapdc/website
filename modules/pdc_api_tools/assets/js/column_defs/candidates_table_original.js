function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['committee_id'] != '') {
					return `<a href="/candidates/${row['committee_id']}" target="_blank">${data}</a>`;
				}
	    		else {
					return `<a href="/candidates/${row['candidacy_id']}" target="_blank">${data}</a>`;
	    		}
			},
			"targets": 0
	    },
	    {
			"visible": false,
			"targets": []
		}
	]
	return column_defs;
}


