function column_defs() {
	var column_defs = [
	    {
			"render": function ( data, type, row ) {
				if (row['Q1_url'] == '') {
					var q1_url = 'n/a';
				}
				else {
					var q1_url = '<a href="' + row['Q1_url']['url'] + '">Q1</a>';
				}
				if (row['Q2_url'] == '') {
					var q2_url = 'n/a';
				}
				else {
					var q2_url = '<a href="' + row['Q2_url']['url'] + '">Q2</a>';
				}
				if (row['Q3_url'] == '') {
					var q3_url = 'n/a';
				}
				else {
					var q3_url = '<a href="' + row['Q3_url']['url'] + '">Q3</a>';
				}
				if (row['Q4_url'] == '') {
					var q4_url = 'n/a';
				}
				else {
					var q4_url = '<a href="' + row['Q4_url']['url'] + '">Q4</a>';
				}
				return `${q1_url}, ${q2_url}, ${q3_url}, ${q4_url}`;
			},
			"targets": 2
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [6,7,8,9,10]
  		},
  		{
        targets: [6,7,8,9,10],
        	className: 'dt-right'
    	},
  		{ responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 1 },
        { responsivePriority: 4, targets: 2 },
        { responsivePriority: 10002, targets: 3 },
        { responsivePriority: 10003, targets: 4 },
        { responsivePriority: 10004, targets: 5 },
        { responsivePriority: 10005, targets: 6 },
        { responsivePriority: 10006, targets: 7 },
        { responsivePriority: 10007, targets: 8 },
        { responsivePriority: 10007, targets: 9 },
        { responsivePriority: 3, targets: 10 },
	]
	return column_defs;
}


