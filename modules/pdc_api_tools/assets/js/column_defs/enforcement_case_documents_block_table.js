function column_defs() {
	var column_defs = [
	    {
			"render": function ( data, type, row ) {
				if (row['id'] == '') {
	    			return `${data}`
	    		}
	    		else {
	    			return `<a href="https://pdc-case-tracking.s3-us-gov-west-1.amazonaws.com/${data}" target="_blank">${data}</a>`
	    		}
			},
			targets: [0]
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['case'] == '') {
					return data
				}
				else {
					return `<a href="/rules-enforcement/enforcement/enforcement-cases/${data}" target="_blank">${data}</a>`
				}
			},
			"targets": [1],
	    }
		,
		{
			"render": function ( data, type, row ) {
				if (row['case_opened'] == '') {
					return data;
				}
				else {
					return moment(row['case_opened']).format("MM/DD/YYYY");
				}
			},
			"targets": [2],
		}
	]
	return column_defs;
}