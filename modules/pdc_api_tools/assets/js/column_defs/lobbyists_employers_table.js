function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['employer_nid'] == '') {
					return data;
				}
				else {
					return `<a href="https://accesshub.pdc.wa.gov/node/${row['employer_nid']}" target="_blank">${data}</a>`;
				}
			},
			"targets": 1
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['Employer_Email'] == '') {
					return data;
				}
				else {
					return `<a href="mailto:${data}" >${data}</a>`;
				}
			},
			"targets": 3
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['url'] == '') {
					return 'Not filed';
				}
				else {
					return `<a href="${data.url}" target="_blank">View Report</a>`;
				}
			},
			"targets": 8
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [4,5,6,7]
  		},
  		{
        targets: [2,4,5,6,7],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 10009, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 5, targets: 3 },
        { responsivePriority: 3, targets: 4 },
        { responsivePriority: 4, targets: 5 },
        { responsivePriority: 6, targets: 6 },
        { responsivePriority: 7, targets: 7 },
        { responsivePriority: 8, targets: 8 },
	]
	return column_defs;
}


