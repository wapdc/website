function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				//Add your rewrite data here.
				//${data} is the column data
				//$row['row_id'] is a cell from the same row. Just change row_id to the row ID from Socrata 
	    		return `custom rewrite here`;
			},
			//The cell to rewrite. Table cells start at 0.
			"targets": 0
	    },
	    {
	    	//If pulling data from other fields, you can hide them afterwards.
	    	//Set visible to false
	    	//Enter an array of cells to hide
			"visible": false,
			"targets": [1]
		}
	]
	//Make sure to return the variable declared above
	return column_defs;
}


