function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['receipt_date'] == '') {
					return data;
				}
				else {
					return moment(data).format("MM/DD/YYYY");
				}
			},
			"targets": 1
	    },
		{
			"render": function ( data, type, row ) {
				if (row['url'] == '') {
	    			return `${data}`;
	    		}
	    		else {
	    			return `<a href="${row['url']['url']}" target="_blank">View Report</a>`
	    		}
			},
			targets: [4]
	    },
	    {
			"render": function ( data, type, row ) {
				if (data == '') {
	    			return `${data}`;
	    		}
	    		else {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
			},
			targets: [3]
	    },
	    {
        targets: [1,3],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 1, targets: 0 },
        { responsivePriority: 3, targets: 1 },
        { responsivePriority: 4, targets: 2 },
        { responsivePriority: 2, targets: 3 },
        { responsivePriority: 5, targets: 4 },
        { responsivePriority: 10010, targets: 5 },
	]
	return column_defs;
}