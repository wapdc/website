function column_defs(tableId) {
	return PDC.campaignTableDefs[tableId] || PDC.campaignTableDefs['default_table'];
}

(function($, PDC) {
	PDC.campaignTableDefs = {
		default_table: [
			{
				"render": (data, type, row) => `${data}`
	  	}
		],

		campaign_reports: [
			{
				"render": function ( data, type, row ) {
					return (row['amended_by_report'] == '')
						? moment(row['report_from']).format("MM/DD/YYYY") + ' - ' + moment(row['report_through']).format("MM/DD/YYYY")
						: '<span class="amended-strikethrough">' + moment(row['report_from']).format("MM/DD/YYYY") + ' - ' + moment(row['report_through']).format("MM/DD/YYYY") + '</span>';
				},
				"targets": 2 // report_from
			},
			{
				"render": function ( data, type, row ) {
					return (row['report_through'] == '') ? data : moment(data).format("MM/DD/YYYY");
				},
				"targets": 3 // report_through
			},
			{
				"render": function ( data, type, row ) {
					if (row['amended_by_report'] == '') {
						return data;
					}
					else {
						return '<span class="amended-strikethrough">' + data + '</span> Amended by: ' + row['amended_by_report'];
					}
				},
				"targets": 4 // report_through
				},
			{
				"render": function ( data, type, row ) {
					if (row['receipt_date'] == '') {
						return data;
					}
					else {
						return moment(data).format("MM/DD/YYYY");
					}
				},
				"targets": 5 // receipt_date
				},
			{
				"render": function ( data, type, row ) {
					return (row['url'] == '') ? 'No report available' : `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				},
				"targets": 6 // url
			},
			{
				"render": function ( data, type, row ) {
					return (row['amends_report'] == '') ? data : data + ' - Amended';
				},
				"targets": 7 // origin
			}
		],

		campaign_contributions: [
			{
				"render": function ( data, type, row ) {
					return (row['receipt_date'] == '') ? data : moment(data).format("MM/DD/YYYY");
				},
				"targets": 0
	    },
	    {
				"render": function ( data, type, row ) {
	    		return (row['url'] == '') ? `${data}` : `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				},
				targets: [9]
	    },
	    {
				"render": function ( data, type, row ) {
	    		return (data == '') ?  `${data}` : new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
				},
				targets: [8]
	    },
	    { targets: [8], className: 'dt-right' },
    	{ responsivePriority: 1, targets: 0 },
      { responsivePriority: 2, targets: 1 },
      { responsivePriority: 4, targets: 2 },
      { responsivePriority: 5, targets: 3 },
      { responsivePriority: 6, targets: 4 },
      { responsivePriority: 7, targets: 5 },
      { responsivePriority: 8, targets: 6 },
      { responsivePriority: 9, targets: 7 },
      { responsivePriority: 3, targets: 8 },
		],

		campaign_expenditures: [
			{
				"render": function ( data, type, row ) {
					return (row['expenditure_date'] == '') ? data : moment(data).format("MM/DD/YYYY");
				},
				"targets": 1
			},
			{
				"render": function ( data, type, row ) {
					return (row['url'] == '') ? `${data}` : `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				},
				targets: [6]
				},
			{
				"render": function ( data, type, row ) {
					return (data == '') ? `${data}` : new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
				},
				targets: [5]
			},
			{ targets: [5], className: 'dt-right' },
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 3, targets: 1 },
			{ responsivePriority: 10001, targets: 2 },
			{ responsivePriority: 4, targets: 3 },
			{ responsivePriority: 5, targets: 4 },
			{ responsivePriority: 2, targets: 5 },
			{ responsivePriority: 6, targets: 6 },
			{ responsivePriority: 10010, targets: 7 },
		],

		campaign_debts: [
			{
				"render": function ( data, type, row ) {
					return row['url'] == '' ? `${data}` : `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				},
				targets: [5]
			},
			{
				"render": function ( data, type, row ) {
					return data == '' ? `${data}` : new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
				},
				targets: [4]
			},
			{
				"render": function ( data, type, row ) {
					if (row['from_date'] == '') {
						return data;
					} else {
						var from_date = moment(data).format("MM/DD/YYYY");
						var thru_date = (row['thru_date'] == '') ? '' : moment(row['thru_date']).format("MM/DD/YYYY");
						return `${from_date} - ${thru_date}`
					}
				},
				"targets": [1],
			},
			{ targets: [4], className: 'dt-right' },
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 3, targets: 1 },
			{ responsivePriority: 10009, targets: 2 },
			{ responsivePriority: 5, targets: 3 },
			{ responsivePriority: 2, targets: 4 },
			{ responsivePriority: 6, targets: 5 },
		],

		campaign_pledges: [
			{
				"render": function ( data, type, row ) {
					if (row['receipt_date'] == '') {
						return data;
					}
					else {
						return moment(data).format("MM/DD/YYYY");
					}
				},
				"targets": 1
			},
			{
				"render": function ( data, type, row ) {
					if (data == '') {
							return `${data}`;
						}
						else {
							return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
						}
				},
				targets: [2]
			},
			{
				"render": function ( data, type, row ) {
					if (row['url'] == '') {
						return `${data}`;
					}
					else {
						return `<a href="${row['url']['url']}" target="_blank">View Report</a>`
					}
			},
				targets: [4]
			},
			{
				targets: [1,2],
				className: 'dt-right'
			},
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: 1 },
			{ responsivePriority: 3, targets: 2 },
			{ responsivePriority: 4, targets: 3 },
			{ responsivePriority: 5, targets: 4 },
		],

		campaign_loans: [
			{
				"render": function ( data, type, row ) {
					if (row['receipt_date'] == '') {
						return data;
					}
					else {
						return moment(data).format("MM/DD/YYYY");
					}
				},
				"targets": 1
				},
			{
				"render": function ( data, type, row ) {
					if (row['url'] == '') {
							return `${data}`;
						}
						else {
							return `<a href="${row['url']['url']}" target="_blank">View Report</a>`
						}
				},
				targets: [4]
			},
			{
				"render": function ( data, type, row ) {
					if (data == '') {
							return `${data}`;
						}
						else {
							return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
						}
				},
				targets: [3]
			},
			{
				targets: [1,3],
				className: 'dt-right'
			},
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 3, targets: 1 },
			{ responsivePriority: 4, targets: 2 },
			{ responsivePriority: 2, targets: 3 },
			{ responsivePriority: 5, targets: 4 },
			{ responsivePriority: 10010, targets: 5 },
		],

		candidate_independent_expenditures: [
			{
				"render": function ( data, type, row ) {
					return (row['report_date'] == '') ? data : moment(data).format("MM/DD/YYYY");
				},
				"targets": [1]
			},
			{
				"render": function ( data, type, row ) {
					return (data == '') ? `${data}` : new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
				},
				targets: [2]
			},
			{
				"render": function ( data, type, row ) {
					return (row['url'] == '') ? `${data}` : `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				},
				targets: [4]
			},
			{ targets: [1], className: 'dt-right' },
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: 2 },
			{ responsivePriority: 3, targets: 3 },
			{ responsivePriority: 4, targets: 1 },
			{ responsivePriority: 5, targets: 4 },
		],
	};
})(jQuery, window.PDC = window.PDC || {});
