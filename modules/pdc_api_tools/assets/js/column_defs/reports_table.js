function column_defs() {
	var column_defs = [
	    {
        targets: [1,2,5],
        	className: 'dt-right'
    	},
    	{
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [2]
  		},
  		{
	    	"render": function ( data, type, row ) {
	    		return `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
	    	},
			"targets": [6]
	    },
	]
	return column_defs;
}
