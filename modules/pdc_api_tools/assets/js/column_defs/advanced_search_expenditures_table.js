function column_defs() {
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['expenditure_date'] == '') {
					return data;
				}
				else {
					return moment(data).format("MM/DD/YYYY");
				}
			},
			"targets": 4 // expenditure_date
	    },
	    {
			"render": function ( data, type, row ) {
				if (row['url'] == '') {
					return 'No report available';
				}
				else {
					return `<a href="${row['url']['url']}" target="_blank">View Report</a>`;
				}
			},
			"targets": 9 // url
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [5] // amount
  		},
  		{
        targets: [1,4,5],// election_year, expenditure_date, amount
        	className: 'dt-right'
    	},
    	{ responsivePriority: 1, targets: 0 },
        { responsivePriority: 5, targets: 1 },
        { responsivePriority: 6, targets: 2 },
        { responsivePriority: 3, targets: 3 },
        { responsivePriority: 4, targets: 4 },
        { responsivePriority: 2, targets: 5 },
        { responsivePriority: 7, targets: 6 },
        { responsivePriority: 8, targets: 7 },
        { responsivePriority: 9, targets: 8 },
        { responsivePriority: 10, targets: 9 },
	]
	return column_defs;
}


