function column_defs() {
	moment.updateLocale(moment.locale(), { invalidDate: 'Invalid Date' });
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['url'] != '') {
					if (row['vendor_name'] != '') {
						return `<a href="${row['url']['url']}" target="_blank">${row['vendor_name']}</a>`;
					}
					else {
						return `<a href="${row['url']['url']}" target="_blank">${data}</a>`;
					}
				}
	    		else {
					if (row['vendor_name'] != '') {
						return `${row['vendor_name']}`;
					}
					else {
						return data;
					}
	    		}
			},
			"targets": 1
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [5]
  		},
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [4]
  		},
  		{
        targets: [3,4,5],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 10012, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 3, targets: 3 },
        { responsivePriority: 5, targets: 4 },
        { responsivePriority: 4, targets: 5 },
        { responsivePriority: 6, targets: 6 },
        { responsivePriority: 7, targets: 7 },
        { responsivePriority: 10006, targets: 7 },
	]
	return column_defs;
}


