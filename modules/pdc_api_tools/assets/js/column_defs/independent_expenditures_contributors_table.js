function column_defs() {
	moment.updateLocale(moment.locale(), { invalidDate: 'Invalid Date' });
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['url'] != '') {
					if (row['funders_name'] != '') {
						return `<a href="${row['url']['url']}" target="_blank">${row['funders_name']}</a>`;
					}
					else {
						return `<a href="${row['url']['url']}" target="_blank">${data}</a>`;
					}
				}
	    		else {
					if (row['funders_name'] != '') {
						return `${row['funders_name']}`;
					}
					else {
						return data;
					}
	    		}
			},
			"targets": 0
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [4]
  		},
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [3]
  		},
  		{
        targets: [2,3,4],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: 1 },
        { responsivePriority: 3, targets: 2 },
        { responsivePriority: 4, targets: 3 },
        { responsivePriority: 5, targets: 4 },
        { responsivePriority: 6, targets: 5 },
        { responsivePriority: 5, targets: 6 },
        { responsivePriority: 10006, targets: 7 },
        { responsivePriority: 10007, targets: 8 },
        { responsivePriority: 10008, targets: 9 },
	]
	return column_defs;
}


