function column_defs() {
	var column_defs = [
  		{
	    	"render": function ( data, type, row ) {
	    		return `<a href="${row['url']['url']}" target="_blank">${row['file_name']}</a>`;
	    	},
			"targets": [0]
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		return `<a href="${row['case_url']['url']}" target="_blank">View Case</a>`;
	    	},
			"targets": [3]
	    },
	]
	return column_defs;
}
