function column_defs() {
	moment.updateLocale(moment.locale(), { invalidDate: 'Invalid Date' });
	var column_defs = [
		{
			"render": function ( data, type, row ) {
				if (row['url'] != '') {
					if (row['candidate_name'] != '') {
						return `<a href="${row['url']['url']}" target="_blank">${row['candidate_name']}</a>`;
					}
					else {
						return `<a href="${row['url']['url']}" target="_blank">${data}</a>`;
					}
				}
	    		else {
					if (row['candidate_name'] != '') {
						return `${row['candidate_name']}`;
					}
					else {
						return data;
					}
	    		}
			},
			"targets": 1
	    },
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' }).format(data);
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [7]
  		},
	    {
	    	"render": function ( data, type, row ) {
	    		if (data) {
	    			return moment(data).format('MM/DD/YYYY');
	    		}
	    		else {
	    			return data;
	    		}
			},
			"targets": [6]
  		},
  		{
        targets: [3,6,7,11],
        	className: 'dt-right'
    	},
    	{ responsivePriority: 10012, targets: 0 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 3, targets: 3 },
        { responsivePriority: 10003, targets: 4 },
        { responsivePriority: 10004, targets: 5 },
        { responsivePriority: 5, targets: 6 },
        { responsivePriority: 4, targets: 7 },
        { responsivePriority: 6, targets: 8 },
        { responsivePriority: 7, targets: 9 },
        { responsivePriority: 8, targets: 10 },
        { responsivePriority: 10013, targets: 11 },
        { responsivePriority: 10014, targets: 12 },
        { targets: [0,1], searchable: true },
     	{ targets: '_all', searchable: false }
	]
	return column_defs;
}


