<?php
//Builds the form block used on the front page

namespace Drupal\pdc_api_tools\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Symfony\Component\Yaml\Yaml;

//This section is required to build the block in Drupal.

/**
 *
 * @Block(
 *   id = "political_disclosures_candidates_chart",
 *   admin_label = @Translation("Political Disclosures Candidates Chart"),
 *   category = @Translation("Landing Page Political Disclosures Candidates Chart"),
 * )
 */

class PoliticalDisclosuresCandidatesChart extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    //grab the module path
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('pdc_api_tools')->getPath();

    //set the conf_dir and the YML path
    $conf_dir = $module_path . '/conf/blocks';
    $yml = $conf_dir . '/chart_blocks.yml';

    //grab the conf data from the YML
    $conf = array();
    $conf = array_merge($conf, Yaml::parseFile($yml));

    //set the data_id
    $data_id = $conf['candidates']['settings']['data_id'];

    $electionYear = +date('Y');
    $current_month = +date('m');

    $queryByPosition = <<<EOD
    select office,
    case(position is not null, jurisdiction || ' (' || position || ')', true , jurisdiction) as jurisdiction,
    SUM(expenditures_amount)
    where office IS NOT NULL and jurisdiction IS NOT NULL and expenditures_amount IS NOT NULL and election_year = $electionYear
    group by office,jurisdiction,position
    order by SUM(expenditures_amount) DESC
    limit 10
    EOD;

    $queryWithoutPosition = <<<EOD
    select office, jurisdiction, SUM(expenditures_amount)
    where office IS NOT NULL and jurisdiction IS NOT NULL and expenditures_amount IS NOT NULL
    and election_year = $electionYear
    group by office, jurisdiction
    order by SUM(expenditures_amount) DESC limit 10
    EOD;

    // Between June 1st and Dec 31st, after declarations, so we have positions and only for years after 2022 where we
    // have good position data, we group by position in addition to office and jurisdiction.
    $query = $electionYear >= 2022 && $current_month >= 6 ? $queryByPosition : $queryWithoutPosition;

    $pathPart = $data_id . '.json?$query=' . $query;

    $chart_data = pdc_api_tools_query($pathPart) ?: [];

    //send the data to the theme function
    return [
      '#type' => 'pdc_chart',
      '#chart_type' => 'expenditures',
      '#title' => $this->t('Most expensive races this year'),
      '#empty_message' => $this->t('No expenditures reported.'),
      '#headings' => [
        $this->t('Office and Jurisdiction'),
        $this->t('Expenditures Amount'),
      ],
      '#rows' => array_map(function($v) {
        return [
          "{$v['office']} - {$v['jurisdiction']}",
          $v['SUM_expenditures_amount'],
        ];
      }, $chart_data)
    ];
  }
}
