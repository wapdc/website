<?php
//Builds the form block used on the front page

namespace Drupal\pdc_api_tools\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Url;
use Symfony\Component\Yaml\Yaml; 

//This section is required to build the block in Drupal.

/**
 *
 * @Block(
 *   id = "enforcement_case_documents_block",
 *   admin_label = @Translation("Enforcement Case Documents Block"),
 *   category = @Translation("Enforcement Case Documents Block"),
 * )
 */

class EnforcementCaseDocumentsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    //Grab the module path
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('pdc_api_tools')->getPath();
    
    //set the conf_dir
    $conf_dir = $module_path . '/conf/blocks';
    $conf = array();

    //Grab the YML data for enforcement_case_documents
    $conf = Yaml::parseFile($conf_dir . '/enforcement_case_documents_block_table.yml');
    
    //set some default vars
    $build = [];
    $jurisdiction_query_array = array();
    $new_query = '';

    //Loop through the attrs for the block
    foreach ($conf['enforcement_case_documents_block_table']['settings']['attrs'] as $key => $setting):
      
      //find the fixed filter value
      if ($setting['name'] == 'data-dadc-fixed-filter'):
        
        //Save that value where we need it
        $conf['enforcement_case_documents_block_table']['settings']['attrs'][$key]['value'] = $setting['value'];
      endif;
    endforeach;

    //Store the column def, filter and column values
    //Save empty vars if nothing exists
    $filters = isset($conf['enforcement_case_documents_block_table']['filters']) ? $conf['enforcement_case_documents_block_table']['filters'] : '';
    $columns = isset($conf['enforcement_case_documents_block_table']['columns']) ? $conf['enforcement_case_documents_block_table']['columns'] : '';
    $column_defs_js = isset($conf['enforcement_case_documents_block_table']['settings']['column_defs_js']) ? $conf['enforcement_case_documents_block_table']['settings']['column_defs_js'] : 0;

    //build the column def path
    $js_path_build = '/' . $module_path . '/assets/js/column_defs/';
    $default_js_path_build = '/' . $module_path . '/assets/js/column_defs/default_table.js';
    $column_defs_path = $column_defs_js != 0 ? $js_path_build . 'enforcement_case_documents_block_table.js' : $default_js_path_build;

    //gather all the data for the block
    $build = [
      '#theme' => 'enforcement_case_documents_block_table',
      '#column_defs_path' => $column_defs_path,
      '#data_id' => $conf['enforcement_case_documents_block_table']['settings']['data_id'],
      '#table_id' => $conf['enforcement_case_documents_block_table']['settings']['table_id'],
      '#filters' => $filters,
      '#settings' => $conf['enforcement_case_documents_block_table']['settings'],
      '#columns' => $columns,
      '#attached' => [
        'library' => [
          'pdc_api_tools/data_tables'
        ]   
      ]
    ];
    return $build;
  }

}