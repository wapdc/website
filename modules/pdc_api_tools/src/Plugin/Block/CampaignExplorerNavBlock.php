<?php
//Builds the form block used on the front page

namespace Drupal\pdc_api_tools\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Url;

//This section is required to build the block in Drupal.

/**
 *
 * @Block(
 *   id = "campaign_explorer_nav_block",
 *   admin_label = @Translation("Campaign Explorer Navigation Block"),
 *   category = @Translation("Campaign Explorer Navigation Block"),
 * )
 */

class CampaignExplorerNavBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'campaign_explorer_nav_block',
    ];

  }

}