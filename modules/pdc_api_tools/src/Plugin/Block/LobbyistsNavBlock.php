<?php
//Builds the form block used on the front page

namespace Drupal\pdc_api_tools\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Url;

//This section is required to build the block in Drupal.

/**
 *
 * @Block(
 *   id = "lobbyists_nav_block",
 *   admin_label = @Translation("Lobbyists Navigation Block"),
 *   category = @Translation("Lobbyists Navigation Block"),
 * )
 */

class LobbyistsNavBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    //The variables below are sent to pdc_api_tools_theme in the .module file
    return [
      '#theme' => 'lobbyists_nav_block',
    ];

  }

}