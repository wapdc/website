<?php
//Builds the form block used on the front page

namespace Drupal\pdc_api_tools\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

//This section is required to build the block in Drupal.

/**
 *
 * @Block(
 *   id = "reports_by_filer_name",
 *   admin_label = @Translation("Reports by Filer Name Form"),
 *   category = @Translation("Reports by Filer Name Form"),
 * )
 */

class ReportsByFilerNameForm extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    //The variables below are sent to pdc_api_tools_theme in the .module file
    return [
      '#theme' => 'reports_by_filer_name_block'
    ];

  }

}