<?php
//Builds the form block used on the front page

namespace Drupal\pdc_api_tools\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Symfony\Component\Yaml\Yaml;

/**
 *
 * @Block(
 *   id = "political_disclosures_committees_chart",
 *   admin_label = @Translation("Landing Page Political Disclosures Committees Chart"),
 *   category = @Translation("Landing Page Political Disclosures Committees Chart"),
 * )
 */

class PoliticalDisclosuresCommitteesChart extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    //grab the module path
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('pdc_api_tools')->getPath();

    //set the conf_dir and the YML path
    $conf_dir = $module_path . '/conf/blocks';
    $yml = $conf_dir . '/chart_blocks.yml';

    //grab the conf data from the YML
    $conf = array();
    $conf = array_merge($conf, Yaml::parseFile($yml));

    //set the data_id
    $data_id = $conf['committees']['settings']['data_id'];

    //set some default and placeholder vars
    $chart_data = [];
    $current_year = date('Y');

    //grab and set some default data from the YML
    //Set the vars to empty if the values do not exist
    $select_query = (!empty($conf['committees']['settings']['select_query'])) ? $conf['committees']['settings']['select_query'] : '';
    $where_query = (!empty($conf['committees']['settings']['where_query'])) ? ' ' . $conf['committees']['settings']['where_query'] . ' and election_year=' . $current_year : '';
    $group_query = (!empty($conf['committees']['settings']['group_query'])) ? ' ' . $conf['committees']['settings']['group_query'] : '';
    $order_query = (!empty($conf['committees']['settings']['order_query'])) ? ' ' . $conf['committees']['settings']['order_query'] : '';
    $limit_query = (!empty($conf['committees']['settings']['limit_query'])) ? ' ' . $conf['committees']['settings']['limit_query'] : '';

    //Build the query
    $data_query = $select_query . $where_query . $group_query . $order_query . $limit_query;
    $chart_query = $data_id . '.json?$query=' . $data_query;

    //run the query if both data_id and data_query exist
    if($data_id != '' && $data_query != ''):

      //grab the data with the pdc_api_tools_query from .module
      $chart_data = pdc_api_tools_query($chart_query) ?: [];
    endif;

    //send the data to the theme function
    return [
      '#type' => 'pdc_chart',
      '#chart_type' => 'expenditures',
      '#title' => $this->t('Top spending committees this year'),
      '#empty_message' => $this->t('No expenditures reported.'),
      '#headings' => [
        $this->t('Committee'),
        $this->t('Expenditures Amount'),
      ],
      '#rows' => array_map(function($v) {
        return [
          "{$v['filer_name']}",
          $v['expenditures_amount'],
        ];
      }, $chart_data)
    ];
  }
}
