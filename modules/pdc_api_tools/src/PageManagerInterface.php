<?php

namespace Drupal\pdc_api_tools;

/**
 * Manages page definitions and configurations for PDC generated pages.
 *
 * Provides an interface for services that provide PDC page and subpage
 * configurations. The service implementation is able to determine how these
 * configuration are loaded or generated. For instance implementations are free
 * to build configurations from files or configuration entities.
 */
interface PageManagerInterface {

  /**
   * Get the page definition cache tags for invalidation.
   * 
   * @return string[]
   *   Returns the cache tags for the page definitions.
   */
  public function getCacheTags(): array;
    
  /**
   * Get all available page definitions.
   *
   * @param bool $force_refresh
   *   Force a refresh of the page definition.
   *
   * @return array
   *   An array of all the page configurations available. keyed by their page
   *   identifiers.
   */
  public function getPageDefinitions(bool $force_refresh = FALSE): array;

  /**
   * Get the page definition matching the page identifier.
   *
   * @param string $page_id
   *   The page identifier fetch the definition of.
   *
   * @return array|null
   *   If a page matching the $page_id is available, an associative array of
   *   the page configurations are returned. NULL if no matching page
   *   configuration is available.
   */
  public function getPageDefinition(string $page_id): ?array;

  /**
   * Get all available subpage definitions for a page.
   *
   * @param bool $force_refresh
   *   Force a refresh of the page definition.
   *
   * @return array
   *   An array of all the page configurations available. keyed by their page
   *   identifiers.
   */
  public function getSubpageDefinitions(string $page_id, bool $force_refresh = FALSE): array;

  /**
   * Get the subpage definition matching the subpage identifier.
   *
   * @param string $page_id
   *   The page identifier to fetch the subpage definition for.
   * @param string $subpage_id
   *   The subpage indentifier to fetch the subpage definition for.
   *
   * @return array|null
   *   The definition for the subpage of the page indentified by the
   *   $subpage_id and $page_id respectively. Null if there is no page or
   *   subpage of that page available.
   */
  public function getSubpageDefinition(string $page_id, string $subpage_id): ?array;

}
