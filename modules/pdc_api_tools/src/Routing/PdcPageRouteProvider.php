<?php

namespace Drupal\pdc_api_tools\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\pdc_api_tools\Controller\PageControllerBase;
use Drupal\pdc_api_tools\PageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route provider to get routes for campaign pages provided by pdc_api_tools.
 */
class PdcPageRouteProvider implements ContainerInjectionInterface {

  /**
   * The page manager service which provide page and subpage definitions.
   *
   * @var \Drupal\pdc_api_tools\PageManagerInterface
   */
  protected PageManagerInterface $pageManager;

  /**
   * Creates a new instance of the CampaignPageRouteProvider class.
   *
   * @param \Drupal\pdc_api_tools\PageManagerInterface $page_manager
   *   The page manager service which provides page and subpage definitions.
   */
  public function __construct(PageManagerInterface $page_manager) {
    $this->pageManager = $page_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pdc_api_tools.page_manager')
    );
  }

  /**
   * Gets the route definitions for the campaign data pages.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   Routes for campaign pages which are maintained by the pdc_api_tools
   *   module.
   */
  public function getRoutes(): RouteCollection {
    $routes = new RouteCollection();

    foreach ($this->pageManager->getPageDefinitions() as $pageId => $definition) {
      $path = $definition['route']['path'];
      $pageController = $definition['route']['controller'] ?? NULL;
      $requirements =  ['_permission' => 'access content'];
      // @todo Allow caching now for candiate pages but still disable page 
      // caching until refactor of other pages to improve the cachability of the
      // Socrata data pages.
      $options = 'candidate' === $pageId ? [] : ['no_cache' => 'TRUE'];

      if (is_a($pageController, PageControllerBase::class, TRUE)) {
        $routes->add($definition['route']['name'], new Route(
          $path,
          [
            '_controller' => $pageController . '::content',
            '_title_callback' => $pageController . '::title',
            'page_id' => $pageId,
          ],
          $requirements,
          $options
        ));
      }

      // Generate subpages of the campaign pages for each of the pages defined
      // by "/conf/subpages".
      foreach ($this->pageManager->getSubpageDefinitions($pageId) as $subpageId => $subDef) {
        $subpageController = $subDef['route']['controller'] ?? $pageController;

        if ($subpageController) {
          $subPath = $subDef['route']['path'] ?? $path . '/' . str_replace('_', '-', $subpageId);

          $routes->add($subDef['route']['name'], new Route(
            $subPath,
            [
              '_controller' => $subpageController . '::content',
              '_title_callback' => $subpageController . '::title',
              'page_id' => $pageId,
              'subpage' => $subpageId,
            ],
            $requirements
          ));
        }
      }
    }

    return $routes;
  }

}
