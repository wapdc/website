<?php

namespace Drupal\pdc_api_tools\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Renders 2D data into an HTML table that is progressively enhanced to a chart.
 *
 * @RenderElement("pdc_chart")
 */
class PdcChart extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#pre_render' => [
        [$class, 'preRenderChart'],
      ],
      '#title' => '',
      '#chart_type' => '',
      '#chart_options' => [],
      '#headings' => [],
      '#rows' => [],
      '#empty_message' => t('No data provided'),
    ];
  }

  public static function preRenderChart($element) {
    $chart_type = $element['#chart_type'];
    $library_name = "pdc_chart_$chart_type";

    // There can be multiple charts on the page. Supply a unique ID to tie the
    // rendered table to the chart data.
    $chart_id = Html::getUniqueId($library_name);

    // Renders the chart as a server-rendered HTML table by default.
    $element['#theme'] = 'pdc_chart';
    $element['#chart_id'] = $chart_id;

    // Pass the same data to JS so that it can replace the table with a chart.
    $element['#attached']['library'][] = "pdc_api_tools/$library_name";
    $element['#attached']['drupalSettings'][$library_name][$chart_id]['title'] = $element['#title'];
    $element['#attached']['drupalSettings'][$library_name][$chart_id]['headings'] = $element['#headings'];
    $element['#attached']['drupalSettings'][$library_name][$chart_id]['rows'] = $element['#rows'];
    $element['#attached']['drupalSettings'][$library_name][$chart_id]['chart_options'] = $element['#chart_options'];

    return $element;
  }
}
