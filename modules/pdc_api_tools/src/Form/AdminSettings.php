<?php

//This form is not used
//Only kept here in case an admin form is required
namespace Drupal\pdc_api_tools\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;


class AdminSettings extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'admin_form';
  }

  protected function getEditableConfigNames() {
    return [
      'pdc_api_tools.settings',
    ];
  }
  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pdc_api_tools.settings');
    if($config->get('candidates_page_parent')):
      $node = \Drupal\node\Entity\Node::load($config->get('candidates_page_parent'));
      $nid = $node->id();
      $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid);
    endif;

    $page_title_desc = 'The clean title for this page is <strong>' . $config->get('candidates_page_title_clean') . '</strong>';
    $form['candidates_page_title'] = [
      '#type' => 'textfield',
      '#description' => $page_title_desc,
      '#default_value' => $config->get('candidates_page_title'),
      '#title' => $this->t('Candidates - Page Title'),
    ];
    if($config->get('candidates_page_parent')):
      $form['candidates_page_parent'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'node',
        '#selection_settings' => array(
          'target_bundles' => 'page',
        ),
        '#default_value' => $node,
        '#description' => 'The full path for the Candidate page is: <strong><a href="' . $config->get('candidates_page_path') . '">' . $config->get('candidates_page_path') . '</a></strong>',
        '#title' => $this->t('Candidates - Parent Page'),
      ];
    else:
      $form['candidates_page_parent'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'node',
        '#selection_settings' => array(
          'target_bundles' => 'page',
        ),
        '#title' => $this->t('Candidates - Parent Page'),
      ];
    endif;
    $form['actions'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $candidates_title = $form_state->getValue('candidates_page_title');
    $candidates_parent = $form_state->getValue('candidates_page_parent');
    if (!$candidates_title) {
      // Set an error for the form element with a key of "title".
      $form_state->setErrorByName('candidates_page_title', $this->t('We really need a title.'));
    }
    if (!$candidates_parent){
      // Set an error for the form element with a key of "accept".
      $form_state->setErrorByName('candidates_page_parent', $this->t('Our page needs a parent.'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $messenger = \Drupal::messenger();
    $messenger->addMessage('Your settings have been saved. Yay.');

    $config = $this->config('pdc_api_tools.settings');

    $candidates_page_title = $form_state->getValue('candidates_page_title');
    $candidates_page_parent = $form_state->getValue('candidates_page_parent');
        
    $node = \Drupal\node\Entity\Node::load($candidates_page_parent);
    $nid = $node->id();
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid);

    $candidates_page_title_clean = preg_replace('/[^a-zA-Z0-9\s]/', '', strtolower($candidates_page_title));
    $candidates_page_title_clean =  preg_replace('!\s+!', '-', $candidates_page_title_clean);

    $config->set('candidates_page_title', $candidates_page_title);
    $config->set('candidates_page_title_clean', $candidates_page_title_clean);
    $config->set('candidates_page_path', $alias . '/' . $candidates_page_title_clean);
    $config->set('candidates_page_parent', $candidates_page_parent);
    
    $config->save();
  }

}