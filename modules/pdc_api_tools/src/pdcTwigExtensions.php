<?php

namespace Drupal\pdc_api_tools;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

//These are the custom Twig functions
//They are used in Twig files exclusively to callback to PHP functions
//Some may be replaced by custom datatable rewrites
//Leaving this here in case there is a need for them
class pdcTwigExtensions extends AbstractExtension {

  /**
   * This is the same name we used on the services.yml file
   * @return string
   */
  public function getName() {
    return "pdc_api_tools.pdcTwigExtension";
  }

  /**
   * {@inheritdoc}
   * Return your custom twig function to Drupal
   */
  public function getFilters() {
    return [
      new TwigFilter('dateformat', [$this, 'dateFormat']),
      new TwigFilter('textformat', [$this, 'textFormat']),
      new TwigFilter('numberformat', [$this, 'numberFormat'])
    ];
  }

  /**
   * @param $string
   * @return float
   */
  public static function dateFormat($string) {
    if ($string):
      $date = date_format(date_create($string),'m/d/Y');
      return $date;
    else:
      return $string;
    endif;
  }

  public static function textFormat($string) {
    if ($string):
      $string = htmlspecialchars($string);
      return $string;
    else:
      return $string;
    endif;
  }
  public static function numberFormat($string) {
    if ($string):
      $val = round($string);
      $num = number_format($val, 0, '.', ',');
      if ((!$num) || ($num == NULL)):
        $num = '$' . 0;
      elseif($num < 0):
        $num = '-$' . abs($num);
      else:
        $num = '$' . $num;
      endif;
      return $num;
    else:
      return $string;
    endif;
  }
}
