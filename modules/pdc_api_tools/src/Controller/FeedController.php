<?php

namespace Drupal\pdc_api_tools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\feeds\Entity\Feed;
use Drupal\node\Entity\Node;
use stdClass;
use Exception;
use Drupal;

class FeedController extends ControllerBase {

  /**
   * @return JsonResponse
   */
  public function processEnforcementCasesFeed(): JsonResponse {
    // clean daily
    $last_cleaned = Drupal::state()->get('enforcement-cases-last-cleaned');
    if (!is_numeric($last_cleaned)) {
      $last_cleaned = 0;
    }
    if (time() - $last_cleaned >= 86400) {
      $this->cleanEnforcementCases();
    }

    $response = new stdClass();
    $response->success = true;
    try {
      // hardcoded feed 11
      $feed = Feed::load(11);
      $result = Drupal::entityQuery('node')
        ->accessCheck()
        ->condition('type', 'enforcement_case')
        ->sort('field_enf_case_updated_at', 'desc')
        ->range(0, 1)
        ->execute();
      $node = Node::load(reset($result));
      $last_updated_at = date('Y-m-d', $node->get('field_enf_case_updated_at')->value);
      $url = 'https://data.wa.gov/resource/a4ma-dq6s.json' . '?$where=updated_at%3E=' . "'$last_updated_at'" . '&$order=updated_at+desc';
      $feed->setSource($url);
      $feed->save();
      $feed->import();
    }
    catch (Exception $e) {
      $response->success = false;
    }
    return new JsonResponse($response);
  }

  /**
   * @return bool
   */
  function cleanEnforcementCases(): bool {
    // get open data case ids
    try {
      $uri = 'https://data.wa.gov/resource/a4ma-dq6s.json?$select=case&$limit=50000';
      $response = Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'text/plain')));
      $data = (string) $response->getBody();
      if (empty($data)) {
        return false;
      }
      $json = json_decode($data);
    }
    catch (Exception $e) {
      return false;
    }

    // get local case ids
    $sql = "
      SELECT base_table.nid AS nid, field_enf_case_id_value as case_id
      FROM node base_table
        INNER JOIN node_field_data ON node_field_data.nid = base_table.nid
        INNER JOIN node__field_enf_case_id ON node__field_enf_case_id.entity_id = base_table.nid
      WHERE (node_field_data.type = 'enforcement_case')";
    $database = Drupal::database();
    $query = $database->query($sql);
    $results = $query->fetchall();
    $case_ids = array_column($json, 'case');
    foreach ($results as $result) {
      if (!in_array($result->case_id, $case_ids)) {
        try {
          $node = Node::load($result->nid);
          $node->delete();
        }
        catch (Exception $e) {
          // notify someone? Drupal error?
          continue;
        }
      }
    }
    $now = time();
    Drupal::state()->set('enforcement-cases-last-cleaned', $now);
    Drupal::logger('pdc_api_tools')->notice("Enforcement case last cleaned time (variable name enforcement-cases-last-cleaned) set to " . date("M j, Y H:i:s", $now));
    return true;
  }

  /**
   * @param bool $clean
   * @return JsonResponse
   */
  public function processCandidatesFeed(bool $clean = false): JsonResponse {
    // clean daily
    $last_cleaned = Drupal::state()->get('candidates-last-cleaned');
    if (!is_numeric($last_cleaned)) {
      $last_cleaned = 0;
    }
    if (time() - $last_cleaned >= 86400) {
      $this->cleanCandidates();
    }

    $response = new stdClass();
    $response->success = true;
    try {
      // hardcoded feed 19
      $feed = Feed::load(19);
      $result = Drupal::entityQuery('node')
        ->accessCheck()
        ->condition('type', 'candidate')
        ->sort('field_candidate_updated_at', 'desc')
        ->range(0, 1)
        ->execute();
      $node = Node::load(reset($result));
      $last_updated_at = date('Y-m-d', $node->get('field_candidate_updated_at')->value);
      $url = 'https://data.wa.gov/resource/3h9x-7bvm.json?$query=select%20candidacy_id%20where%20political_committee_type=%22Candidate%22%20and%20updated_at%3E=' . $last_updated_at . '%20order%20by%20updated_at%20desc%20limit%20200000';
      $feed->setSource($url);
      $feed->save();
      $feed->import();
    }
    catch (Exception $e) {
      $response->success = false;
    }
    return new JsonResponse($response);
  }

  /**
   * @return bool
   */
  function cleanCandidates(): bool {
    // get open data case ids
    try {
      $url = 'https://data.wa.gov/resource/3h9x-7bvm.json?$query=select%20candidacy_id%20where%20political_committee_type=%22Candidate%22%20order%20by%20updated_at%20desc%20limit%20200000';
      $response = Drupal::httpClient()->get($url, array('headers' => array('Accept' => 'text/plain')));
      $data = (string) $response->getBody();
      if (empty($data)) {
        return false;
      }
      $json = json_decode($data);
    }
    catch (Exception $e) {
      return false;
    }

    // get drupal ids
    $sql = "
      SELECT base_table.nid AS nid, field_candidate_unique_id_value as candidacy_id
      FROM node base_table
        INNER JOIN node_field_data node_field_data ON node_field_data.nid = base_table.nid
        INNER JOIN node__field_candidate_unique_id node__field_candidate_unique_id ON node__field_candidate_unique_id.entity_id = base_table.nid
      WHERE (node_field_data.type = 'candidate')";
    $database = Drupal::database();
    $query = $database->query($sql);
    $results = $query->fetchall();
    $json_ids = array_column($json, 'candidacy_id');
    foreach ($results as $result) {
      if (!in_array($result->candidacy_id, $json_ids)) {
        try {
          $node = Node::load($result->nid);
          $node->delete();
        }
        catch (Exception $e) {
          // notify someone? Drupal error?
          continue;
        }
      }
    }
    Drupal::state()->set('candidates-last-cleaned', time());
    return true;
  }

}