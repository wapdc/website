<?php

namespace Drupal\pdc_api_tools\Controller;

use Symfony\Component\HttpFoundation\Request;

//Handles the enforcement cases page
class EnforcementCases extends PageControllerBase {
	//Build the title for the page
	//Called by the routing data
	public function title($case = NULL, $page_id = NULL) {
		//Only runs if a case variable is set
		if ($case != 'all'):
			//Build the module path
			$page_conf = $this->getPageDefinition($page_id)['page'];

			//Build the query
			$data_id = $page_conf['page_settings']['data_id'];
			$query = $data_id . '.json?case=' . $case;

			//Make the query request
			//This function lives in the .module file
			$data = pdc_api_tools_query($query);

			//Handle the title if data is returned or not
			if (!empty($data)):
				$title = $data[0]['subject'];
			else:
				$title = 'No case matches found';
			endif;
		//If no case variable is set or it's set to all
		else:
			$title = 'No case selected';
		endif;
		return $title;
	}

	//Build the content for the page
	//Called by the routing data
	public function content(Request $request, $case = NULL, $page_id = NULL) {
		//Only runs if a case variable is set
		if ($case != 'all'):
			$data = '';
			$theme_name = '';

			$page_conf = $this->getPageDefinition($page_id)['page'];

			//Build the query
			$data_id = $page_conf['page_settings']['data_id'];
			$query = $data_id . '.json?case=' . $case;

			//Check if data_id is empty
			if ($data_id != ''):

				//Make the call for the data
				//Function lives in the .module file
				$data = pdc_api_tools_query($query);

				if(!empty($data[0]['description'])):
					$data[0]['description'] = html_entity_decode($data[0]['description']);
				endif;

				if(!empty($data[0]['attachments'])):
					$attachments_array = json_decode($data[0]['attachments'], true);
					$data[0]['attachments'] = $attachments_array;
				endif;

				if(!empty($data[0]['penalties'])):
					$penalties_array = json_decode($data[0]['penalties'], true);
					$payments_total = 0;
					foreach ($penalties_array as $key => $penalty):
						$payments_total += $penalty['amount'];
					endforeach;
					$data[0]['penalties'] = $penalties_array;
					$data[0]['payments_total'] = '$' . $payments_total;
				endif;

			endif;

			//Set the theme_name to the page_id var
			$theme_name = $page_conf['page_settings']['theme'];
		endif;

		//send all this to the .module theme function
		return [
			'#theme' => $theme_name,
			'#case' => $case,
			'#enforcement_cases_data' => $data
		];
	}
}
