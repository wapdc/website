<?php

namespace Drupal\pdc_api_tools\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableRedirectResponse;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base routing controller for candidate and committee finance report pages.
 *
 * Intended to contain the base page title and page content callbacks that
 * should provide most of the logic to build candidate and committee pages and
 * their subpages.
 */
class CampaignControllerBase extends PageControllerBase {

  /**
   * Campaign page title callback.
   *
   * The base version provides a generic title that gets replaced on the
   * frontend after the campaign data is fetched from Socrata.
   *
   * @param string|null $id
   *   The candidate or committee identifier.
   * @param string $page_id
   *   The page type identifier from the route. This should be "candidate" or
   *   "committee".
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   A generic title for the page type. Page title is expected to be replaced
   *   once the Socrata data is populated.
   */
  public function title($id = NULL, $page_id = NULL) {
    return t('@campaign_type Finance Report', [
      '@campaign_type' => ucfirst($page_id),
    ]);
  }

  /**
   * Generates the campaign finance report overview content.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request the response is being built for.
   * @param string|null $id
   *   The candidate or committee identifier to build the page content for.
   * @param string|null $page_id
   *   The page type ID ("candidate" or "committee").
   * @param string|null $subpage
   *   The subpage if a subpage was requested.
   */
  public function content(Request $request, $id = NULL, $page_id = NULL, $subpage = NULL) {
    $pageDef = $this->pageManager->getPageDefinition($page_id);

    // Handle subpage content requests.
    if ($subpage) {
      // Throws HTTP 404 error if subpage requested is invalid.
      $subpageDef = $this->getPageDefinition($page_id, $subpage);
      return $this->subpageContent($request, $id, $subpageDef);
    }

    $tabs = [];
    $route_params = ['id' => $id];
    $subpages = $this->pageManager->getSubpageDefinitions($page_id);
    foreach ($this->getAllowedSubpages() as $subpageId) {
      if (isset($subpages[$subpageId])) {
        $tabs[$subpageId] = [
          'title' => ucwords(str_replace('_', ' ', $subpageId)),
          'url' => Url::fromRoute($subpages[$subpageId]['route']['name'], $route_params)->toString(),
        ];
      }
    }

    $page = [];
    $page['#attached']['library'] = [
      'pdc_api_tools/global',
      'pdc_api_tools/datatables_library',
      'pdc_api_tools/datatables',
      'pdc_api_tools/candidate_committee_datatables',
      'pdc_api_tools/pdc_chart_pie',
      'pdc_api_tools/pdc_chart_bar',
      'pdc_api_tools/campaign_page',
      'pdc_api_tools/candidate_report_tables',
    ];
    $page['#attached']['drupalSettings']['pdcCampaignPage'] = [
      'id' => $id,
      'type' => $page_id,
      'label' => $page_id === 'committee' ? 'Committee' : 'Candidate',
      'field' => $page_id === 'committee' ? 'id' : 'candidacy_id',
      'path' => $pageDef['route']['path'],
      'dsid' => $pageDef['page_settings']['data_id'],
      'subpages' => $tabs,
    ];
    $page['#theme_wrappers'] = ['container'];
    $page['#attributes']['class'] = ['campaign-data-wrapper'];

    $page['content']['#theme'] = $pageDef['page_settings']['theme'];
    $page['spinner']['#theme'] = 'pdc_module_spinner';

    // Add charts and tables from the definitions.
    $charts = $pageDef['chart_settings'] ?? [];
    foreach ($charts as $chart_name => $chart_config) {
      $page['content']["#{$chart_name}_chart"] = $this->buildLoadableChart($chart_name, $chart_config);
    }

    // Add the reports tables;
    $tables = $pageDef['datatable_settings'] ?? [];
    foreach ($tables as $table_name => $table_settings) {
      $table_settings['fixed_filters'] = 'where ' . implode(' and ', [
        'filer_id="{filer_id}"',
        'election_year="{election_year}"',
      ]);

      $page['content']["#{$table_name}_table_settings"] = $table_settings;

      //Build the paths for the custom_def file
      $js_path_build = '/' . $this->modulePath . '/assets/js/column_defs/';
      $page['content']['#column_defs_reports_path'] = $js_path_build . 'candidates_reports.js';
    }

    $cache = new CacheableMetadata();
    $cache->addCacheTags($this->pageManager->getCacheTags());
    $cache->addCacheContexts(['route.name']);
    $cache->applyTo($page);

    return $page;
  }

  /**
   * Generates the campaign finance report subpage content.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request the response is being built for.
   * @param string $id
   *   The candidate or committee identifier to build the page content for.
   * @param object $subpageDef
   *   The loaded subpage configurations to use to define the page
   */
  public function subpageContent(Request $request, $id, $subpageDef) {
    ['page' => $page, 'subpage' => $subpage] = $subpageDef;

    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    $context = new RenderContext();

    if ('json' === $request->get('_format')) {
      $elements = [
        '#theme' => $subpage['page_settings']['theme'],
        '#subpage_settings' => $subpage,
        '#subpage_info_block' => $subpage['page_settings']['subpage_info_block'],
      ];

      if (!empty($subpage['chart_settings'])) {
        $elements['#subpage_chart'] = $this->buildLoadableChart("{$subpage['id']}-subpage", $subpage['chart_settings']);
      }

      if (!empty($subpage['table_settings'])) {
        $subpage['table_settings']['id'] = $subpage['id'];
        $subpage['table_settings']['fixed_filter'] = 'where ' . implode(' and ', [
          'filer_id="{filer_id}"',
          'election_year="{election_year}"',
        ]);
        $elements['#reports_table_settings'] = $subpage['table_settings'];
      }

      $json = [];
      if (!empty($subpage['data_block_settings'])) {
        $json['data_block_settings'] = $subpage['data_block_settings'];
      }

      $renderer->executeInRenderContext($context, function() use (&$json, $elements, $renderer) {
        $json['content'] = $renderer->renderRoot($elements);
      });

      $response = new CacheableJsonResponse($json);
    }
    else {
      $uriParams = ['id' => $id, 'page_id' => $page['id']];
      $options = ['fragment' => $subpage['id']];
      $pageUri = Url::fromRoute($page['route']['name'], $uriParams, $options);

      $renderer->executeInRenderContext($context, function() use ($pageUri, &$uri) {
        $uri = $pageUri->toString();
      });

      $response = new CacheableRedirectResponse($uri);
    }

    // Cache based on the route name and if the "_format" type of the request.
    $cache = CacheableMetadata::createFromObject($context)->addCacheContexts([
      'route.name',
      'url.query_args:_format'
    ]);

    return $response->addCacheableDependency($cache);
  }

  /**
   * Produces an array in a format that is expected for passing on to the chart
   * library to produce a chart and a legend. Limits the number of buckets to
   * the value provided by $buckets. For example, if there are 100 buckets or
   * groups generated by the query and $buckets is "6" then the chart data will
   * have the actual values and labels for the 5 highest values of $sumField and
   * a sixth bucket that is the aggregate of the remaining 95 buckets with a
   * label of "Other"
   *
   * @param $socrataId string Socrata dataset id.
   * @param $groupByField string The field to group by. These are the label field.
   * @param $sumField string The field to aggregate. Must be summable.
   * @param $filerId string Should be id from the dataset, so it's not reliant on filer_id.
   * @param $electionYear string Should be removed. See filer_id.
   * @param array $chart Data is appended to the array passed
   * @param int $buckets Maximum number of groups
   *
   * @return void
   */
  protected function getChartData(
    string $socrataId, string $groupByField, string $sumField, string $filerId, string $electionYear,
    array &$chart, int $buckets=1000000): void {
    //build the query
    $query = $socrataId . '.json?$query='
      . "select $groupByField, sum($sumField) where filer_id='$filerId' and election_year=$electionYear group by $groupByField order by sum($sumField) desc";

    //grab the data
    //uses the .module function
    $chart_json = pdc_api_tools_query($query);

    //check that chart_data is not empty
    if (!empty($chart_json)) {
      //loop through the results
      $index = 0;
      $aggregateAmount = 0;
      foreach ($chart_json as $key => $item) {

        //check if the chart_group value exists in the data
        //if not, povide a default
        if ($index < $buckets - 1) {
          if (!isset($item[$groupByField])) {
            $group = 'Not Provided';
          }
          else {
            // Trim the group names to a maximum of 4 words then count and if over 30 chars, pop off
            // another word. This is not very precise but we don't know the layout. This would be much
            // better handled in the display code but WaTech was not able to do it.
            $words = explode(" ", $item[$groupByField], 5);
            if (count($words) > 4) {
              array_pop($words);
              $group = implode(" ", $words);
              if (strlen($group) > 30) {
                $lessWords = explode(" ", $group, 4);
                array_pop($lessWords);
                $group = implode(" ", $lessWords);
              }
              $group = $group . '...';
            }
            else {
              $group = $item[$groupByField];
            }
          }

          // Create groups for each of the valid labels.
          $chart[$group] = $item['sum_amount'];
        }
        else {
          $aggregateAmount += $item['sum_amount'];
        }
        $index++;
      }

      // If we've exceeded the buckets limit, or have an aggregated value, we
      // need to make room for it trimming the extra data groups.
      if ($aggregateAmount || count($chart) > $buckets) {
        $remainder = array_splice($chart, $buckets - 2);
        $chart['Miscellaneous'] = ($aggregateAmount ?? 0) + array_sum($remainder);
      }
    }
    return;
  }

  public function buildLoadableChart(string $chart_id, array $config): array {
    $attrs = array_filter([
      'data-set' => $config['dataset'],
      'data-dsid' => $config['data_id'] ?? null,
      'data-field' => $config['chart_select'] ?? null,
      'data-sum-field' => $config['chart_sum'] ?? null,
      'data-legend-values' => $config['legend_values'] ?? FALSE,
      'data-buckets' => $config['buckets'] ?? 6,
    ]);
    $attrs['class'] = ['campaign-data-chart'];

    return [
      // Use a theme variation of the chart that is setup for frontend
      // loading of the data, instead of having it loaded in Drupal.
      '#theme' => ['pdc_chart__loadable', 'pdc_chart'],
      '#attributes' => $attrs,
      '#title' => $config['label'],
      '#chart_id' => Html::getUniqueId('pdc-chart-' . $chart_id . '-' . $config['type']),
      '#chart_type' => $config['type'],
      '#headings' => $config['headings'],
      '#empty_message' => $config['empty_message'],
    ];
  }

  public function getAllowedSubpages(): array {
    return [
      'contributions',
      'expenditures',
      'pledges',
      'debts',
      'loans',
      'independent_expenditures',
    ];
  }

  /**
   * Builds campaign page subpage navigation element.
   *
   * @param string $id
   *   The page data identifier being rendered (candidate or committee ID).
   * @param string $page_id
   *   The campaign page identifier the navigation is being built for.
   * @param string|null $subpage
   *   The subpage identifier if building for a subpage. The navigation will
   *   exclude the current subpage.
   *
   * @return array
   *   Renderable array with populated with subpage navigation content.
   */
  public function buildSubNavigation(string $id, string $page_id, string $subpage = NULL) {
    //list of subpages to generate navigation links for
    $include_subpages = $this->getAllowedSubpages();

    $links = [];
    $route_params = ['id' => $id];
    $subpages = $this->pageManager->getSubpageDefinitions($page_id);

    //build links for subpages requested.
    foreach ($include_subpages as $subpage_id) {
      // If subpage is valid and not currently on that subpage add the link
      if ($subpage !== $subpage_id && isset($subpages[$subpage_id])) {
        $links[$subpage_id] = [
          'title' => ucwords(str_replace('_', ' ', $subpage_id)),
          'url' => Url::fromRoute($subpages[$subpage_id]['route']['name'], $route_params),
        ];
      }
    }

    return [
      '#theme' => 'pdc_page_sub_navigation',
      '#title' => $this->t('More About this @page_id:', [
        '@page_id' => ucwords(str_replace('_', ' ', $page_id)),
      ]),
      '#links' => $links,
    ];
  }

}
