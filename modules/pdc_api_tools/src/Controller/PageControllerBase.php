<?php

namespace Drupal\pdc_api_tools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\pdc_api_tools\PageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Base controller implementation for PDC page route callbacks.
 *
 * Base class for generating page content for the pdc_api_tools module and
 * driven by the configurations in the "/conf/pages" and "/conf/subpages"
 * configuration folders.
 */
abstract class PageControllerBase extends ControllerBase {

  /**
   * The page manager service to manage page and subpage definitions.
   *
   * @var \Drupal\pdc_api_tools\PageManagerInterface
   */
  protected PageManagerInterface $pageManager;

  /**
   * The path to the pdc_api_tools module.
   *
   * @var string
   */
  protected string $modulePath;

  /**
   * Creates a new instance of the CampaignControllerBase subclass.
   *
   * @param \Drupal\pdc_api_tools\PageManagerInterface $page_manager
   *   The page and subpage definitions manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_list
   *   The module extension list service.
   */
  public function __construct(PageManagerInterface $page_manager, ModuleExtensionList $module_list) {
    $this->pageManager = $page_manager;
    $this->modulePath = $module_list->getPath('pdc_api_tools');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pdc_api_tools.page_manager'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Gets the page route title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   String or translation that can be used as the page route title.
   */
  abstract public function title();

  /**
   * Build the response for a campaign page content.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   The HTTP response object or a renderable array representing the requested
   *   page contents of the campaign page or subpage.
   */
  abstract public function content(Request $request);

  /**
   * Get the page and subpage definitions.
   *
   * Method will always return a page definition (exception otherwise).
   * When requested, a subpage will always be returned.
   *
   * @param string $page_id
   *   The page identifier to fetch the page definition of.
   * @param string $subpage_id
   *   Optional subpage identifier if a subpage definition is requested. If not
   *   requested, the "subpage" value of the array will be NULL.
   *
   * @return array
   *   Returns an array with "page" and "subpage" keys which contain the
   *   respective definitions.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   When requested page or subpage definition does not exist, this page
   *   request should return a 404 because the page request is not valid.
   */
  protected function getPageDefinition(string $page_id, string $subpage_id = NULL): array {
    $page_conf = $this->pageManager->getPageDefinition($page_id);

    if (!$page_conf) {
      throw new NotFoundHttpException();
    }

    //if subpage requested, ensure that subpage is valid before making any
    //data calls to the PDC API systems.
    if (!empty($subpage_id)) {
      $subpage_conf = $this->pageManager->getSubpageDefinition($page_id, $subpage_id);

      //Invalid subpage requested, return a 404 error.
      if (!$subpage_conf) {
        throw new NotFoundHttpException();
      }
    }
    else {
      //set a default value for the theme variables.
      $subpage_conf = NULL;
    }

    return [
      'page' => $page_conf,
      'subpage' => $subpage_conf,
    ];
  }

}
