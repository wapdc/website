<?php

namespace Drupal\pdc_api_tools\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DataTables extends ControllerBase {

  /**
   * The only purpose of this function is to proxy requests to download filtered rows from Socrata to a web client
   * because providing a link directly to the socrata api results in the browser loading the csv into a browser, rather
   * than doing a download. If Socrata supported setting the content-disposition header, then this function would not be
   * needed. Using fopen/fpassthrough was tested with hte full contributions dataset and worked fine.
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return void
   */
  public function socrataDownloadPassthrough(Request $request) {
		$params = $request->query->all();
		$dsid = $params['dsid'];
		$fname = $params['fname'] ?? 'data';
		unset($params['dsid'], $params['fname']);

		// Without a datasource ID this request is invalid.
		if (empty($dsid)) {
			throw new NotFoundHttpException();
		}

    $url = "https://data.wa.gov/resource/{$dsid}.csv?" . UrlHelper::buildQuery($params);
    $fp = fopen($url, 'rb');
    foreach (get_headers($url) as $header)
    {
      header($header);
      header("Content-type: text/csv");
      header("Content-Disposition: attachment; filename=" . $fname . ".csv");
    }
    fpassthru($fp);
    exit;
  }


  //ONLY used by Candidates - Surplus Funds table and the filtered page
	//Build the title for the page
	//Called by the routing data
	public function title($table_name = NULL) {
		//Build the module path
		$module_handler = \Drupal::service('module_handler');
		$module_path = $module_handler->getModule('pdc_api_tools')->getPath();

		//Set the conf_dir
		$conf_dir = $module_path . '/conf/datatables';

		//Build the YML array
		$ymls = array_diff(scandir($conf_dir), array('..', '.'));
		$conf = array();
		$conf = Yaml::parseFile($conf_dir . '/' . $table_name . '.yml');

		//Need to check if we have a filder_id present
		if ((count($_GET)) && (isset($_GET['filer_id']))):
			//check for conf var
			if ($conf):
				//loop through the conf array
				foreach ($conf as $key => $settings):
					$table_name = $key;

	    			//Build the query
	    			$data_id = $settings['settings']['data_id'];
	    			$query = $data_id . '.json?$where=filer_id="' . $_GET['filer_id'] . '"';

	    			//Make the query
	    			//Lives in the .module file
	    			$results = pdc_api_tools_query($query);
				endforeach;
			endif;

			//check if the table_name meets our needs
			if ($table_name = 'candidates_surplus_funds_reports_table'):
				//check that we have results
				if(!empty($results)):
					//set the title to the filer_name column on the first result
					$title = 'Reports for ' . $results[0]['filer_name'];

				//if not, make it the default title
				else:
					$title = 'Reports';
				endif;
			endif;
		//No filer_id means we just load the default title
		else:
			$title = 'Reports';
		endif;

		//send that title to Drupal
		return $title;
	}

	//Build the content for the page
	//Called by the routing data
	public function content($table_name = NULL) {
		//set the current_path var based on, uh, the current path
		$current_path = \Drupal::service('path.current')->getPath();

		//need to grab the route data
		$route = \Drupal::routeMatch()->getRouteName();

		//now the path for that route
		//we'll use all this path stuff later
		$table_uri = Url::fromRoute($route)->toString();

		//grab the module path
		$module_handler = \Drupal::service('module_handler');
		$module_path = $module_handler->getModule('pdc_api_tools')->getPath();

		//set the conf_dir
		$conf_dir = $module_path . '/conf/datatables';

		//grab the YMLs
		$conf = Yaml::parseFile($conf_dir . '/' . $table_name . '.yml');
		$build = [];
		$selected_filter = '';
		$jurisdiction_query = '';
		
		//Check for and add table captions as necessary.
		$caption = [];
		if (!empty($conf[$table_name]['caption'])) {
			if (isset($conf[$table_name]['caption']['markup'])) {
				$caption['#markup'] = $conf[$table_name]['caption']['markup'];
			}
			elseif (isset($conf[$table_name]['caption']['text'])) {
				$caption['#plain_text'] = $conf[$table_name]['caption']['text'];
			}	
		}

		//check that we have $_GET vars
		//Used for filters
		if (count($_GET)):
			$jurisdiction_query_array = array();
			$new_query = '';

			//need to build a query based on the $_GET vars
			$query = '?' . htmlentities($_SERVER['QUERY_STRING']);
			$json_query = urldecode($_SERVER['QUERY_STRING']);

			//runs if the exp $_GET var exists
			if (isset($_GET['exp'])):
				parse_str($json_query, $json_query_array);
				$json_juris_query_str = $json_query_array['juris'];
				$jurisdiction_query_array = json_decode($json_juris_query_str);
				$query_array_unique = array_unique($jurisdiction_query_array);
				foreach($query_array_unique as $key => $item):
					$jurisdiction_query .= 'jurisdiction_code = "' . $item . '" OR ';
				endforeach;
				$jurisdiction_query =  rtrim($jurisdiction_query, ' OR ');
				$jurisdiction_query =  '(' . $jurisdiction_query . ')';
			endif;

			//make sure the jurisdiction var is not empty
			//builds a query to pass if any parameters are set in the URL
			//Not really used but it was added early on and not removed
			//Just in case
			if ($jurisdiction_query != ''):
				foreach ($conf[$table_name]['settings']['attrs'] as $key => $setting):
					if ($setting['name'] == 'data-dadc-fixed-filter'):
						if ($conf[$table_name]['settings']['attrs'][$key]['value'] != ''):
							$conf[$table_name]['settings']['attrs'][$key]['value'] = $setting['value'] . ' AND ' . $jurisdiction_query;
						else:
							$conf[$table_name]['settings']['attrs'][$key]['value'] = $jurisdiction_query;
						endif;
					endif;
				endforeach;
			endif;
			if (isset($conf[$table_name]['filters']['button_options'])):
				foreach($conf[$table_name]['filters']['button_options'] as $key => $filter):
					if ($filter['dest'] == $query):
						$selected_filter = $filter['name'];
					endif;
				endforeach;
			endif;
		endif;

		//Set the filters and columns vars if they exist
		$filters = isset($conf[$table_name]['filters']) ? $conf[$table_name]['filters'] : '';
		$columns = isset($conf[$table_name]['columns']) ? $conf[$table_name]['columns'] : '';

		//Check if the datatable has custom_def set
		if(isset($conf[$table_name]['settings']['column_defs_js'])):
			//Grab the value of the file from the YML
			$column_defs_js = $conf[$table_name]['settings']['column_defs_js'];
		else:
			//Otherwise, set it to default
			$column_defs_js = 0;
		endif;

		//Build the paths for the custom_def file
		$js_path_build = '/' . $module_path . '/assets/js/column_defs/';
		$default_js_path_build = '/' . $module_path . '/assets/js/column_defs/default_table.js';

		//Switch the path depending if a custom file is declared
		$column_defs_path = $column_defs_js == 1 ? $js_path_build . $table_name . '.js' : $default_js_path_build;

		//Gather all the build data so it can be sent off to the .module them function
		$build = [
			'#theme' => $table_name,
			'#current_path' => $current_path,
			'#column_defs_path' => $column_defs_path,
			'#data_id' => $conf[$table_name]['settings']['data_id'],
			'#table_id' => $conf[$table_name]['settings']['table_id'],
			'#caption' => $caption,
			'#filters' => $filters,
			'#selected_filter' => $selected_filter,
			'#table_uri'=> $table_uri,
			'#settings' => $conf[$table_name]['settings'],
			'#table_uri' => $table_uri,
			'#columns' => $columns,
			'#attached' => [
				'library' => [
					'pdc_api_tools/data_tables'
				],
				'drupalSettings' => [
					'pdc_api_tools' => [
            'filters' => $conf[$table_name]['settings']['filters'],
						'jurisdiction_query' => $jurisdiction_query
					]
				]
			]
		];
		//send all this to the .module theme function
		return $build;
	}
}
