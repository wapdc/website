<?php

namespace Drupal\pdc_api_tools;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Utility\Error;
use Drupal\pdc_api_tools\Controller\CampaignControllerBase;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Page manager for providing page and subpages from the module config files.
 *
 * The pdc_api_tools module provides campaign pages and enforcement case page
 * definitions from YAML files in the module's "/conf/pages" and
 * "/conf/subpages" folders.
 */
class PageManager implements PageManagerInterface {
  
  use LoggerChannelTrait;

  /**
   * The path to the module source files.
   *
   * Used to locate page conf folder.
   *
   * @var string
   */
  protected string $confPath;

  /**
   * Cache backend to store page definitions.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $confCache;

  /**
   * Loaded page definitions (array if loaded, null otherwise).
   *
   * @var array|null
   */
  protected ?array $pageDefinitions;

  /**
   * Loaded subpage definitions (array if loaded, null otherwise).
   *
   * Subpage definitions are stored as a two dimensional array. The values are
   * stored with the keys "subdefinition[$page_id][$subpage_id]".
   *
   * @var array|null
   */
  protected ?array $subpageDefinitions;

  /**
   * Creates a new instance of the PageManager class.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extensions
   *   The module extension list service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to store discovered page definitions.
   */
  public function __construct(ModuleExtensionList $module_extensions, CacheBackendInterface $cache_backend) {
    $this->confPath = $module_extensions->getPath('pdc_api_tools') . '/conf';
    $this->confCache = $cache_backend;
  }
  
  /**
   * Get the page definition cache tags for invalidation.
   * 
   * @return string[]
   *   Returns the cache tags for the page definitions.
   */
  public function getCacheTags(): array {
    return [
      'pdc_api_tools:page_definitions',
      'pdc_api_tools:subpage_definitions',
    ];
  }

  /**
   * Create a default base path for pages.
   *
   * @todo Include this in configurations or find a better way to resolve paths
   * based on definition details. For now this matches the current
   * implementation and avoids having broken link while the site transitions to
   * using a more service driven implementation.
   *
   * @param string $page_id
   *   The page identifier to get the default path for.
   * @param array $definition
   *   The page configuration and definition.
   */
  public function getDefaultPagePath(string $page_id, array $definition): string {
    if ('enforcement_cases' === $page_id) {
      return '/rules-enforcement/enforcement/enforcement-cases/{case}';
    }

    return "/political-disclosure-reporting-data/browse-search-data/{$page_id}s/{id}";
  }

  /**
   * {@inheritdoc}
   */
  public function getPageDefinition(string $page_id): ?array {
    return $this->getPageDefinitions()[$page_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubpageDefinition(string $page_id, string $subpage_id): ?array {
    return $this->getSubpageDefinitions($page_id)[$subpage_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageDefinitions(bool $force_refresh = FALSE): array {
    if (!isset($this->pageDefinitions)) {
      $cid = 'pdc_api_tools:page_definitions';

      if (!$force_refresh && $cached = $this->confCache->get($cid)) {
        $this->pageDefinitions = $cached->data;
      }
      else {
        $idRegex = '/^([\w_]*?)(?:_page|$)/i';

        foreach ($this->loadConfDefinitions($this->confPath . '/pages') as $id => $definition) {
          $pageId = preg_match($idRegex, strtolower($id), $matches) ? $matches[1] : strtolower($id);

          // Apply configuration defaults to ensure these settings have values.
          $definition += [
            'id' => $pageId,
            'route' => [],
            'page_settings' => $definition['settings'][$id] ?? [],
          ];

          // Provide defaults based on the naming schemes, but only if not
          // available in the page definition.
          $definition['route'] += [
            'name' => 'pdc_api_tools.page.' . $id,
            'path' => $this->getDefaultPagePath($pageId, $definition),
          ];
          
          if (empty($definition['route']['controller'])) {
            $defaultController = '\\Drupal\\pdc_api_tools\\Controller\\' . str_replace('_', '', ucwords($pageId, '_'));
            $definition['route']['controller'] = class_exists($defaultController) ? $defaultController : CampaignControllerBase::class;
          }          

          $this->pageDefinitions[$pageId] = $definition;
        }
        $this->confCache->set($cid, $this->pageDefinitions, Cache::PERMANENT, $this->getCacheTags());
      }
    }

    return $this->pageDefinitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubpageDefinitions(string $page_id, bool $force_refresh = FALSE): array {
    if (!isset($this->subpageDefinitions)) {
      $cid = 'pdc_api_tools:subpage_definitions';

      if (!$force_refresh && $cached = $this->confCache->get($cid)) {
        $this->subpageDefinitions = $cached->data;
      }
      else {
        $pageIds = array_keys($this->getPageDefinitions());
        $pageRegex = '/^(' . implode('|', array_map('preg_quote', $pageIds, ['/'])) . ')_(.+)/';

        foreach ($this->loadConfDefinitions($this->confPath . '/subpages') as $id => $definition) {
          if (preg_match($pageRegex, strtolower($id), $matches)) {
            [, $pageId, $subpageId] = $matches;

            // Append route info if not defined by config files. This ensures
            // that navigation and URL generation can be consistently created.
            if (!isset($definition['route']['name'])) {
              $definition['route']['name'] = 'pdc_api_tools.subpage.' . $id;
            }
            
            $definition['id'] = $subpageId;
            $this->subpageDefinitions[$pageId][$subpageId] = $definition;
          }
        }

        $this->confCache->set($cid, $this->subpageDefinitions, Cache::PERMANENT, $this->getCacheTags());
      }
    }

    return $this->subpageDefinitions[$page_id] ?? [];
  }

  /**
   * Load the page and subpage definitions from a directory.
   *
   * The page and subpage configurations are YAML files which provide
   * definitions and settings for pages available. The module provides these
   * files in the "/conf/pages" and "/conf/subpages" folders.
   *
   * @param string $path
   *   The directory to search for YAML configuration files.
   *
   * @return array
   *   An array of all configurations loaded from the $path directory, keyed
   *   by their configuration IDs defined in the files.
   */
  protected function loadConfDefinitions(string $path): array {
    $definitions = [];
    foreach (new \DirectoryIterator($path) as $file) {
      if ('yml' === $file->getExtension()) {
        try {
          $definitions += Yaml::parseFile($file->getRealPath());
        }
        catch (ParseException $e) {
          // Error reading the sub-page definition files.
          Error::logException($this->getLogger('pdc_api_tools'), $e);
        }
      }
    }

    return $definitions;
  }

}
