<?php

namespace Drupal\pdc_fullcalendar\Plugin\FullcalendarViewProcessor;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\fullcalendar_view\Plugin\FullcalendarViewProcessorBase;
use Drupal\pdc_fullcalendar\Plugin\views\style\PdcFullCalendarStyle;

/**
 * Apply PDC incremental display loading settings for the client JS.
 *
 * This processor provides the fetch URL and the currently loaded date interval
 * for the frontend client JS to use for loading more events when users
 * navigate the calendar displays.
 *
 * @FullcalendarViewProcessor(
 *   id = "pdc_fullcalendar_loading_intervals",
 *   label = @Translation("Loading intervals processor"),
 * )
 */
class PdcCalendarViewProcessor extends FullcalendarViewProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function process(array &$variables) {
    $view = $variables['view'];
    $style = $view->display_handler->getPlugin('style');

    if ($style instanceof PdcFullCalendarStyle && $style->isUsingLoadingIntervals()) {
      foreach ($variables['#attached']['drupalSettings']['fullCalendarView'] as &$full_calendar_view) {
        [$start, $end] = $style->getLoadingInterval() ?? [NULL, NULL];

        // Provide the current date interval for the JS client.
        if (!empty($start) && !empty($end)) {
          $full_calendar_view['dateInterval'] = [
            $start->format('Y-m-d'),
            $end->format('Y-m-d'),
          ];
        }

        // Provide the URL for the JS client to fetch additional date ranges.
        $full_calendar_view['fetchUrl'] = Url::fromRoute('pdc_fullcalendar.view_events_by_range', [
          'view' => $view->id(),
          'display' => $view->display_handler->display['id'],
        ])->toString();

        // The following modification omits entries without starts or ends, and
        // entries that are outside the provided interval.
        //
        // - By default, FullcalendarViewPreprocess will add an event entry for
        //   each date value of each event returned by the view. It ignores the
        //   results of the views, but will happily duplicate entries if an
        //   event shows up multiple times in the views results. This appears to
        //   be expected behavior.
        // - Also, Fullcalendar requires a start and end date to render an entry.
        //   FullcalendarViewPreprocess only add these on core date fields,
        //   SmartDateProcessor adds it for smart_date fields. End date may be
        //   missing in certain conditions. This means entries that are NOT
        //   renderable may be returned.
        //
        // This has to happen after all the other processors, as some processors
        // may still be altering start and end. There is an accompanying module
        // weight change to ensure pdc_fullcalendar is after smart_date.
        //
        // The only remaining potential cause of performance issues is the
        // serialization and deserialization of calendar_options. There's no way
        // around this, FullcalendarViewPreprocess serializes the options before
        // passing it to processors, which must deseralize and reserialize it
        // should the processor need to alter the options.
        $calendar_options_json = $full_calendar_view['calendar_options'];
        $calendar_options = Json::decode($calendar_options_json);
        $events = $calendar_options['events'] ?? [];

        // We cannot determine what to include if the interval is empty.
        $filtered_events = empty($start) || empty($end)
          ? []
          : array_filter($events, function($e) use ($start, $end) {
            if (empty($e['start'])) return FALSE;
            if (empty($e['end'])) return FALSE;

            // At this point in the processing, the start and end could either be
            // timestamps or date strings.
            // @see \Drupal\fullcalendar_view\FullcalendarViewPreprocess::process
            // @see \Drupal\smart_date\Plugin\FullcalendarViewProcessor\SmartDateProcessor::updateEntry
            $entry_start = is_integer($e['start'])
              ? $e['start']
              : (is_string($e['start'])
                ? strtotime($e['start'])
                : NULL);

            if (empty($entry_start)) return FALSE;

            return $start->getTimestamp() <= $entry_start && $entry_start < $end->getTimestamp();
          });

        $termField = $style->options['tax_field'];
        $calendar_options['events'] = [];
        foreach ($filtered_events as $event) {
          [$rowIndex,] = explode('-', $event['id'], 2);

          if ($event_type = $style->getFieldValue(intval($rowIndex), $termField)) {
            $event['className'] = 'event-category-' . $event_type;
          }
          $calendar_options['events'][] = $event;
        }

        $full_calendar_view['calendar_options'] = Json::encode($calendar_options);
      }
    }
  }

}
