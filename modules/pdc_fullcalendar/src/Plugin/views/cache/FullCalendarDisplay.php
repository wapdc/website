<?php

namespace Drupal\pdc_fullcalendar\Plugin\views\cache;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\pdc_fullcalendar\Plugin\views\style\PdcFullCalendarStyle;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Set the max age to the end of the FullCalendar display duration.
 *
 * Ensures that results and rendered displays are updated upon the next calendar
 * display interval.
 *
 * @ingroup views_cache_plugins
 *
 * @ViewsCache(
 *   id = "pdc_fullcalendar_interval",
 *   title = @Translation("FullCalendar display interval"),
 *   help = @Translation("Expires caches to the end of a display interval when FullCalendar loading intervals are used. Falls back to standard cache tags.")
 * )
 */
class FullCalendarDisplay extends CachePluginBase {

  /**
   * The datetime time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $timeService;

  /**
   * Creates a new instance of the FullCalendarDisplay view cache plugin.
   *
   * @param array $configuration
   *   The cache plugin configurations.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->timeService = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('FullCalendar display');
  }

  /**
   * {@inheritdoc}
   */
  protected function cacheExpire($type) {
    if ($expires = $this->getDateRangeEnd()) {
      return $expires;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function cacheSetMaxAge($type) {
    if ($expires = $this->getDateRangeEnd()) {
      $current = $this->timeService->getRequestTime();

      if ($expires > $current) {
        return $expires - $current;
      }

      // Already expired? Generally the date range end calculation should always
      // be a value in the future, but in case.
      return 0;
    }
    else {
      return Cache::PERMANENT;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultCacheMaxAge() {
    // The max age, unless overridden by some other piece of the rendered code
    // is determined by the output time setting.
    return (int) $this->cacheSetMaxAge('output');
  }

  /**
   * Get the end of the display date interval as a timestamp value.
   *
   * When using loading intervals for the FullCalendar display, the cache for
   * results needs to expire at the end of that date interval.
   *
   * For example, if results are for Monthly calendar view, at the end of the
   * month the cache should expire, and should fetch (and cache) the next month
   * until the end of that interval.
   *
   * This method gets the timestamp for the date, which the date interval should
   * end, and can be used to calculate the cache max-age / expiration date. Note
   * that cache tags are still being used, so event updates, or settings changes
   * will still cause cache invalidations.
   *
   * @return int|null
   *   The timestamp of when the default fetched calendar display results should
   *   expire. If no expiration, then NULL will be returned.
   */
  protected function getDateRangeEnd(): ?int {
    $view = $this->view;
    $style = $view->display_handler->getPlugin('style');

    if ($style instanceof PdcFullCalendarStyle && $style->isUsingLoadingIntervals()) {
      // If no date range provided, just using cache tags is appropriate.
      // This only needs to change the cache of the default view result set as
      // the display interval changes. The dynamically fetched intervals are
      // already cached appropriately.
      if (empty($view->getExposedInput()['caldisp']['start'])) {
        // Default interval is just 1 day.
        $interval = 1;
        $date = new DateTimePlus();

        switch ($style->options['default_view']) {
          case 'listMonth':
          case 'dayGridMonth':
            $parts = [
              'year' => intval($date->format('Y')),
              'month' => intval($date->format('n')),
              'day' => $date->format('j'),
            ];

            // Cache until end of month (move date to start of following month).
            if (12 <= $parts['month']) {
              $parts['month'] = 1;
              $parts['year'] += 1;
            }
            else {
              $parts['month'] += 1;
            }
            // starts at the first day of the month.
            $parts['day'] = '01';

            // Timestamp for end of month.
            return DateTimePlus::createFromArray($parts)->format('U');

          case 'timeGridWeek':
          case 'listWeek':
            $offset = $date->format('w') - intval($this->options['firstDay'] ?? 0);
            if ($offset < 0) {
              $offset += 7;
            }

            // Add the number of days remaining in the current week. This is
            // the start of week offset, minus a full week (7 days).
            $interval = 7 - $offset;
            break;
        }

        $date->add(new \DateInterval("P{$interval}D"));
        return $date->format('U');
      }
    }

    // No caching, cache tags only.
    return NULL;
  }

}
