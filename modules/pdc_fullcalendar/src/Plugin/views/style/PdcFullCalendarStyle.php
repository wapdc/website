<?php

namespace Drupal\pdc_fullcalendar\Plugin\views\style;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\core\form\FormStateInterface;
use Drupal\fullcalendar_view\Plugin\views\style\FullCalendarDisplay;
use Drupal\views\Plugin\views\query\Sql;

/**
 * Replacement FullCalendar Views style plugin.
 *
 * The normal FullCalendar Views style plugin does not support incremental
 * loading of events by date ranges. This causes calendar displays to need to
 * load all available events even when they aren't displayed. For views
 * with a large set of events, this causes the system to be overwhelmed.
 *
 * This filter overrides the default by adding options to load by date ranges.
 * This limits the results to only events in the requested range, and can load
 * only events needed for the current display.
 *
 * @see pdc_fullcalendar_views_plugins_style_alter()
 */
class PdcFullCalendarStyle extends FullCalendarDisplay {

  /**
   * The start interval range events if incremental load was used.
   *
   * @var \Drupal\Component\Datetime\DateTimePlus|null
   */
  protected $startDate;

  /**
   * The end interval range events if incremental load was used.
   *
   * @var \Drupal\Component\Datetime\DateTimePlus|null
   */
  protected $endDate;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Add support for dynamic loading intervals support.
    $options['use_loading_intervals'] = [
      'default' => FALSE,
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Add options to support incremental loading of events by calendar display.
    $form['use_loading_intervals'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load events by display interval'),
      '#default_value' => $this->options['use_loading_intervals'],
      '#description' => $this->t('Load the results based on the display settings (e.g. one month at a time). This limits the results to date intervals compatible to the display settings and helps to alleviate loading issues when there are too many results.'),
    ];
  }

  /**
   * Method to check if the style plugin is configure to use incremenal loading.
   *
   * @return bool
   *   TRUE if the display is configured and ready to return events by date
   *   ranges. FALSE if the configuration is incompatiable or the feature is
   *   disabled in the style options.
   */
  public function isUsingLoadingIntervals(): bool {
    // The loading intervals options are disabled.
    if (empty($this->options['use_loading_intervals'])) {
      return FALSE;
    }

    // Pager functionality supercedes the loading intervals, so check if
    // the dsiplay is already chunking results into pages.
    // Using the pager can cause incomplete results for the requested intervals
    // and display incomplete calendar views, so paging is incompatible.
    $pager = $this->displayHandler->getPlugin('pager');
    return !$pager || 'none' == $pager->getPluginId();
  }

  /**
   * Get the start and ending date range for the loading interval of events.
   *
   * Reports back for preprocess and processors what the date ranges the events
   * were fetched for if the query was altered to support incremental loading
   * of events.
   *
   * @return DateTimePlus[]|null
   *   An array with the first value as the start of the date interval, and the
   *   second array value as the end of the date interval. If date intervals
   *   are not available, return NULL.
   */
  public function getLoadingInterval() {
    if ($this->startDate && $this->endDate) {
      return [
        $this->startDate,
        $this->endDate,
      ];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    // Add a loading spinner for when events are loaded.
    if ($this->isUsingLoadingIntervals() && empty($this->view->getExposedInput()['caldisp']['start'])) {
      $build['loading_spinner'] = [
        '#theme' => 'pdc_module_spinner',
        '#spinner_id' => 'fullcalendar-loading-spinner',
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();

    // If not utilizing loading intervals, no changes are needed.
    if (!$this->isUsingLoadingIntervals()) {
      return;
    }

    $view = $this->view;
    $query = $view->query;
    $startField = $this->displayHandler->getHandler('field', $this->options['start']);

    // Ensure a query type that we can alter and that the required $startField
    // is available. In general both of these should be true, but it is good to
    // ensure configurations or fields haven't been changed without updating
    // the style plugin options.
    if ($query instanceof Sql && $startField) {
      $input = $view->getExposedInput()['caldisp'] ?? [];

      // Create the start and end date ranges for the display interval. Prefer
      // the date ranges (from FullCalendar display ranges) from the exposed
      // input if available, otherwise create date ranges based on the display
      // options available in style plugin settings.
      $this->startDate = empty($input['start']) ? $this->getDefaultStartDate() : new DateTimePlus($input['start']);
      if (empty($input['end'])) {
        $refDate = $this->startDate->getPhpDateTime();
        $this->endDate = $this->getDefaultEndDate($refDate);
      }
      else {
        $this->endDate = new DateTimePlus($input['end']);
      }

      // Format the date interval for use with queries.
      $range = [
        $this->startDate->format('U'),
        $this->endDate->format('U'),
      ];

      // The "end" date is optional, so check, but it doesn't have to exist.
      if (!empty($this->options['end'])) {
        $endField = $this->displayHandler->getHandler('field', $this->options['end']);
      }

      if (!empty($endField)) {
        $query->addWhere(1, "{$endField->tableAlias}.{$endField->realField}", $range[0] , '>=');
        $query->addWhere(1, "{$startField->tableAlias}.{$startField->realField}", $range[1], '<');
      }
      else {
        $query->addWhere(1, "{$startField->tableAlias}.{$startField->realField}", $range, 'BETWEEN');
      }
    }
  }

  /**
   * Get the default start date to use based on the calendar display options.
   *
   * @return \Drupal\Component\Datetime\DateTimePlus
   *   A datetime object with the default start date time to filter events by.
   */
  public function getDefaultStartDate() {
    $date = new DateTimePlus();
    $dateParts = [
      'year' => $date->format('Y'),
      'month' => $date->format('n'),
      'day' => $date->format('j'),
    ];

    switch ($this->options['default_view']) {
      case 'listMonth':
        // List view starts at the first day of the month.
        $dateParts['day'] = '01';
        break;

      case 'dayGridMonth':
        $dateParts['day'] = '01';
        $date = DateTimePlus::createFromArray($dateParts);
        // purposely fall through to offset to first day of week.

      case 'timeGridWeek':
      case 'listWeek':
        $intval = $date->format('w') - intval($this->options['firstDay'] ?? 0);
        if ($intval < 0) {
          $intval += 7;
        }

        $date->sub(new \DateInterval("P{$intval}D"));
        return $date;
    }

    // Rest are day to display the current day.
    return DateTimePlus::createFromArray($dateParts);
  }

  /**
   * Create the default end date to use based on the calendar display settings.
   *
   * @param \DateTime $ref_date
   *   A reference date to base the end date from.
   *
   * @return \Drupal\Component\Datetime\DateTimePlus
   *   A datetime object with the default end date time to filter events by.
   */
  public function getDefaultEndDate(\DateTime $ref_date) {
    switch ($this->options['default_view']) {
      case 'listMonth':
        // Set the end date to the start of the following month.
        $date = new DateTimePlus();
        $parts = [
          'year' => intval($date->format('Y')),
          'month' => intval($date->format('n')),
          'day' => 1,
        ];

        if (12 <= $parts['month']) {
          $parts['month'] = 1;
          $parts['year'] += 1;
        }
        else {
          $parts['month'] += 1;
        }
        return DateTimePlus::createFromArray($parts);

      case 'dayGridMonth':
        // The grid display always shows 6 weeks (42 days). This allows it to
        // always show complete weeks at the start and end of the month.
        $offset = 42;
        break;

      case 'timeGridWeek':
      case 'listWeek':
        $offset = 7;

      default:
        // The remaining intervals are just 1 day.
        $offset = 1;
    }

    $end = DateTimePlus::createFromDateTime($ref_date);
    $end->add(new \DateInterval("P{$offset}D"));
    return $end;
  }

}
