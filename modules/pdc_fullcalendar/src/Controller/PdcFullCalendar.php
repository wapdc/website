<?php

namespace Drupal\pdc_fullcalendar\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\fullcalendar_view\FullcalendarViewPreprocess;
use Drupal\pdc_fullcalendar\Plugin\views\style\PdcFullCalendarStyle;
use Drupal\views\ViewEntityInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Route controller for generating event results for calendar displays.
 */
class PdcFullCalendar implements ContainerInjectionInterface {

  /**
   * Views executable factory service.
   *
   * @var \Drupal\views\ViewExecutableFactory
   */
  protected ViewExecutableFactory $viewFactory;

  /**
   * The FullCalender View preprocess service.
   *
   * @var \Drupal\fullcalendar_view\FullcalendarViewPreprocess
   */
  protected FullcalendarViewPreprocess $preprocessor;

  /**
   * FullCalender preprocoess plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $preprocessManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Constructs a new instance of the PdcFullCalendar route controller.
   *
   * @param \Drupal\views\ViewExecutableFactory $view_executable_factory
   *   The views executable factory service.
   * @param \Drupal\fullcalendar_view\FullcalendarViewPreprocess $fullcalendar_view_preprocess
   *   The FullCalendar View preprocess service.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $fullcalendar_view_preprocess_manager
   *   The renderer service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(ViewExecutableFactory $view_executable_factory, FullcalendarViewPreprocess $fullcalendar_view_preprocess, PluginManagerInterface $fullcalendar_view_preprocess_manager, RendererInterface $renderer) {
    $this->viewFactory = $view_executable_factory;
    $this->renderer = $renderer;

    // FullCalendar preprocessors which build out the event JSON data.
    $this->preprocessor = $fullcalendar_view_preprocess;
    $this->preprocessManager = $fullcalendar_view_preprocess_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('views.executable'),
      $container->get('fullcalendar_view.view_preprocess'),
      $container->get('plugin.manager.fullcalendar_view_processor'),
      $container->get('renderer')
    );
  }

  /**
   * Ensure that user has access to the view and display.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session to check access to the view for.
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view config entity for the view being checked for access.
   * @param string $display
   *   The view display identifier.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result of the access check of the user account for viewing
   *   this view display.
   */
  public function viewAccess(AccountInterface $account, ViewEntityInterface $view, string $display): AccessResultInterface {
    $viewObj = $this->viewFactory->get($view);
    if ($viewObj->setDisplay($display)) {
      $displayHandler = $viewObj->display_handler;;
      $access = AccessResult::allowedIf($displayHandler->access($account))
        ->addCacheableDependency($displayHandler->getCacheMetadata());
    }
    else {
      $access = AccessResult::forbidden()->addCacheTags($viewObj->getCacheTags());
    }

    $access->addCacheContexts(['route']);
    return $access;
  }

  /**
   * Fetches events for a FullCalender view display for a date range.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current HTTP request.
   * @param \Drupal\views\ViewEntityInterface $view
   *   The FullCalendar view to get results from.
   * @param string $display
   *   The display identifier for the views display to fetch events from.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return the event results with the events formatted for use with the
   *   FullCalendar client.
   */
  public function getEventsByRange(Request $request, ViewEntityInterface $view, string $display) {
    $viewObj = $this->viewFactory->get($view);

    // Ensure view is available and is using a compatible style plugin.
    if ($viewObj->setDisplay($display)) {
      $style = $viewObj->display_handler->getPlugin('style');

      if ($style instanceof PdcFullCalendarStyle && $style->isUsingLoadingIntervals()) {
        $exposedInput = $viewObj->getExposedInput();

        // Build the Views result events into a JSON format compatible with
        // the FullCalendar Javascript loading callback.
        $data = $this->buildEventsJson($viewObj);
        $response = new CacheableJsonResponse([
          'events' => $data['events'],
        ]);

        $cacheMeta = CacheableMetadata::createFromRenderArray($data);
        $cacheMeta->addCacheContexts([
          'route',
          'url.query_args'
        ]);

        return $response->addCacheableDependency($cacheMeta);
      }
    }

    // Missing View display or incorrect style plugin. Treat this as a page not
    // found because this route should not be available in this situation.
    throw new NotFoundHttpException();
  }

  /**
   * Execute the view and format the results into JSON data.
   *
   * The event results are formatted into JSON object using the FullCalendar
   * View preprocessors. This matches the formatting of the configured
   * FullCalendar view and the Javascript client.
   *
   * @param \Drupal\views\ViewExecutable $view_exec
   *   The views executable with all the exposed input already applied.
   * @param array $args
   *   Any additional views arguments to add to the views result constraints.
   *
   * @return array
   *   Array with the resulting events in the "events" key, and caching
   *   metadata in the "#cache" key. If not events available the "events" key
   *   will still be present, but the value will be an empty array.
   */
  protected function buildEventsJson(ViewExecutable $view_exec, array $args = []): array {
    $context = new RenderContext();

    // The views and preprocess potentially run in a render context and generate
    // caching metadata that needs to be captured in a render context. Run
    // these processes in an encapsulated render context so we can accurately
    // provide caching metadata for the built results.
    //
    // This allows the controller to return cacheable response types.
    $variables = $this->renderer->executeInRenderContext($context, function() use ($view_exec, $args) {
      $output = $view_exec->preview(NULL, $args);
      $variables = [
        '#cache' => $output['#cache'],
        'view' => $output['#view'],
        'rows' => $output['#rows'],
        'user' => $view_exec->getUser(),
      ];

      // The list of available plugins.
      $plugin_definitions = $this->preprocessManager->getDefinitions();

      // Getting supported field types from plugin definitions
      $variables['fullcalendar_fieldtypes'] = [];
      foreach ($plugin_definitions as $definition) {
        if (isset($definition['field_types'])) {
          $variables['fullcalendar_fieldtypes'] += $definition['field_types'];
        }
      }

      $this->preprocessor->process($variables);

      // Allow plugins to alter the view variables
      // (ex: smart_date does this to handle recurrances).
      foreach ($plugin_definitions as $definition) {
        $plugin = $this->preprocessManager->createInstance($definition['id']);

        if (method_exists($plugin, 'process')) {
          $plugin->process($variables);
        }
      }

      return $variables;
    });

    // The FullCalendar View style plugin preprocess places the formatted
    // events into the DrupalSettings value for the calendar index. Check that
    // these values exist and extract them if they do.
    $index = $variables['view_index'] ?? 0;
    $jsSettings = $variables['#attached']['drupalSettings']['fullCalendarView'] ?? [];

    $events = [];
    if (!empty($jsSettings[$index]['calendar_options'])) {
      $calendarOptions = $jsSettings[$index]['calendar_options'];
      $options = is_string($calendarOptions) ? json_decode($calendarOptions, TRUE) : $calendarOptions;
      $events = $options['events'] ?? [];
    }

    // Ensure that caching metadata collected during the build and preprocess
    // methods are carried through into the returned cache metadata.
    if (!$context->isEmpty()) {
      /** @var \Drupal\Core\Cache\CacheableMetadata $metadata */
      $metadata = $context->pop();
      $metadata->merge(CacheableMetadata::createFromRenderArray($variables))
        ->applyTo($variables);
    }

    return [
      '#cache' => $variables['#cache'],
      'events' => $events,
    ];
  }

}
