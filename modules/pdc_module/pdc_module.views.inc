<?php

/**
 * @file
 * Implements views hooks and data alter functions for the PDC module.
 */

/**
 * Implements hook_views_data_alter().
 */
function pdc_module_views_data_alter(array &$data) {
  $data['node_field_data']['pdc_skip_content'] = [
    'title' => t('Skip news items'),
    'description' => t('Filter to skip the most recent sticky news articles.'),
    'filter' => [
      'id' => 'pdc_skip_content_filter',
      'real field' => 'nid',
    ],
  ];
}
