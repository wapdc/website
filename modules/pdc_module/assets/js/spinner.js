/**
 * Add loading spinner utilities to active and hide the loading spinner.
 */

(function ($, Drupal) {
  // Ensure that the namespace is available, but don't overwrite it if it
  // was already initialized by another JS file.
  Drupal.pdcModule = Drupal.pdcModule || {};

  // Utility function to enable and disable a loading spinner by identifier.
  Drupal.pdcModule.loadingSpinner = function(spinnerId, show = true) {
    const spinner = $(`#${spinnerId}`);

    // Show or hide the loading spinner based on the show parameter.
    show ? spinner.show() : spinner.hide();
  }
})(jQuery, Drupal);
