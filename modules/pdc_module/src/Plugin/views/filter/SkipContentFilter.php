<?php

namespace Drupal\pdc_module\Plugin\views\filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter to exclude content results matching status and flags.
 * 
 * At the time of writing, this is used to exclude very specificly the news 
 * content surfaced on the homepage blocks that display a single sticky or
 * most recent news item.
 * 
 * In the future these options can become expanded and configurable:
 *  - content type selection
 *  - other sort fields
 *  - more flag conditions
 * 
 * For now the filters are static and only the options for the number of items
 * to skip.
 * 
 * @ViewsFilter("pdc_skip_content_filter")
 */
class SkipContentFilter extends FilterPluginBase {
  
  /**
   * The entity type manager.
   * 
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;
  
  /**
   * Constructs new instance of SkipContentFilter views filter plugin.
   * 
   * @param array $configuration
   *   The filter plugin options and configuration.
   * @param string $plugin_id
   *   The plugin filter identitifier.
   * @param mixed $plugin_definition
   *   The views filter plugin definition from discovery.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    
    $this->entityTypeManager = $entity_type_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    ); 
  }
  
  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    
    // Only allow "NOT IN" is allowed for this filter.
    $options['operator'] = ['default' => 'not in'];
    
    // Potential options in the future.
    $options['types'] = ['default' => ['news']];
    $options['count'] = ['default' => 1];
    $options['sort'] = ['default' => [
      'sticky' => 'DESC',
      'field_date' => 'DESC',
    ]];
  
    return $options;
  }
 
  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    
    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of items to skip'),
      '#min' => 1,
      '#step' => 1,
      '#default_value' => $this->options['count'],
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    
    $entityTypeId = $this->getEntityType();
    $subquery = \Drupal::entityTypeManager()
      ->getStorage($entityTypeId)
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $this->options['types'], 'IN')
      ->condition('status', NodeInterface::PUBLISHED)
      ->range(0, $this->options['count'] ?: 1);
    
    foreach ($this->options['sort'] ?? [] as $field => $direction) {
      $subquery->sort($field, $direction);
    }
    // MySQL doesn't support "limit" in subqueries, so execute the query here.
    $nids = $subquery->execute();
    
    // Only "NOT IN" is the only operation that makes sense. 
    $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", $nids, 'NOT IN');
  }
  
}
