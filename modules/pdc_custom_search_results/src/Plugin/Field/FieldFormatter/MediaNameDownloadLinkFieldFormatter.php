<?php

namespace Drupal\pdc_custom_search_results\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media_entity_download\Plugin\Field\FieldFormatter\DownloadLinkFieldFormatter;

/**
 * Media Name Download Link field formatter.
 *
 * An extension of DownloadLinkFieldFormatter that uses the owning media name
 * instead of the file name. Only applies to file fields that only hold one
 * file (e.g. Media types).
 *
 * @FieldFormatter(
 *   id = "pdc_media_name_download_link",
 *   label = @Translation("Media Name Download link"),
 *   field_types = {
 *     "file",
 *     "image"
 *   }
 * )
 */
class MediaNameDownloadLinkFieldFormatter extends DownloadLinkFieldFormatter {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter only makes sense if the file field only supports a
    // single value. If the file field allows for multiple values, each
    // value would use the host media's name, making it confusing.
    return $field_definition->getFieldStorageDefinition()->getCardinality() === 1;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      if (empty($elements[$delta])) continue;
      $elements[$delta]['#title'] = $item->getEntity()->label();
    }

    return $elements;
  }

}
